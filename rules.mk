ifndef	tools/rules.mk
	tools/rules.mk = included


.PHONY: menuconfig
menuconfig : link $(PKG_BLD_DIR)/.dir $(call bstamp_f,install-root,ncurses)
ifdef MENUCONFIG_CMD
	@$(MENUCONFIG_CMD)
endif


endif # tools/rules.mk
