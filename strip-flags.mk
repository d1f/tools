define _strip_help_
  -p --preserve-dates              Copy modified/access timestamps to the output
  -R --remove-section=<name>       Remove section <name> from the output
  -s --strip-all                   Remove all symbol and relocation information
  -g -S -d --strip-debug           Remove all debugging symbols & sections
     --strip-unneeded              Remove all symbols not needed by relocations
     --only-keep-debug             Strip everything but the debug information
  -N --strip-symbol=<name>         Do not copy symbol <name>
  -K --keep-symbol=<name>          Do not strip symbol <name>
     --keep-file-symbols           Do not strip file symbol(s)
  -w --wildcard                    Permit wildcard in symbol comparison
  -x --discard-all                 Remove all non-global symbols
  -X --discard-locals              Remove any compiler-generated symbols
  -v --verbose                     List all object files modified
  -V --version                     Display this program's version number
     --info                        List object formats & architectures supported
  -o <file>                        Place stripped output into <file>
endef


STRIP_SECTIONS  = .comment .note .pdr .mdebug.abi32
STRIP_SECT_OPTS = $(addprefix --remove-section=,$(STRIP_SECTIONS))

STRIP_SHLIB = $(STRIP_SECT_OPTS) --strip-unneeded
STRIP_EXEC  = $(STRIP_SECT_OPTS) --strip-all --discard-all
STRIP_KMOD  = $(STRIP_SECT_OPTS) --strip-unneeded
