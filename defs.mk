ifndef	tools/defs.mk
	tools/defs.mk = included


ifeq ($(filter $(NOT_BUILD_TARGETS),$(MAKECMDGOALS)),)
  ifndef    PLATFORM
    $(error PLATFORM undefined)
  endif
endif


define REMOVE_DOT_SVN_CMD
  echo -n "$(I1)Removing .svn ... "
  cd $(PKG_ORIG_SRC_DIR) &&	\
	find . -depth -type d -name .svn -exec rm -rf '{}' ';' $(LOG)
  echo done
endef


PKG_MENUCONFIG_FILE ?= $(PLATFORM_DIR)/$(PKG_BASE).config
PKG_MENUCONFIG_SUBDIR ?= .

define _MCONF_CMD_
  cp -vf $(PKG_MENUCONFIG_FILE) $(PKG_BLD_SUB_DIR)/$(PKG_MENUCONFIG_SUBDIR)/.config
  $(DOMAKE)                  -C $(PKG_BLD_SUB_DIR) menuconfig $(MK_VARS) $(MCONF_MK_VARS)
  cp -vf $(PKG_BLD_SUB_DIR)/$(PKG_MENUCONFIG_SUBDIR)/.config $(PKG_MENUCONFIG_FILE)
endef

MENUCONFIG_CMD_DEFAULT = $(if $(strip $(PKG_MENUCONFIG_FILE)),$(_MCONF_CMD_),\
			     $(error No PKG_MENUCONFIG_FILE defined))


include    $(XWMAKE_DIR)/defs/platform.mk
include $(TOOLS_PKG_DIR)/linux.mk
include $(TOOLS_PKG_DIR)/toolchain.mk
include     $(TOOLS_DIR)/build-flags.mk


endif # tools/defs.mk
