# common project build flags

ifndef	tools/build-flags.mk
	tools/build-flags.mk = included


ifndef      xwmake/defs/dir-defs.mk
$(error	$(I)xwmake/defs/dir-defs.mk is not included)
endif


include $(TOOLS_PKG_DIR)/pkg-config.mk


# .. lang=ru
# ������ ��������� ��� ���� ������ ������ ���������� (<PREFIX>/include: *.h)
# ��� ��������� ���������.
BUILD_IDIRS += $(BINC_DIR)

# .. lang=ru
# ������ ��������� ��� ���� ������ ��������� (<PREFIX>/lib: lib*.a, lib*.so*)
# ��� ��������� ���������.
BUILD_LDIRS += $(BLIB_DIR)


ifeq ($(PLATFORM),build)

  STD_IDIRS += $(BUILD_IDIRS)
  STD_LDIRS += $(BUILD_LDIRS)

else

# .. lang=ru
# ������ ��������� ��� ���� ������ ������ ���������� (<PREFIX>/include: *.h)
# ��� ������� ���������.
  STD_IDIRS +=             $(IINC_DIR)

# .. lang=ru
# ������ ��������� ��� ���� ������ ��������� (<PREFIX>/lib: lib*.a, lib*.so*)
# ��� ������� ���������.
  STD_LDIRS += $(RLIB_DIR) $(ILIB_DIR)

endif

# .. lang=ru
# ������ ������ :manpage:`ld(1)` � ��ԣ� ������ ������ ����������
# ��� ������� ���������.
STD_IDIRSFLAGS = $(addprefix -I,$(STD_IDIRS))

# .. lang=ru
# ������ ������ :manpage:`ld(1)` � ��ԣ� ������ ������ ����������
# ��� ��������� ���������.
BUILD_IDIRSFLAGS = $(addprefix -I,$(BUILD_IDIRS))

# .. lang=ru
# ������ ������ :manpage:`ld(1)` � ��ԣ� ������ ���������
# ��� ������� ���������.
STD_LDIRSFLAGS = $(addprefix -L,$(STD_LDIRS))

# .. lang=ru
# ������ ������ :manpage:`ld(1)` � ��ԣ� ������ ���������
# ��� ��������� ���������.
BUILD_LDIRSFLAGS = $(addprefix -L,$(BUILD_LDIRS))

# .. lang=ru
# ������ ������ :manpage:`ld(1)` � ��ԣ� ������ ��������� ������� ����������
# ��� ������� ���������.
# .. lang=en
# LD flags list with run-time search path for target platform.
STD_LDIRSFLAGS += -Wl,-rpath-link,$(ILIB_DIR)
ifeq ($(PLATFORM),build)
STD_LDIRSFLAGS += -Wl,-rpath,$(ILIB_DIR)
STD_LDIRSFLAGS += -Wl,-rpath,$(RLIB_DIR)
else
STD_LDIRSFLAGS += -Wl,-rpath,/lib
endif

# .. lang=ru
# ������ ������ :manpage:`ld(1)` � ��ԣ� ������ ��������� ������� ����������
# ��� ��������� ���������.
# .. lang=en
# LD flags list with run-time search path for build platform.
BUILD_LDIRSFLAGS += -Wl,-rpath-link,$(BLIB_DIR)
BUILD_LDIRSFLAGS +=      -Wl,-rpath,$(BLIB_DIR)


# .. lang=ru
# ������� ����������� ����������� (�������� gcc -Ox)
# ��� ������ ����������� :term:`toolchain <Toolchain>`.
#
# �������� �� ���������: s.
TC_OLEVEL ?= s

# .. lang=ru
# ���� ����������� ����������� (�������� gcc -Ox)
# ��� ������ ����������� :term:`toolchain <Toolchain>`.
#
# �������� �� ���������: -O$(TC_OLEVEL).
TC_OFLAG  ?= -O$(TC_OLEVEL)

# .. lang=ru
# ����� ����������� ����� ��
# ��� c����� ����������� :term:`toolchain <Toolchain>`.
#
# �������� �� ���������: -pipe -w $(TC_OFLAG).
TC_CFLAGS ?= -pipe -w $(TC_OFLAG)


# .. lang=ru
# ������� ����������� ����������� (�������� gcc -Ox)
# ��� ������ ����������� ��������� ���������.
#
# �������� �� ���������: s.
BUILD_OLEVEL ?= s

# .. lang=ru
# ���� ����������� �����������
# ��� ������ ����������� ��������� ���������.
#
# �������� �� ���������: -O$(BUILD_OLEVEL).
BUILD_OFLAG  ?= -O$(BUILD_OLEVEL)

# .. lang=ru
# ����� ����������� ����� ��
# ��� c����� ����������� ��������� ���������.
#
# �������� �� ���������: -pipe -w $(BUILD_OFLAG).
BUILD_CFLAGS += -pipe -w $(BUILD_OFLAG)
BUILD_CFLAGS += $(if $(DEBUG),-g,-g0)
BUILD_CFLAGS += $(if $(PROFILE),-pg)


# .. lang=ru
# ������� ����������� ����������� (�������� gcc -Ox)
# ��� ������� ���������.
#
# �������� �� ���������: s.

STD_OLEVEL ?= $(if $(filter build,$(PLATFORM)),$(BUILD_OLEVEL),s)

# .. lang=ru
# ���� ����������� �����������
# ��� ������ ����������� ������� ���������.
#
# �������� �� ���������: -O$(STD_OLEVEL).
STD_OFLAG  ?= $(if $(filter build,$(PLATFORM)),$(BUILD_OFLAG),-O$(STD_OLEVEL))

# .. lang=ru
# ����� ����������� ����� ��
# ��� ������� ���������.
STD_CFLAGS += -pipe $(STD_OFLAG)
STD_CFLAGS += $(if $(WARN),-Wall -W,-w)
STD_CFLAGS += $(if $(DEBUG),-g,-g0)
STD_CFLAGS += $(if $(PROFILE),-pg)


ifneq ($(PLATFORM),build)
  ifdef EXTERN_TOOLCHAIN
    STD_CFLAGS  += $(GCC_ENDIAN_FLAG)
    ifdef GLD_ENDIAN_FLAG
      STD_CFLAGS  += -Wl,$(GLD_ENDIAN_FLAG)
    endif
  endif
endif

# .. lang=ru
# ����� ������������� ����� ��
# ��� ������� ���������.
STD_CPPFLAGS = $(STD_IDIRSFLAGS)

# .. lang=ru
# ����� ������������� ����� ��
# ��� ��������� ���������.
BUILD_CPPFLAGS = $(BUILD_IDIRSFLAGS)

# .. lang=ru
# ����� :manpage:`ld(1)`
# ��� ������� ���������.
STD_LDFLAGS ?= $(STD_LDIRSFLAGS)

# .. lang=ru
# ����� :manpage:`ld(1)`
# ��� ��������� ���������.
BUILD_LDFLAGS ?= $(BUILD_LDIRSFLAGS)


ifneq ($(PLATFORM),build)
  ifeq ($(shell $(VERCMP) $(BINUTILS_VERSION) '>=' 2.22),true)
    STD_LDFLAGS += -Wl,--copy-dt-needed-entries
  endif
endif

STD_LDFLAGS  += $(if $(PROFILE),-pg)


# .. lang=ru
# ���������� �� ��������� ������ :manpage:`cc(1)`
BUILD_CC      ?= gcc

# .. lang=ru
# �������� ���������� �� ��������� ������ :manpage:`cc(1)`
BUILD_CC_     ?= /usr/bin/gcc

# .. lang=ru
# ������������ �� ��������� ������ :manpage:`cpp(1)`
BUILD_CPP     ?= "$(BUILD_CC) -E"

# .. lang=ru
# ���������� ��++ ��������� ������ :manpage:`c++(1)`
BUILD_CXX     ?= g++

# .. lang=ru
# ��������� ��������� ������ :manpage:`as(1)`
BUILD_AS      ?= $(shell $(BUILD_CC_) -print-prog-name=as)

# .. lang=ru
# :manpage:`ld(1)` ��������� ������
BUILD_LD      ?= $(shell $(BUILD_CC_) -print-prog-name=ld)

# .. lang=ru
# :manpage:`ar(1)` ��������� ������
BUILD_AR      ?= $(shell $(BUILD_CC_) -print-prog-name=ar)

# .. lang=ru
# :manpage:`nm(1)` ��������� ������
BUILD_NM      ?= $(shell $(BUILD_CC_) -print-prog-name=nm)

# .. lang=ru
# :manpage:`ranlib(1)` ��������� ������
BUILD_RANLIB  ?= $(shell $(BUILD_CC_) -print-prog-name=ranlib)

# .. lang=ru
# :manpage:`readelf(1)` ��������� ������
BUILD_READELF  ?= $(shell $(BUILD_CC_) -print-prog-name=readelf)

# .. lang=ru
# :manpage:`strip(1)` ��������� ������
BUILD_STRIP   ?= $(shell $(BUILD_CC_) -print-prog-name=strip)

# .. lang=ru
# :manpage:`objcopy(1)` ��������� ������
BUILD_OBJCOPY ?= $(shell $(BUILD_CC_) -print-prog-name=objcopy)

# .. lang=ru
# :manpage:`objcopy(1)` ��������� ������
BUILD_OBJDUMP ?= $(shell $(BUILD_CC_) -print-prog-name=objdump)


ifeq ($(PLATFORM),build)
      CC=$(BUILD_CC)
     CXX=$(BUILD_CXX)
     CPP=$(BUILD_CPP)
      AS=$(BUILD_AS)
      LD=$(BUILD_LD)
      AR=$(BUILD_AR)
      NM=$(BUILD_NM)
  RANLIB=$(BUILD_RANLIB)
 READELF=$(BUILD_READELF)
   STRIP=$(BUILD_STRIP)
 OBJCOPY=$(BUILD_OBJCOPY)
 OBJDUMP=$(BUILD_OBJDUMP)
else
# .. lang=ru
# ���������� �� ������� ��������� :manpage:`cc(1)`
      CC=$(CROSS_PREFIX)gcc
# .. lang=ru
# ���������� �� ������� ��������� ��� mangling :manpage:`cc(1)`
      CC_=$(CC)__
# .. lang=ru
# ������������ �� ������� ��������� :manpage:`cpp(1)`
     CPP=$(CC) -E

ifneq ($(GCC_VERSION),$(GCCK_VERSION))
     KCROSS_PREFIX = $(tc_PREFIX)/kcc/bin/$(TARGET)-
     KCC = $(KCROSS_PREFIX)gcc
else
     KCC = $(CC)
endif

# .. lang=ru
# ���������� ��++ ������� ��������� :manpage:`c++(1)`
     CXX=$(CROSS_PREFIX)g++
# .. lang=ru
# :manpage:`ld(1)` ������� ���������
      LD=$(CROSS_PREFIX)ld
# .. lang=ru
# ��������� ������� ��������� :manpage:`as(1)`
      AS=$(CROSS_PREFIX)as
# .. lang=ru
# :manpage:`ar(1)` ������� ���������
      AR=$(CROSS_PREFIX)ar
# .. lang=ru
# :manpage:`nm(1)` ������� ���������
      NM=$(CROSS_PREFIX)nm
# .. lang=ru
# :manpage:`ranlib(1)` ������� ���������
  RANLIB=$(CROSS_PREFIX)ranlib
# .. lang=ru
# :manpage:`ranlib(1)` ������� ���������
 READELF=$(CROSS_PREFIX)readelf
# .. lang=ru
# :manpage:`strip(1)` ������� ���������
   STRIP=$(CROSS_PREFIX)strip
# .. lang=ru
# :manpage:`objcopy(1)` ������� ���������
 OBJCOPY=$(CROSS_PREFIX)objcopy

# .. lang=ru
# :manpage:`objdump(1)` ������� ���������
 OBJDUMP=$(CROSS_PREFIX)objdump
endif


# .. lang=ru
# ��� ����� ������� ���������.
# ����� ���������� ��� ��������� :manpage:`make(1)`, ��� � � ���������� �����.
STD_VARS  =       CC=$(CC)
STD_VARS +=       AS=$(AS)
#STD_VARS +=      CPP="$(CPP)"
STD_VARS +=      CXX=$(CXX)
STD_VARS +=       LD=$(LD)
STD_VARS +=       AR=$(AR)
STD_VARS +=       NM=$(NM)
STD_VARS +=   RANLIB=$(RANLIB)
STD_VARS +=  READELF=$(READELF)
STD_VARS +=    STRIP=$(STRIP)
STD_VARS +=  OBJCOPY=$(OBJCOPY)
STD_VARS +=  OBJDUMP=$(OBJDUMP)

STD_VARS +=  BUILD_CC=$(BUILD_CC)
STD_VARS += BUILD_CPP=$(BUILD_CPP)
STD_VARS += BUILD_CXX=$(BUILD_CXX)
STD_VARS +=    HOSTCC=$(BUILD_CC)
STD_VARS +=   HOSTCPP=$(BUILD_CPP)
STD_VARS +=   HOSTCXX=$(BUILD_CXX)

STD_VARS += CPPFLAGS='$(STD_CPPFLAGS)'
STD_VARS +=   CFLAGS='$(STD_CFLAGS)'
STD_VARS +=  LDFLAGS='$(STD_LDFLAGS)'
STD_VARS += PKG_CONFIG=$(PKG_CONFIG)
STD_VARS += PKG_CONFIG_PATH=$(PKG_CONFIG_PATH)


include $(TOOLS_DIR)/strip-flags.mk


# why BUILD_CC* is exported in deep recursive make?
unexport
unexport BUILD_CFLAGS BUILD_CPPFLAGS BUILD_LDFLAGS
unexport BUILD_CC  BUILD_CPP  BUILD_CXX
unexport BUILD_CC_ BUILD_CPP_ BUILD_CXX_
unexport CC CPP CXX KCC
unexport LD AS AR NM RANLIB READELF STRIP


endif # tools/build-flags.mk
