ifndef	tools/make/conf.mk
	tools/make/conf.mk = included


configure : $(call tstamp_f,link,makes)

local_mk = $(PKG_BLD_SUB_DIR)/local.mk

MAKES_MAKEFILE ?= $(PKG_SRC_DIR)/Makefile

ifeq       ($(BUILD_TYPE),LIB)
  LIB_NAME   ?= $(error LIB_NAME undefined)
else ifeq ($(BUILD_TYPE),SHARED_LIB)
  LIB_NAME   ?= $(error LIB_NAME undefined)
else ifeq ($(BUILD_TYPE),MULTI_SRC_BIN)
  BIN_NAME   ?= $(error BIN_NAME undefined)
else ifeq ($(BUILD_TYPE),SINGLE_SRC_BINS)
else
  $(error BUILD_TYPE undefined)
endif

ifdef SIGNATURES
  STD_CPPFLAGS += -I$(PKG_BLD_DIR)
endif

define MAKES_CONFIGURE_CMD
  echo "BUILD_TYPE   = $(BUILD_TYPE)"                  >> $(local_mk)
  echo "TOP_DIR      = $(BLD_DIR)"                     >> $(local_mk)
  echo "SRC_DIR      = $(PKG_ORIG_DIR)/$(PKG_SUB_DIR)" >> $(local_mk)
  echo "BLD_DIR      = $(PKG_BLD_SUB_DIR)"             >> $(local_mk)
  echo "CROSS_PREFIX = $(CROSS_PREFIX)"                >> $(local_mk)
  echo " CFLAGS      = $(STD_CFLAGS) $(STD_CPPFLAGS)"  >> $(local_mk)
  echo "CXXFLAGS     = $(STD_CFLAGS) $(STD_CPPFLAGS)"  >> $(local_mk)
  echo "LDFLAGS      = $(STD_LDFLAGS)"                 >> $(local_mk)
  echo "PREFIX       = $(INST_DIR)"                    >> $(local_mk)
  echo "ROOT_PREFIX  = $(ROOT_DIR)"                    >> $(local_mk)
  echo "BIN_DIR      = $(BIN_DIR)"                     >> $(local_mk)

  $(LN_S) $(MAKES_MAKEFILE) $(PKG_BLD_SUB_DIR)/Makefile $(LOG);
endef

ifneq ($(BIN_NAME),)
  MAKES_CONFIGURE_CMD += echo "BIN_NAME     = $(BIN_NAME)"                   >> $(local_mk);
endif

ifneq ($(LIB_NAME),)
  MAKES_CONFIGURE_CMD += echo "LIB_NAME     = $(LIB_NAME)"                   >> $(local_mk);
endif

ifneq ($(LIBS),)
  MAKES_CONFIGURE_CMD += echo "LIBS         = $(LIBS)"                       >> $(local_mk);
endif

ifneq ($(CFLAGS_),)
  MAKES_CONFIGURE_CMD += echo "CFLAGS_     += $(CFLAGS_)"                    >> $(local_mk);
endif

ifdef SIGNATURES
  MAKES_CONFIGURE_CMD += ctags-struct $(PKG_ORIG_DIR) | ctags-struct-parse > $(PKG_BLD_DIR)/$(PKG)-signatures.h
endif


endif # tools/make/conf.mk
