defs_make := $(or \
	$(wildcard       ../makes/defs.make),\
	$(wildcard    ../../makes/defs.make),\
	$(wildcard ../../../makes/defs.make))
MAKES_DIR  = $(dir $(defs_make))

TOP_DIR   ?= $(MAKES_DIR)/..

include $(MAKES_DIR)/defs.make


BUILD_TYPE ?= $(error BUILD_TYPE undefined)

ifeq       ($(BUILD_TYPE),LIB)
  LIB_NAME   ?= $(error LIB_NAME undefined)
else ifeq ($(BUILD_TYPE),SHARED_LIB)
  LIB_NAME   ?= $(error LIB_NAME undefined)
else ifeq ($(BUILD_TYPE),MULTI_SRC_BIN)
  BIN_NAME   ?= $(error BIN_NAME undefined)
else ifeq ($(BUILD_TYPE),SINGLE_SRC_BINS)
endif


include $(MAKES_DIR)/rules.make
