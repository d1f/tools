ifndef	tools/makes/rules.make
	tools/makes/rules.make = included


ifndef       tools/makes/defs.make
include $(TOP_DIR)/makes/defs.make
endif

# Q= in your Makefile to see compilation lines
Q ?= @


#+ to shortcut dependency chain checking on source files
#$(SRCS):;
Makefile:;
%.make:;
%.mk:;
#- to shortcut dependency chain checking on source files


# directory creating rule
%/.dir:
	@echo "> Making directory $@"
	$(Q)$(MKDIR_P) $(dir $@)
	$(Q)touch $@


etags: TAGS

TAGS: $(SRCS) $(HDRS)
	etags $^

obj objs: $(OBJS)

lib libs:



pre_subdirs:
ifdef PRE_SUBDIRS
	$(Q)for d in $(PRE_SUBDIRS); do \
		if test -d $$d; then $(MAKE) -C $$d $(MAKECMDGOALS) || exit $?; fi; \
	done
endif

post_subdirs:
ifdef POST_SUBDIRS
	$(Q)for d in $(POST_SUBDIRS); do \
		if test -d $$d; then $(MAKE) -C $$d $(MAKECMDGOALS) || exit $?; fi; \
	done
endif



ifneq ($(filter LIB SHARED_LIB,$(BUILD_TYPE)),)

all: pre_subdirs lib post_subdirs

lib libs: $(lib_file)

$(lib_file): $(OBJS) #$(DEP_FILES)

CLEAN_FILES += lib$(LIB_NAME)

all lib:

endif # BUILD_TYPE == LIB | SHARED_LIB



ifeq ($(BUILD_TYPE),LIB)

$(lib_file): $(OBJS)
	@echo "> Making library $@"
	$(Q)$(AR) rcsu $@ $^

endif	# BUILD_TYPE == LIB


ifeq ($(BUILD_TYPE),SHARED_LIB)

$(lib_file): $(OBJS)
	@echo "> Linking    $@"
	$(Q)$(CC) -o $@ $^ $(ALL_LDFLAGS)

endif	# BUILD_TYPE == SHARED_LIB



ifeq ($(BUILD_TYPE),SINGLE_SRC_BINS)

$(patsubst %.c.o,%,$(filter %.c.o,$(OBJS))): %: %.c.o #$(DEP_FILES)
	@echo "> Linking    $@"
	$(Q)$(CC) -o $@ $^ $(ALL_LDFLAGS)

$(patsubst %.cc.o,%,$(filter %.cc.o,$(OBJS))): %: %.cc.o #$(DEP_FILES)
	@echo "> Linking    $@"
	$(Q)$(CXX) -o $@ $^ $(ALL_LDFLAGS)

$(patsubst %.cpp.o,%,$(filter %.cpp.o,$(OBJS))): %: %.cpp.o #$(DEP_FILES)
	@echo "> Linking    $@"
	$(Q)$(CXX) -o $@ $^ $(ALL_LDFLAGS)

$(patsubst %.cxx.o,%,$(filter %.cxx.o,$(OBJS))): %: %.cxx.o #$(DEP_FILES)
	@echo "> Linking    $@"
	$(Q)$(CXX) -o $@ $^ $(ALL_LDFLAGS)

$(patsubst %.C.o,%,$(filter %.C.o,$(OBJS))): %: %.C.o #$(DEP_FILES)
	@echo "> Linking    $@"
	$(Q)$(CXX) -o $@ $^ $(ALL_LDFLAGS)

all:	pre_subdirs $(BIN_NAMES) post_subdirs

  INST_BIN_FILES ?= $(BIN_NAMES)

endif	# BUILD_TYPE == SINGLE_SRC_BINS


ifeq ($(BUILD_TYPE),MULTI_SRC_BIN)

 ifneq ($(filter %.cc.o %.cpp.o %.cxx.o %.C.o,$(OBJS)),)
  _ld_ = $(CXX)
 else ifneq ($(filter %.c.o,$(OBJS)),)
  _ld_ = $(CC)
 else
  _ld_ = $(CC)
 endif

all:	pre_subdirs $(BIN_NAME) post_subdirs

$(BIN_NAME)  : $(OBJS) #$(DEP_FILES)
	@echo "> Linking    $@"
	$(Q)$(_ld_) -o $@ $^ $(ALL_LDFLAGS)

  INST_BIN_FILES ?= $(BIN_NAME)

endif	# BUILD_TYPE == MULTI_SRC_BIN



ifeq ($(filter LKMOD,$(BUILD_TYPE)),LKMOD)

lkmodld_flags  = -Ur --sort-common --warn-common #--warn-section-align
lkmodld_flags += $(ALL_LDFLAGS)

all: pre_subdirs $(OBJS) lkmod post_subdirs

lkmod: $(lkmod_file)

$(lkmod_file): $(OBJS) $(DEP_FILES)
	@echo "> Linking    $@"
	$(Q)$(LD) $(lkmodld_flags) -o $@ \
		$(filter %begin.c.o,$(OBJS)) \
		$(filter-out %begin.c.o %end.c.o $(lkmod_file),$(OBJS)) \
		$(filter   %end.c.o,$(OBJS))

# 2.1.18 == 131346
%.ver: %.c $(DEP_FILES)
	@echo "> Generating $@"
	$(Q)$(CPP) $< $(all_cpp_flags) -D__GENKSYMS__ | \
	if test `expr $(lkversion_major) \* 65536 + $(lkversion_minor) \* 256 + $(lkversion_revision)` -lt 131346; then \
		$(GENKSYMS) $(genksyms_smp_prefix) -k $(lkversion_base) . && $(MV) $(notdir $@) $(notdir $@).ver && $(MV) $(notdir $@).ver $@; \
	else \
		$(GENKSYMS) $(genksyms_smp_prefix) -k $(lkversion_base) >$@; \
	fi

endif	# BUILD_TYPE == LKMOD


%.c.o: %.c $(DEP_FILES)
	@echo "> Compiling  $<"
	$(Q)$(CC)  -c $< -o $@ $(ALL_CFLAGS)

%.cc.o: %.cc $(DEP_FILES)
	@echo "> Compiling  $<"
	$(Q)$(CXX) -c $< -o $@ $(ALL_CXXFLAGS)

%.cpp.o: %.cpp $(DEP_FILES)
	@echo "> Compiling  $<"
	$(Q)$(CXX) -c $< -o $@ $(ALL_CXXFLAGS)

%.cxx.o: %.cxx $(DEP_FILES)
	@echo "> Compiling  $<"
	$(Q)$(CXX) -c $< -o $@ $(ALL_CXXFLAGS)

%.C.o: %.C $(DEP_FILES)
	@echo "> Compiling  $<"
	$(Q)$(CXX) -c $< -o $@ $(ALL_CXXFLAGS)

%.c.lo: %.c $(DEP_FILES)
	@echo "> Compiling  $<"
	$(Q)$(CC)  -c $< -o $@ $(ALL_CFLAGS)

%.cc.lo: %.cc $(DEP_FILES)
	@echo "> Compiling  $<"
	$(Q)$(CXX) -c $< -o $@ $(ALL_CXXFLAGS)

%.cpp.lo: %.cpp $(DEP_FILES)
	@echo "> Compiling  $<"
	$(Q)$(CXX) -c $< -o $@ $(ALL_CXXFLAGS)

%.cxx.lo: %.cxx $(DEP_FILES)
	@echo "> Compiling  $<"
	$(Q)$(CXX) -c $< -o $@ $(ALL_CXXFLAGS)

%.C.lo: %.C $(DEP_FILES)
	@echo "> Compiling  $<"
	$(Q)$(CXX) -c $< -o $@ $(ALL_CXXFLAGS)


#+ dependencies generation

deps = $(addsuffix .dep,$(SRCS))

dep_gen_cmd_tail = -w $< > $@.tmp.dep \
	&& sed '\''s!$(subst .,\.,$*.o) *:!$(patsubst %.dep,%.o,$@) $(patsubst %.dep,%.lo,$@) $@ $(patsubst %.dep,%.s,$@):!g'\'' \
	   < $@.tmp.dep > $@.tmp2.dep \
	&& $(MV) $@.tmp2.dep $@ && $(RM) $@.tmp.dep; } \
	|| $(RM) $@ $@.tmp.dep $@.tmp.dep

  c_deps_gen_cmd = $(SHELL) -ec '{ $(CC)  -D__GENDEPS__ -M $(ALL_CFLAGS)   $(dep_gen_cmd_tail)'
 cc_deps_gen_cmd = $(SHELL) -ec '{ $(CC)  -D__GENDEPS__ -M $(ALL_CXXFLAGS) $(dep_gen_cmd_tail)'


%.c.dep: %.c $(DEP_FILES)
	@echo "> Generating dependencies for $<"
	$(Q)$(c_deps_gen_cmd)

%.cc.dep: %.cc $(DEP_FILES)
	@echo "> Generating dependencies for $<"
	$(Q)$(cc_deps_gen_cmd)

%.cpp.dep: %.cpp $(DEP_FILES)
	@echo "> Generating dependencies for $<"
	$(Q)$(cc_deps_gen_cmd)

%.cxx.dep: %.cxx $(DEP_FILES)
	@echo "> Generating dependencies for $<"
	$(Q)$(cc_deps_gen_cmd)

%.C.dep: %.C $(DEP_FILES)
	@echo "> Generating dependencies for $<"
	$(Q)$(cc_deps_gen_cmd)

#- dependencies generation


clean: pre_subdirs post_subdirs
	@echo "> Cleaning up"
	-$(Q)$(RM) $(CLEAN_FILES) $(BIN_NAMES) $(BIN_NAME) $(lib_file) \
		$(addprefix $(BLD_DIR)/,*.dep *.ver *.o *.lo *.s *.bak *.core tags *~)

distclean: clean pre_subdirs post_subdirs
ifndef NO_GITIGNORE
	$(Q)$(RM) .gitignore
endif




all: pre_subdirs .gitignore post_subdirs


.gitignore: $(MAKEFILE_LIST) $(SRCS)
ifndef NO_GITIGNORE
	@echo "> Generating .gitignore"
	@echo /.gitignore	 > $@
	@echo /*.dep		>> $@
	@echo /*.o		>> $@
	@echo /*.a		>> $@
	@echo /*.lo		>> $@
	@echo /*.so		>> $@
	@echo /*.so.[1-9]	>> $@
ifneq ($(CLEAN_FILES),)
	@echo /$(CLEAN_FILES)	>> $@
endif
ifneq ($(BIN_NAMES),)
	@echo /$(BIN_NAMES)	>> $@
endif
ifneq ($(BIN_NAME),)
	@echo /$(BIN_NAME)	>> $@
endif
ifdef GITIGNORE_FILES
	@echo $(GITIGNORE_FILES) >> $@
endif
endif

_depclean:
	-$(Q)$(RM) *.dep

depclean depsclean: pre_subdirs post_subdirs
	@echo "> Cleaning up dependencies"
	-$(Q)$(RM) *.dep

depend deps dep: pre_subdirs $(deps) post_subdirs


deps: ccopts.mk
ccopts.mk:
	$(Q)for ccopt in $(C_CXX_OPTS); do \
		if $(CC) $(CFLAGS) $$ccopt -S -o /dev/null -xc /dev/null \
		   > /dev/null 2>&1; then echo "_C_CXX_FLAGS += $$ccopt"; fi >> $@; done

-include ccopts.mk


ifneq ($(filter install,$(MAKECMDGOALS)),)

  ifndef PREFIX
    $(error PREFIX is not defined)
  endif

  INST_HDRS := $(or $(wildcard $(SRC_DIR)/include/*),$(wildcard $(SRC_DIR)/inc/*),$(wildcard $(SRC_DIR)/*.h))

  ifneq ($(filter LIB SHARED_LIB,$(BUILD_TYPE)),)
    ifndef LIB_NAME
      $(error LIB_NAME is not defined)
    endif

    install :
	$(Q)$(MKDIR_P)         $(PREFIX)/lib
	$(Q)$(MKDIR_P)         $(PREFIX)/include
	$(Q)cp -a $(lib_file)  $(PREFIX)/lib/
	$(Q)for h in $(INST_HDRS); do cp -a $$h $(PREFIX)/include/; done
  endif # LIB SHARED_LIB


  ifeq ($(BUILD_TYPE),MULTI_SRC_BIN)
    ifndef BIN_NAME
      $(error BIN_NAME is not defined)
    endif

    install :
	$(Q)$(MKDIR_P)         $(PREFIX)/$(BIN_DIR)
	$(Q)cp -a $(BIN_NAME)  $(PREFIX)/$(BIN_DIR)/
  endif # MULTI_SRC_BIN


  ifeq ($(BUILD_TYPE),SINGLE_SRC_BINS)
    ifndef BIN_NAMES
      $(error BIN_NAMES is not defined)
    endif

    install :
	$(Q)$(MKDIR_P)         $(PREFIX)/$(BIN_DIR)
	$(Q)cp -a $(BIN_NAMES) $(PREFIX)/$(BIN_DIR)/
  endif # SINGLE_SRC_BINS

endif # install

ifneq ($(filter install-root,$(MAKECMDGOALS)),)

  ifndef ROOT_PREFIX
    $(error ROOT_PREFIX is not defined)
  endif

  ifeq ($(BUILD_TYPE),SHARED_LIB)
    ifndef LIB_NAME
      $(error LIB_NAME is not defined)
    endif

    install-root :
	$(Q)$(MKDIR_P)         $(ROOT_PREFIX)/lib
	$(Q)cp -a $(lib_file)  $(ROOT_PREFIX)/lib/
  endif # SHARED_LIB


  ifeq ($(BUILD_TYPE),MULTI_SRC_BIN)
    ifndef BIN_NAME
      $(error BIN_NAME is not defined)
    endif

    install-root :
	$(Q)$(MKDIR_P)         $(ROOT_PREFIX)/$(BIN_DIR)
	$(Q)cp -a $(BIN_NAME)  $(ROOT_PREFIX)/$(BIN_DIR)/
  endif # MULTI_SRC_BIN


  ifeq ($(BUILD_TYPE),SINGLE_SRC_BINS)
    ifndef BIN_NAMES
      $(error BIN_NAMES is not defined)
    endif

    install-root :
	$(Q)$(MKDIR_P)         $(ROOT_PREFIX)/$(BIN_DIR)
	$(Q)cp -a $(BIN_NAMES) $(ROOT_PREFIX)/$(BIN_DIR)/
  endif # SINGLE_SRC_BINS

endif # install-root


ifeq ($(deps),)
  DONT_INCLUDE_DEPS = defined
endif

ifneq ($(filter %clean %_uninstall doc_% include_% .cvsignore dist% none,$(MAKECMDGOALS)),)
  DONT_INCLUDE_DEPS = defined
endif

ifndef DONT_INCLUDE_DEPS
  -include $(deps)
endif


endif # tools/makes/rules.make
