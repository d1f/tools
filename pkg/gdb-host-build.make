# This builds cross-gdb: run on build and accept target core and binaries
GDB_HOST   = $(BUILD)
GDB_PREFIX = $(tc_PREFIX)

configure : $(call bstamp_f,install,ncurses)

INSTALL_CMD = $(INSTALL_CMD_DEFAULT)

include $(TOP_DIR)/tools/pkg/gdb.mk
