ifneq ($(strip $(shell which $(PKG) 2>/dev/null)),)

PKG_HAS_NO_SOURCES = defined

else # has no help2man

PKG_TYPE          = DEBIAN_NATIVE
PKG_VERSION       = 1.47.4
PKG_SITE_PATH     = pool/main/h/$(PKG_BASE)

PLATFORM          = build

include $(DEFS)


configure : $(call bstamp_f,install,gawk)

# CFLAGS can be needed for $(preload).so
define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) && \
     ./configure -C $(LOG)	\
	--prefix=$(INST_DIR)	\
	--disable-nls		\
	$(STD_VARS)
endef


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
  INSTALL_CMD =   $(INSTALL_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)


endif # has no help2man

include $(RULES)
