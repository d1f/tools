# only for fetching, extracting, patching

PKG_TYPE      = ORIGIN
PKG_VERSION   = $(GCCK_VERSION)
PKG_SITES     = $(GNU_ORG_SITES)
PKG_SITE_PATH = gcc/gcc-$(PKG_VERSION)
gcc_base      = $(if $(filter 2.95% 3.0,$(PKG_VERSION)),gcc-everything,gcc)
PKG_FILE_BASE = $(gcc_base)-$(PKG_VERSION)

SEPARATE_SRC_BLD_DIRS = defined

include $(DEFS)


ifdef GCC_PLATFORM_PATCHES
  PKG_PATCH_FILES += $(GCC_PLATFORM_PATCHES)
else
  include $(TOOLS_PKG_DIR)/crosstool.mk
  PKG_PATCH_FILES += $(CROSSTOOL_PATCH_DIR)/gcc/$(PKG_VERSION)/*.patch

  gcc_ver_2_3 = $(if $(filter 2.% 3.%,$(PKG_VERSION)),23)
  PKG_PATCH_FILES += $(wildcard $(PKG_PKG_DIR)/gcc/gcc-$(gcc_ver_2_3)_collect2.c_open-mode.patch)
  PKG_PATCH_FILES += $(wildcard $(PKG_PKG_DIR)/gcc/gcc-$(PKG_VERSION)*.patch)
endif # ndef GCC_PLATFORM_PATCHES


include $(RULES)
