ifdef NOCCACHE

PKG_HAS_NO_SOURCES = defined
include $(RULES)

else # ifndef NOCCACHE

ccache := $(strip $(shell which ccache 2>/dev/null))
ifneq ($(ccache),)

PKG_HAS_NO_SOURCES = defined
include $(DEFS)

define INSTALL_CMD
  $(IW) $(LN_S) $(ccache) $(BINST_DIR)/bin/cpp $(LOG)
  $(IW) $(LN_S) $(ccache) $(BINST_DIR)/bin/gcc $(LOG)
  $(IW) $(LN_S) $(ccache) $(BINST_DIR)/bin/cc  $(LOG)
  $(IW) $(LN_S) $(ccache) $(BINST_DIR)/bin/g++ $(LOG)
  $(IW) $(LN_S) $(ccache) $(BINST_DIR)/bin/c++ $(LOG)
endef

include $(RULES)

else # has ccache


PKG_TYPE      = ORIGIN
PKG_VERSION   = 3.3.3
PKG_SITES     = http://samba.org
PKG_SITE_PATH = ftp/ccache
PKG_REMOTE_FILE_SUFFICES = .tar.gz # bootstrap
SEPARATE_SRC_BLD_DIRS=defined
PLATFORM      = build

include $(DEFS)


define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) && \
     $(PKG_SRC_DIR)/configure -C $(LOG)	\
	--prefix=$(BINST_DIR)		\
	--with-bundled-zlib		\
	$(STD_VARS)			\
	CC=$(BUILD_CC_)			\
	CFLAGS="-pipe -O3"
endef


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)
  INSTALL_CMD =   $(INSTALL_CMD_DEFAULT)

define POST_INSTALL_CMD
  $(IW) $(LN_S) ccache $(BINST_DIR)/bin/cpp $(LOG)
  $(IW) $(LN_S) ccache $(BINST_DIR)/bin/gcc $(LOG)
  $(IW) $(LN_S) ccache $(BINST_DIR)/bin/cc  $(LOG)
  $(IW) $(LN_S) ccache $(BINST_DIR)/bin/g++ $(LOG)
  $(IW) $(LN_S) ccache $(BINST_DIR)/bin/c++ $(LOG)
endef


# CCACHE_BASEDIR
#	rewrites absolute paths into relative paths before computing the hash
#	that identifies the compilation, but only for paths under the specified directory.
# CCACHE_DIR
#	where ccache will keep its cached compiler outputs.
# CCACHE_CC
#	to force the name of the compiler to use.
# CCACHE_PATH
#	ccache will search directories in this list when looking for the real compiler.
#	The list separator is ':'.
#	If not set, ccache will look for the first executable matching the compiler name
#	in the normal PATH that is not a symbolic link to ccache itself.
# CCACHE_PREFIX
#	list of prefixes (separated by space) to the command line
#	that ccache uses when invoking the compiler.
# CCACHE_TEMPDIR
#	where ccache will put temporary files. The default is <cache_dir>/tmp.


endif # has ccache
endif # ifndef NOCCACHE

include $(RULES)
