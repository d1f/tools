ifndef	tools/pkg/configure-menuconfig.mk
	tools/pkg/configure-menuconfig.mk = included


# Usage example:
define configure-menuconfig.mk-usage

include $(TOOLS_PKG_DIR)/configure-menuconfig.mk


define CONFIGURE_CMD # example only!
  cd $(PKG_BLD_DIR)	&&	\
     ./configure -C $(LOG)	\
	...			\
	$(CONFIGURE_OPTIONS)	\
	$(STD_VARS)
endef

endef


menuconfig : $(call bstamp_f,install,mconf)

ifndef PRE_CONFIGURE_CMD
define PRE_CONFIGURE_CMD
  $(CONFIGURE_PARSE_CMD)
  $(CONFIGURE_OPTS_CMD)
endef
endif

ifndef MENUCONFIG_CMD
define MENUCONFIG_CMD
  $(CONFIGURE_PARSE_CMD)

  echo "$(I1)Running menuconfig"
  KCONFIG_CONFIG=$(PKG_MENUCONFIG_FILE) $(BBIN_DIR)/mconf $(PKG_BLD_SUB_DIR)/Config.in

  $(RM) $(PKG_MENUCONFIG_FILE).old
  echo "$(I1)done"
endef
endif

ifndef CONFIGURE_PARSE_CMD
define CONFIGURE_PARSE_CMD
  echo -n "$(I1)Generate Config.in and Config.list ... "
  $(PKG_BLD_SUB_DIR)/configure --help | 	\
	$(TOOLS_DIR)/script/configure-parse	\
		$(PKG_BLD_SUB_DIR)/Config.in	\
		$(PKG_BLD_SUB_DIR)/Config.list
  echo done
endef
endif

MENUCONFIG_PARSE_CMD = $(TOOLS_DIR)/script/menuconfig-parse	\
				$(PKG_MENUCONFIG_FILE)		\
				$(PKG_BLD_SUB_DIR)/Config.list

define CONFIGURE_OPTS_CMD
  echo -n "$(I1)Generate Configure options ... "
  echo -n "CONFIGURE_OPTIONS = "  > $(PKG_BLD_SUB_DIR)/Configure.mk
  $(MENUCONFIG_PARSE_CMD) >> $(PKG_BLD_SUB_DIR)/Configure.mk
  echo done
endef


ifneq ($(filter conf% build bld inst% pack,$(MAKECMDGOALS)),)
-include $(PKG_BLD_SUB_DIR)/Configure.mk
endif

$(PKG_BLD_SUB_DIR)/Configure.mk : pre-configure


endif # tools/pkg/configure-menuconfig.mk
