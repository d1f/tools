PKG_TYPE      = ORIGIN
PKG_VERSION   = 0.13.0.4
PKG_SITES     = http://www.colm.net
PKG_SITE_PATH = files/colm

PLATFORM      = build

include $(DEFS)


define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) &&			\
     ./configure -C	$(LOG)		\
	--prefix=$(INST_DIR)		\
	$(STD_VARS)

  $(RM) $(PKG_BLD_DIR)/src/version.h $(LOG)
endef


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)
  INSTALL_CMD =   $(INSTALL_CMD_DEFAULT)


include $(RULES)
