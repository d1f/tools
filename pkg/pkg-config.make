PKG_TYPE        = DEBIAN1

ifeq ($(PKG_TYPE),DEBIAN1)
  PKG_VERSION   = 0.29
  PKG_VER_PATCH = -4
  PKG_SITE_PATH = pool/main/p/$(PKG_BASE)
else
  PKG_VERSION   = 0.29.1
  PKG_SITES     = http://www.freedesktop.org/software/pkgconfig/releases
endif

include $(DEFS)


configure : $(call bstamp_f,install,ccache gawk sed)

ifeq ($(PLATFORM),build)
  program_prefix=
else
  program_prefix=$(TARGET)-
endif

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) && ./configure -C	$(LOG)	\
	--prefix=$(BINST_DIR)			\
	--program-prefix=$(program_prefix)	\
	--disable-host-tool			\
	--with-gnu-ld				\
	--with-pc-path="$(PKG_CONFIG_PATH)"	\
	--with-internal-glib			\
	--disable-debug				\
	--disable-mem-pools			\
	--disable-rebuilds			\
	--with-python=/bin/true			\
	--without-libiconv			\
	CC=$(BUILD_CC)
endef


    MK_VARS  = ACLOCAL=true AUTOMAKE=true AUTOCONF=true AUTOHEADER=true
    MK_VARS += MAKEINFO=true YLWRAP=true
    MK_VARS += GLIB_WARN_CFLAGS=
    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
  INSTALL_CMD =   $(INSTALL_CMD_DEFAULT)

DISTCLEANMK_VARS = DIST_SUBDIRS="popt check"
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)


include $(RULES)
