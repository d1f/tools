ifneq ($(strip $(shell which $(PKG) 2>/dev/null))$(PLATFORM),build)

PKG_HAS_NO_SOURCES = defined

else  # has no curl

PKG_TYPE         = ORIGIN
PKG_VERSION      = 7.52.1
PKG_ORIG_DIR     = $(TOOLS_SRC_DIR)/$(PKG)
PKG_FETCH_METHOD = DONE

include $(DEFS)


#PKG_PATCH_FILES = $(PKG_PKG_DIR)/patches/*.patch


configure : $(call tstamp_f,install,toolchain)

 enable =
disable =
with    =
without =

disable += debug
 enable += optimize
disable += warnings
disable += werror
disable += curldebug
 enable += symbol-hiding
disable += ares
disable += rt
disable += shared
 enable += http
 enable += ftp
disable += file
disable += ldap
disable += ldaps
disable += rtsp
 enable += proxy
disable += dict
disable += telnet
disable += tftp
disable += pop3
disable += imap
disable += smb
disable += smtp
disable += gopher
disable += manual
disable += libcurl-option
#enable += libgcc
disable += ipv6
disable += versioned-symbols
 enable += threaded-resolver
 enable += verbose
disable += sspi
disable += crypto-auth
disable += ntlm-wb
disable += tls-srp
disable += unix-sockets
 enable += cookies
disable += soname-bump
with    += gnu-ld
without += zlib
without += winssl
without += darwinssl
without += ssl
with    += random=/dev/urandom
without += gnutls
without += polarssl

ifeq ($(PLATFORM),build)
with    += mbedtls=$(INST_DIR)
configure : $(call bstamp_f,install,mbedtls)
else
without += mbedtls
endif

without += cyassl
without += nss
without += axtls
without += ca-bundle
without += ca-path
without += ca-fallback
without += libpsl
without += libmetalink
without += libssh2
without += librtmp
without += winidn
without += libidn
without += nghttp2
without += zsh-functions-dir

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) &&				\
     ./configure -C $(LOG)			\
	--prefix=$(INST_DIR)			\
	--build=$(BUILD)			\
	--host=$(TARGET)			\
	$(addprefix --disable-,$(disable))	\
	$(addprefix --enable-,$(enable))	\
	$(addprefix --with-,$(with))		\
	$(addprefix --without-,$(without))	\
	$(STD_VARS)
endef


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)


define INSTALL_CMD
  $(RELINK) $(PKG_BLD_DIR)/docs/libcurl/libcurl-symbols.3 $(LOG)
  $(INSTALL_CMD_DEFAULT)
endef

INSTALL_ROOT_CMD = $(IW) $(INSTxFILES) $(IBIN_DIR)/curl $(RBIN_DIR) $(LOG)


endif # has curl

include $(RULES)
