ifndef	tools/pkg/crosstool.mk
	tools/pkg/crosstool.mk = included


ifndef xwmake/defs.mk
include $(DEFS)
endif

ifeq ($(filter crosstool%,$(PKG)),)
  patch : $(call tstamp_f,patch,crosstool)
endif


CROSSTOOL_KEGEL_VERSION  = 0.43
CROSSTOOL_NG_153_VERSION = 1.5.3
CROSSTOOL_NG_122_VERSION = 1.22.0
CROSSTOOL_NG_VERSION     = 1.23.0


CROSSTOOL_PATCH_DIR = $(PATCHED_SRC_DIR)/crosstool/patches


endif # tools/pkg/crosstool.mk
