PKG_HAS_NO_SOURCES = defined

include $(DEFS)


include $(TOOLS_PKG_DIR)/mtd-utils.mk

include $(TOOLS_PKG_DIR)/klibc.mk
KLCC = $(INST_KLIBC_DIR)/bin/klcc


pre-configure : $(call tstamp_f,patch, mtd-utils)

define PRE_CONFIGURE_CMD
  echo "$(I1)Linking         $(PKG_NAME) sources before building ... "
  $(RM_R)                                                     $(PKG_BLD_DIR) $(LOG)
  $(LNTREE) $(PATCHED_SRC_DIR)/mtd-utils_$(MTD_UTILS_VERSION) $(PKG_BLD_DIR) $(LOG)
  touch $(PKG_BLD_DIR)/include/features.h $(LOG)
endef


build : $(call tstamp_f,install, toolchain klibc)

STD_LDFLAGS := $(filter-out -rdynamic,$(STD_LDFLAGS))

ENV_VARS = $(STD_VARS)
 MK_VARS = CC=$(KLCC) AR=$(AR) RANLIB=$(RANLIB)
 MK_VARS += BUILDDIR=$(PKG_BLD_DIR)

targets = flash_erase nandwrite #nandcmp #flashcp
instdir = $(INST_KLIBC_DIR)/bin

    BUILD_CMD_TARGET = $(addprefix $(PKG_BLD_DIR)/,$(targets))
    BUILD_CMD = $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =   $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD =   $(CLEAN_CMD_DEFAULT)

  INSTALL_CMD = $(IW) $(INSTxFILES) \
	$(addprefix $(PKG_BLD_DIR)/,$(targets)) $(instdir) $(LOG)


include $(RULES)
