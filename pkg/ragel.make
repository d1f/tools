ifneq ($(strip $(shell which $(PKG) 2>/dev/null)),)

PKG_HAS_NO_SOURCES = defined

else # has ragel

PKG_TYPE      = DEBIAN1
PKG_VERSION   = 6.9
PKG_VER_PATCH = -1.1~bpo8+1
PKG_SITE_PATH = pool/main/r/$(PKG_BASE)

#PKG_TYPE      = ORIGIN
#PKG_VERSION   = 7.0.0.8
#PKG_SITES     = http://www.colm.net
#PKG_SITE_PATH = files/ragel

PLATFORM      = build

include $(DEFS)


#configure : $(call bstamp_f,install,colm)

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) &&			\
     ./configure -C	$(LOG)		\
	--prefix=$(INST_DIR)		\
	--disable-manual		\
	$(STD_VARS)

  $(RM) $(PKG_BLD_DIR)/ragel/version.h $(LOG)
endef

MK_VARS += "CXXFLAGS=-O2 -std=gnu++98"

    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)
  INSTALL_CMD =   $(INSTALL_CMD_DEFAULT)


endif # has ragel

include $(RULES)
