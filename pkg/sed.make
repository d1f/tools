ifneq ($(strip $(shell which $(PKG) 2>/dev/null)),)

PKG_HAS_NO_SOURCES = defined

else # has no $(PKG)

PKG_TYPE          = DEBIAN3
PKG_VERSION       = 4.2.2
PKG_VER_PATCH     = -8
PKG_SITE_PATH     = pool/main/s/$(PKG_BASE)

PLATFORM          = build

include $(DEFS)


define PRE_CONFIGURE_CMD
  $(RM) $(PKG_BLD_DIR)/doc/s-texi $(LOG)
endef

configure : $(call bstamp_f,install,ccache gawk)

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) &&			\
     ./configure -C $(LOG)		\
	--prefix=$(INST_DIR)		\
	--disable-acl			\
	--disable-nls			\
	--disable-i18n			\
	--disable-regex-tests		\
	--without-selinux		\
	--with-gnu-ld			\
	--without-libiconv-prefix	\
	--without-libintl-prefix	\
	$(STD_VARS)
endef

    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
  INSTALL_CMD =   $(INSTALL_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)


endif # has no $(PKG)

include $(RULES)
