# newer installwatch from checkinstall

PKG_FETCH_METHOD = DONE
PKG_ORIG_DIR     = $(TOOLS_SRC_DIR)/$(PKG)
PKG_SUB_DIR      = installwatch
PLATFORM         = build

# special case - [un]install itself
IWTYPE = no
include $(DEFS)


# common patch dir prefix
patch_pfx = $(PKG_PKG_DIR)/patch

pkg_patch_files  = $(patch_pfx)/installwatch_Makefile_cc-ld-vars.patch
pkg_patch_files += $(patch_pfx)/installwatch_script_chdir.patch
pkg_patch_files += $(patch_pfx)/checkinstall_script_append-watch-file.patch
pkg_patch_files += $(patch_pfx)/checkinstall_script_bin-pwd.patch
#pkg_patch_files += $(patch_pfx)/checkinstall_glibc-minor.patch
#pkg_patch_files += $(patch_pfx)/checkinstall_glibc-minor2.patch

pkg_patch_log = $(LOG_DIR)/$(PKG).patch.log2


define PATCH_CMD
	@echo "$(I1)Copying         $(PKG_BASE) $(PKG_VERSION) sources for cmd-patching ... "
	@$(XWMAKE_SCRIPT_DIR)/patch-relink-cmd	$(PKG_PATCH_STRIP_NUM)	\
						$(PKG_SRC_SUB_DIR)	\
						$(pkg_patch_files) $(LOG)

	@echo "$(I1)Cmd-Patching    $(PKG_BASE) $(PKG_VERSION) sources ... "
	@$(XWMAKE_SCRIPT_DIR)/patch-cmd		$(pkg_patch_log)	\
						$(PKG_PATCH_STRIP_NUM)	\
						$(PKG_SRC_SUB_DIR)	\
						$(pkg_patch_files) $(LOG) \
	|| { cat $(pkg_patch_log) >> $(LOG_FILE); $(RM) $(pkg_patch_log); exit 1; }
	@    cat $(pkg_patch_log) >> $(LOG_FILE); $(RM) $(pkg_patch_log)
endef


# debian quilt patches
PKG_PATCH3_FILES += $(patch_pfx)/debian-1.6.2-4/*.patch

PKG_PATCH3_FILES += $(patch_pfx)/checkinstall_no-c99.patch
#PKG_PATCH3_FILES += $(patch_pfx)/installwatch_scandir-proto_glibc-2.10.patch
PKG_PATCH3_FILES += $(patch_pfx)/installwatch_inline-proto.patch


libdldirs := $(dir $(shell find /lib /usr/lib -name libdl.so))

MK_VARS = PREFIX=$(INST_DIR)
MK_VARS += CC='$(BUILD_CC_) -pipe -O3 -w'
MK_VARS += LDFLAGS='$(addprefix -L,$(libdldirs))'

    BUILD_CMD = $(BUILD_CMD_DEFAULT)
    CLEAN_CMD = $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(CLEAN_CMD_DEFAULT)


define INSTALL_CMD
  $(MKDIR_P) $(addprefix $(INST_DIR)/,bin lib man/man1)	$(LOG)
  $(INSTALL_CMD_DEFAULT)
endef


define UNINSTALL_CMD
  $(RM) $(BLIB_DIR)/installwatch.so				$(LOG)
  $(RM) $(BBIN_DIR)/installwatch				$(LOG)
  $(RM) $(MMAN_DIR)/man1/installwatch.1				$(LOG)
 -$(RMDIR_P) $(addprefix $(INST_DIR)/,bin lib man/man1)	$(LOG)
endef


include $(RULES)
