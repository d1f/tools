ifneq ($(strip $(shell which $(PKG) 2>/dev/null)),)

PKG_HAS_NO_SOURCES = defined

else # has no m4

PKG_TYPE      = DEBIAN3
PKG_VERSION   = 1.4.17
PKG_VER_PATCH = -5
PKG_SITE_PATH = pool/main/m/$(PKG_BASE)
PLATFORM      = build

SEPARATE_SRC_BLD_DIRS=defined

include $(DEFS)


configure : $(call bstamp_f,install,ccache gawk)

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) &&			\
     $(PKG_SRC_DIR)/configure	$(LOG)	\
	--prefix=$(INST_DIR)		\
	--enable-threads=posix		\
	--disable-assert		\
	--enable-changeword		\
	--with-gnu-ld			\
	--without-libsigsegv-prefix	\
	--without-libpth-prefix		\
	--without-included-regex	\
	$(STD_VARS)
endef

    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
  INSTALL_CMD =   $(INSTALL_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)


endif # has no m4

include $(RULES)
