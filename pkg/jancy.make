PKG_TYPE      = ORIGIN
PKG_VERSION   = 1.8.0
PKG_SITES     = http://tibbo.com
PKG_SITE_PATH = downloads/archive/$(PKG)/$(PKG)-1.8.0
PKG_BASE      = $(PKG)-src
PLATFORM      = build
#SEPARATE_SRC_BLD_DIRS = defined


include $(DEFS)


configure : $(call bstamp_f,install,cmake ragel axl llvm)
ifndef XDEP # FIXME: avoid unrecognized error
configure : $(call bstamp_f,link,bulldozer)
endif

conf_opts  = -DCMAKE_INSTALL_PREFIX=$(INST_DIR)
conf_opts += -DRAGEL_EXE=$(INST_DIR)/bin/ragel
#conf_opts +=  -DPERL_EXE=$(INST_DIR)/bin/perl
conf_opts += -DCMAKE_BUILD_TYPE=Release
conf_opts += -DLLVM_LIB_DIR=$(ILIB_DIR)
conf_opts += -DLLVM_INC_DIR=$(IINC_DIR)
conf_opts += -DLLVM_CMAKE_DIR=$(BLD_DIR)/llvm/share/llvm/cmake
conf_opts += -DAXL_CMAKE_DIR=$(BLD_DIR)/axl-src/cmake
conf_opts += -DBULLDOZER_CMAKE_DIR=$(BLD_DIR)/bulldozer/cmake

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) && \
     $(BBIN_DIR)/cmake $(PKG_BLD_DIR) $(conf_opts) $(LOG)
endef


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)
  INSTALL_CMD =   $(INSTALL_CMD_DEFAULT)


include $(RULES)
