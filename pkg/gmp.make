PKG_TYPE      = DEBIAN3
pkg_version   = 6.1.2
PKG_VERSION   = $(pkg_version)+dfsg
PKG_VER_PATCH = -1
PKG_SITE_PATH = pool/main/g/$(PKG_BASE)
PLATFORM      = build

include $(DEFS)


define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) &&		\
     ./configure $(LOG)		\
	--prefix=$(INST_DIR)	\
	--disable-shared	\
	--without-readline	\
	--without-pic		\
	--with-gnu-ld		\
	$(STD_VARS)
endef


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)
  INSTALL_CMD =   $(INSTALL_CMD_DEFAULT)


include $(RULES)
