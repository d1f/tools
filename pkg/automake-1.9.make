AC_VER = 2.59
AM_VER = 1.9

PKG_TYPE      = DEBIAN1
PKG_VER_MAJOR = $(AM_VER)
PKG_VER_MINOR = 6+nogfdl
PKG_VER_PATCH = -4
PKG_BASE      = automake$(PKG_VER_MAJOR)
PKG_SITE_PATH = pool/main/a/$(PKG_BASE)

PLATFORM      = build

include $(DEFS)


POST_EXTRACT_CMD = $(RM) $(addprefix $(PKG_ORIG_SRC_DIR)/doc/,\
	version.texi stamp-vti) $(LOG)


include $(TOOLS_PKG_DIR)/autoconf.mk
include $(TOOLS_PKG_DIR)/automake.mk

define CONFIGURE_CMD
  $(RELINK) $(PKG_BLD_DIR)/configure
  cd     $(PKG_BLD_DIR) && ./configure    $(LOG) \
			--prefix=$(AM_PREFIX) $(AC_VARS)
  touch -c $(PKG_BLD_DIR)/Makefile.am
  touch -c $(PKG_BLD_DIR)/Makefile.in
  touch -c $(PKG_BLD_DIR)/Makefile
  touch -c $(PKG_BLD_DIR)/automake.in
endef


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)


define INSTALL_CMD
  $(INSTALL_CMD_DEFAULT)
  $(IWSH) "cd $(AM_PREFIX)/share && ln -s aclocal-$(AM_VER) aclocal" $(LOG)
  $(IWSH) "echo $(BSHARE_DIR)/aclocal > $(AM_PREFIX)/share/aclocal-$(AM_VER)/dirlist" $(LOG)
endef


include $(RULES)
