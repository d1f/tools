PKG_TYPE      = DEBIAN3
pkg_ver_major = 0.1
pkg_ver_minor = ~r459
PKG_VERSION   = $(pkg_ver_major)$(pkg_ver_minor)
PKG_VER_PATCH = -4
PKG_SITE_PATH = pool/main/libm/$(PKG_BASE)
PLATFORM      = build

include $(DEFS)


define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) &&		\
     ./configure $(LOG)		\
	--prefix=$(INST_DIR)	\
	--disable-shared	\
	--without-pic		\
	--with-gnu-ld		\
	$(STD_VARS)
endef


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)
  INSTALL_CMD =   $(INSTALL_CMD_DEFAULT)


include $(RULES)
