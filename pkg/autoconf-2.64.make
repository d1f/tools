AC_VER        = 2.64

PKG_TYPE      = DEBIAN3
PKG_VER_MAJOR = $(AC_VER)
PKG_VERSION   = $(AC_VER)+dfsg
PKG_VER_PATCH = -1
PKG_BASE      = autoconf$(PKG_VER_MAJOR)

include $(TOP_DIR)/tools/pkg/autoconf-common.mk
