PKG_TYPE          = DEBIAN_NATIVE
PKG_VERSION       = 2.0.38
PKG_SITE_PATH     = pool/main/d/$(PKG_BASE)

PLATFORM          = build

include $(DEFS)


PKG_PATCH_FILES = $(PKG_PKG_DIR)/*.patch

       patch : $(call bstamp_f,install,sed gawk)
define PATCH_CMD
  set +e; \
	echo "$(I1)Replace /usr/bin/nawk to $(AWK)";	\
	$(SED) -i -e "s,/usr/bin/nawk,$(AWK)," $(PKG_PATCHED_SRC_DIR)/tools/shpp.awk;
endef


MK_VARS = prefix=$(INST_DIR) sysconfdir=$(INST_DIR)/etc
MK_VARS+= DPATCH_REVISION=$(PKG_VERSION)

    BUILD_CMD = $(BUILD_CMD_DEFAULT)
    CLEAN_CMD = $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(CLEAN_CMD_DEFAULT)

define INSTALL_CMD
  $(IW) $(MKDIR_P)	$(BSHARE_DIR)/man/man7 \
			$(BSHARE_DIR)/man/man1 \
			$(BBIN_DIR)	$(LOG)
  $(INSTALL_CMD_DEFAULT)
endef


# CMD: apply-all deapply-all list-all
#
# cd $(PKG_PATCHED_SRC_DIR) && dpatch --strict CMD


include $(RULES)
