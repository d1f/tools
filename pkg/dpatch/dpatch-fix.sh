#!/bin/sh

if test "$#" -lt 2; then
	echo 1>&2
	echo 1>&2 "`basename $0` tools-prefix dpatch-files ..."
	echo 1>&2
	exit 1
fi

tools_dir=$1
shift

set -e

for file in $*
do
  if test -f $file -o -L $file; then
    sed -i -e "s,/usr/share/dpatch/,${tools_dir}/share/dpatch/,g" $file \
	|| exit 1
  fi
done

exit 0
