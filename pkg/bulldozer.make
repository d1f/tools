PKG_TYPE      = ORIGIN
PKG_VERSION   = 1.1.1
PKG_SITES     = http://tibbo.com
PKG_SITE_PATH = downloads/archive/$(PKG)/$(PKG)-$(PKG_VERSION)
PKG_NAME      = $(PKG)-linux-$(PKG_VERSION)-x86
EXTRACT_STRIP_NUM = 2
PLATFORM      = build


include $(DEFS)
include $(RULES)
