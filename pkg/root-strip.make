PKG_HAS_NO_SOURCES = defined

# special case - [un]install itself
IWTYPE = no
include $(DEFS)


UNINSTALL_CMD=

ifneq ($(PLATFORM),build)
ifndef EXTERN_TOOLCHAIN
configure : $(call tstamp_f,install,binutils)
endif
endif

define INSTALL_CMD
  echo "$(I1)Stripping root fs binaries ..."
	export STRIP=$(STRIP) NM=$(NM)	\
	STRIP_SHLIB="$(STRIP_SHLIB)"	\
	STRIP_EXEC="$(STRIP_EXEC)"	\
	STRIP_KMOD="$(STRIP_KMOD)";	\
  $(TOOLS_DIR)/script/strip $(ROOT_DIR) $(LOG)
endef

INSTALL_ROOT_CMD = $(INSTALL_CMD)


include $(RULES)
