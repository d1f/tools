PKG_TYPE      = DEBIAN3
PKG_VERSION   = 2.4.47
PKG_VER_PATCH = -2
PKG_SITE_PATH = pool/main/a/$(PKG_BASE)

PLATFORM      = build

include $(DEFS)


configure : $(call bstamp_f,install,sed libtool)

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR)	&&	\
     PLATFORM=			\
     PKG_NAME= PKG_VERSION=	\
     ./configure -C $(LOG)	\
	--prefix=$(INST_DIR)	\
	--enable-shared		\
	--disable-static	\
	--disable-gettext	\
	--with-gnu-ld		\
	$(STD_VARS)		\
	PCFLAGS="$(STD_CFLAGS) $(STD_CPPFLAGS)" \
	DEBUG=-DNDEBUG
endef


MK_VARS = PCFLAGS="$(STD_CFLAGS) $(STD_CPPFLAGS) -D_GNU_SOURCE -D_FILE_OFFSET_BITS=64"

    BUILD_CMD_TARGET = default
    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)


       INSTALL_CMD_TARGET = install install-lib
define INSTALL_CMD
  $(INSTALL_CMD_DEFAULT)
  $(IWMAKE) -C $(PKG_BLD_DIR) install-dev $(LOG)
  $(IWMAKE) -C $(PKG_BLD_DIR) install-lib $(LOG)
endef


include $(RULES)
