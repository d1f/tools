ifneq ($(strip $(shell which $(PKG) 2>/dev/null)),)

PKG_HAS_NO_SOURCES = defined

else # has no $(PKG)

PKG_TYPE      = ORIGIN
PKG_VERSION   = 1.0.6
PKG_SITES     = http://bzip.org
PKG_SITE_PATH = $(PKG_VERSION)
PLATFORM      = build
PKG_REMOTE_FILE_SUFFICES = .tar.gz # bootstrap

include $(DEFS)


build : $(call bstamp_f,install,ccache)

MK_VARS = $(STD_VARS)

    BUILD_CMD_TARGET = bzip2 libbz2.a
    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)


define INSTALL_CMD
  $(IW) $(INSTxFILES) $(PKG_BLD_DIR)/bzip2    $(BBIN_DIR) $(LOG)
  $(IW) $(INSTxFILES) $(PKG_BLD_DIR)/libbz2.a $(BLIB_DIR) $(LOG)
  $(IW) $(INSTxFILES) $(PKG_BLD_DIR)/bzlib.h  $(BINC_DIR) $(LOG)
  $(IW) $(LN) $(BBIN_DIR)/bzip2 $(BBIN_DIR)/bunzip2       $(LOG)
  $(IW) $(LN) $(BBIN_DIR)/bzip2 $(BBIN_DIR)/bzcat         $(LOG)
endef


endif # has no $(PKG)

include $(RULES)
