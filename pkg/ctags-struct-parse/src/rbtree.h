#ifndef  RBTREE_H
# define RBTREE_H

# include "./keyhash.h"


typedef void (*rbtree_walk_action_t)(const keyhash_t *kh)               __attribute__((__nonnull__));

void rbtree_add (void      **root_p, const char *key, const char *data) __attribute__((__nonnull__));
void rbtree_walk(const void *root  , rbtree_walk_action_t action)       __attribute__((__nonnull__));


#endif //RBTREE_H
