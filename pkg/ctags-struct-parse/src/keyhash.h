#ifndef  KEYHASH_H
# define KEYHASH_H

# include "./aphash.h"


typedef struct keyhash_s
{
    const char *key;
    aphash_t   hash;
} keyhash_t;


int        keyhash_cmp   (const void *left, const void *right) __attribute__((__nonnull__));
keyhash_t *keyhash_create(const char *key) __attribute__((__nonnull__)); // key strdup'ed
void       keyhash_delete(keyhash_t **khp) __attribute__((__nonnull__)); // key free'd


#endif //KEYHASH_H
