#include "./freep.h"
#include <stdlib.h> // NULL free


void freep(void *vpp)
{
	void **_vpp = vpp;
	void *vp = *_vpp;

	if (vp != NULL)
	{
	    free(vp);
	    *_vpp = NULL;
	}
}
