#include "./aphash.h"
#include <stdlib.h> // size_t


static aphash_t ap_hash_add_char(aphash_t hash, size_t i, const char c)
{
    // (C) Arash Partow http://www.partow.net/programming/hashfunctions/
    hash ^= ((i & 1) == 0)
	? (  (hash <<  7) ^  c * (hash >> 3))
	: (~((hash << 11) + (c ^ (hash >> 5))));

    return hash;
}

aphash_t ap_hash_add_str(aphash_t hash, const char *str)
{
    size_t i;
    for(i = 0; *str != 0; ++str, ++i)
	hash = ap_hash_add_char(hash, i, *str);

    if (i & 1)
	hash = ap_hash_add_char(hash, i, 0);

    return hash;
}
