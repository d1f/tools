#include "./rbtree.h"
#include <search.h> // tsearch
#include <stdlib.h> // EXIT_FAILURE
#include <stdio.h>
#include <error.h>
#include <errno.h>


void rbtree_add(void **root_p, const char *key, const char *data)
{
    keyhash_t *kh = keyhash_create(key);

    keyhash_t **found_p = tsearch(kh, root_p, keyhash_cmp);
    if (found_p == NULL)
	error(EXIT_FAILURE, errno, "%s(): tsearch() failed", __func__);
    keyhash_t *found = *found_p;
    if (found != NULL)
    {
	if (found->hash != AP_HASH_INIT_VALUE) // old key found
	    keyhash_delete(&kh);
	found->hash = ap_hash_add_str(found->hash, data);
    }
    else
    {
	error(EXIT_FAILURE, 0, "%s(): found is NULL", __func__);
    }
}

static rbtree_walk_action_t _action = NULL;

static void rbtree_action(const void *nodep, const VISIT which, const int depth)
{
    (void)depth;

    switch (which)
    {
    case preorder:
    case endorder:
	break;
    case postorder:
    case leaf:
	{
	    const keyhash_t *kh = *(const keyhash_t * const *) nodep;
	    _action(kh);
	    break;
	}
    }
}

void rbtree_walk(const void *root, rbtree_walk_action_t action)
{
    _action = action;
    twalk(root, rbtree_action);
}
