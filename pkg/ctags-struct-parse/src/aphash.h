#ifndef  APHASH_H
# define APHASH_H

# include <stdint.h> // uint32_t


typedef uint32_t aphash_t;

# define AP_HASH_INIT_VALUE (0xAAAAAAAA)

// (C) Arash Partow http://www.partow.net/programming/hashfunctions/
aphash_t ap_hash_add_str(aphash_t hash, const char *str);


#endif //APHASH_H
