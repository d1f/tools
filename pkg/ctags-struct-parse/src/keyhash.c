#include "./keyhash.h"
#include "./freep.h"
#include <stdlib.h> // free
#include <string.h> // strcmp strdup
#include <error.h>
#include <errno.h>


int keyhash_cmp(const void *left, const void *right)
{
    return strcmp(((const keyhash_t *)left)->key, ((const keyhash_t *)right)->key);
}

keyhash_t *keyhash_create(const char *key)
{
    keyhash_t *kh = malloc(sizeof(keyhash_t));
    if (kh == NULL)
	error(EXIT_FAILURE, errno, "Can't allocate %zu bytes", sizeof(keyhash_t));

    kh->key  = strdup(key);
    if (kh->key == NULL)
	error(EXIT_FAILURE, errno, "Can't dup string %zu bytes", strlen(key)+1);
    kh->hash = AP_HASH_INIT_VALUE;

    return kh;
}

void keyhash_delete(keyhash_t **khp)
{
    keyhash_t *kh = *khp;
    if (kh == NULL)
	error(EXIT_FAILURE, 0, "%s(): kh == NULL", __func__);

    freep(&kh->key);
    freep(khp);
}
