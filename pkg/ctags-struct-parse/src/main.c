#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <stddef.h> // size_t
#include <string.h> // strcmp strlen
#include <ctype.h>  // toupper
#include <error.h>
#include <errno.h>
#include "./freep.h"
#include "./rbtree.h"


static void action(const keyhash_t *kh)
{
    printf("# define %s_SIGNATURE 0x%08x\n", kh->key, kh->hash);
}

int main(int ac, char *av[])
{
    (void)ac; (void)av;

    void *root = NULL;

    while (1)
    {
	char *lineptr = NULL;
	size_t n = 0;
	ssize_t rc = getline(&lineptr, &n, stdin);
	if (rc < 0)
	{
	    if (ferror(stdin))
		error(EXIT_FAILURE, errno, "Error on stdin");
	    if (feof(stdin))
		break;
	}

	char *struct_member=NULL, *type=NULL, *file=NULL, *rest=NULL;
	size_t line = 0;

	// df_list_node::data member 19 int/list.h intptr_t data[0];
	rc = sscanf(lineptr, "%ms %ms %zu %ms %m[\t -~]", &struct_member, &type, &line, &file, &rest);
	if (rc < 5)
	{
	    freep(&struct_member); freep(&type); freep(&file); freep(&rest);
	    continue;
	}

	if (strcmp(type, "member") == 0)
	{
	    char *colons = strstr(struct_member, "::");
	    if (colons != NULL)
	    {
		const char *structure = struct_member;
		*colons = 0;
		const char *member = colons + 2;
#if 0
		printf("%s::%s %s:%zu %s\n",
		       structure, member, file, line, rest);
#endif

		char *data = NULL;
		asprintf(&data, "%s::%s %s:%zu %s", structure, member, file, line, rest);
		rbtree_add(&root, structure, data);
		freep(&data);
	    }
	}

	freep(&struct_member); freep(&type); freep(&file); freep(&rest);
	freep(&lineptr);
    }

    printf("#ifndef  STRUCT_SIGNATURES_H\n"
	   "# define STRUCT_SIGNATURES_H\n"
	   "\n"
	  );

    rbtree_walk(root, action);

    printf("\n"
	   "#endif //STRUCT_SIGNATURES_H\n");

    return EXIT_SUCCESS;
}
