# Usage:
#
# $(BBIN_DIR)/ctags-struct dirs|files ... | $(BBIN_DIR)/ctags-struct-parse


PKG_ORIG_DIR     = $(PKG_PKG_DIR)/src
PKG_FETCH_METHOD = DONE
SEPARATE_SRC_BLD_DIRS = defined
PLATFORM         = build

include $(DEFS)


BUILD_TYPE     = MULTI_SRC_BIN
BIN_NAME       = $(PKG)
MAKES_MAKEFILE = $(TOOLS_DIR)/makes/all.Makefile
include          $(TOOLS_DIR)/makes/conf.mk

CONFIGURE_CMD  = $(MAKES_CONFIGURE_CMD)


#MK_VARS = Q=
    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)
  INSTALL_CMD =   $(INSTALL_CMD_DEFAULT)


install : $(call bstamp_f,install,exuberant-ctags)

define INSTALL_ROOT_CMD
  $(INSTALL_ROOT_CMD_DEFAULT)
  $(IW) $(INSTxFILES) $(PKG_PKG_DIR)/ctags-struct $(BBIN_DIR) $(LOG)
endef


include $(RULES)
