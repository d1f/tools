PKG_VERSION   = $(LIBC_VERSION)
PKG_SITES     = $(GNU_ORG_SITES)
PKG_SITE_PATH = glibc
SEPARATE_SRC_BLD_DIRS = defined

include $(DEFS)


include $(TOOLS_PKG_DIR)/glibc/addons.mk

ifneq ($(glibc_have_ext_addon_ports),)

EXTRACT_STRIP_NUM = 1
PKG_PATCH_STRIP_NUM = 2

include $(TOOLS_PKG_DIR)/crosstool.mk

ifeq ($(shell $(VERCMP) $(LIBC_VERSION) '<' 2.10),true)
PKG_PATCH_FILES  = $(CROSSTOOL_PATCH_DIR)/glibc/ports-$(LIBC_VERSION)/*.patch
endif

PKG_PATCH_FILES += $(wildcard $(PKG_PKG_DIR)/$(LIBC_VERSION)_*.patch)

else  # !glibc_have_ext_addon_ports
  PKG_HAS_NO_SOURCES = defined
endif #  glibc_have_ext_addon_ports


include $(RULES)
