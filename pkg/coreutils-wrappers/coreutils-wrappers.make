PKG_HAS_NO_SOURCES = defined
PLATFORM           = build

# special case - [un]install itself
IWTYPE = no
include $(DEFS)


wrappers = $(wildcard $(PKG_PKG_DIR)/wrappers/*)

  INSTALL_CMD = $(INSTxFILES) $(wrappers) $(BBIN_DIR)                         $(LOG)
UNINSTALL_CMD = $(RM) -v $(addprefix      $(BBIN_DIR)/,$(notdir $(wrappers))) $(LOG)


include $(RULES)
