PKG_TYPE      = DEBIAN3
PKG_VERSION   = 0.003
PKG_VER_PATCH = -2
PKG_SITE_PATH = pool/main/libp/$(PKG_BASE)

PLATFORM      = build

include $(DEFS)


include $(TOOLS_PKG_DIR)/perl.mk

configure : $(call bstamp_f,install,perl)

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) && \
	unset $(PERL_UNSET); PERL5LIB=$(PERL_LIB) $(PERL_BIN) Build.PL --prefix $(INST_DIR) $(LOG)
endef


define BUILD_CMD
  cd $(PKG_BLD_DIR) && unset $(PERL_UNSET); $(PERL_BIN) Build $(LOG)
endef


define INSTALL_CMD
  cd $(PKG_BLD_DIR) && unset $(PERL_UNSET); $(IW) $(PERL_BIN) Build install $(LOG)
endef


include $(RULES)
