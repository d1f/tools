AC_VER        = 2.61

PKG_TYPE      = DEBIAN1
PKG_VER_PATCH = -8
PKG_BASE      = autoconf

MK_JOBS       = 1

include $(TOP_DIR)/tools/pkg/autoconf-common.mk
