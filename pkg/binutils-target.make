PKG_ORIG_DIR     = $(wildcard $(PATCHED_SRC_DIR)/binutils_*)
PKG_FETCH_METHOD = DONE
SEPARATE_SRC_BLD_DIRS = defined

include $(DEFS)


configure : $(call tstamp_f,install,toolchain)
configure : $(call bstamp_f,install,gawk sed bison flex texinfo m4)

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) &&			\
     $(PKG_SRC_DIR)/configure -C $(LOG)	\
	--prefix=$(INST_DIR)		\
	--build=$(BUILD)		\
	--host=$(TARGET)		\
	--target=$(TARGET)		\
	--disable-gold			\
	--disable-ld			\
	--disable-libquadmath		\
	--disable-libquadmath-support	\
	--disable-libada		\
	--disable-libssp		\
	--disable-static-libjava	\
	--disable-bootstrap		\
	--enable-lto			\
	--enable-stage1-languages=c	\
	--disable-objc-gc		\
	--enable-shared			\
	--disable-static		\
	--enable-install-libbfd		\
	--disable-nls			\
	--with-gnu-ld			\
	--with-mmap			\
	--without-zlib			\
	--without-libiconv-prefix	\
	--without-included-gettext	\
	--without-libintl-prefix	\
	--enable-install-libiberty	\
	--with-cross-host=$(TARGET)	\
	--disable-multilib		\
	$(STD_VARS)
endef


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)


define INSTALL_CMD
  $(INSTALL_CMD_DEFAULT)
  $(IW) $(INSTxFILES) $(IBIN_DIR)/objdump        $(RBIN_DIR) $(LOG)
  $(IW) $(INSTxFILES) $(ILIB_DIR)/libopcodes*.so $(RLIB_DIR) $(LOG)
  $(IW) $(INSTxFILES) $(ILIB_DIR)/libbfd*.so     $(RLIB_DIR) $(LOG)

  $(STRIP) $(STRIP_SHLIB) $(RLIB_DIR)/libbfd*.so            $(LOG)
  $(STRIP) $(STRIP_SHLIB) $(RLIB_DIR)/libopcodes*.so        $(LOG)
  $(STRIP) $(STRIP_EXEC)  $(RBIN_DIR)/objdump               $(LOG)
endef


include $(RULES)
