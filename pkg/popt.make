PKG_TYPE      = DEBIAN3
PKG_VERSION   = 1.16
PKG_VER_PATCH = -10
PKG_SITE_PATH = pool/main/p/$(PKG_BASE)

include $(DEFS)


configure : $(call tstamp_f,install,toolchain)
configure : $(call bstamp_f,install,gawk sed)

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) &&		\
     ./configure -C	$(LOG)	\
	--prefix=$(INST_DIR)	\
	--build=$(BUILD)	\
	--host=$(TARGET)	\
	--disable-static	\
	--enable-shared		\
	--disable-nls		\
	--with-gnu-ld		\
	--without-libiconv-prefix \
	--without-libintl-prefix  \
	$(STD_VARS)
endef


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)


INSTALL_CMD  = $(INSTALL_CMD_DEFAULT)
INSTALL_ROOT_CMD = $(IW) $(INSTxFILES)     $(ILIB_DIR)/libpopt.so* $(RLIB_DIR) $(LOG)


include $(RULES)
