ifneq ($(strip $(shell which $(PKG) 2>/dev/null))$(PLATFORM),build)

PKG_HAS_NO_SOURCES = defined

else # has no lzma for build platform

PKG_TYPE      = DEBIAN3
PKG_VERSION   = 9.22
PKG_VER_PATCH = -2
PKG_SITE_PATH = pool/main/l/$(PKG_BASE)
PLATFORM      = build

include $(DEFS)


PKG_SUB_DIR = CPP/7zip/Bundles/LzmaCon

define BUILD_CMD
  $(DOMAKE) $(MKJ) -C $(PKG_BLD_DIR)/C/Util/Lzma -f makefile.gcc all $(LOG)
  $(DOMAKE) $(MKJ) -C $(PKG_BLD_SUB_DIR)         -f makefile.gcc all $(LOG)
endef

    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)


inc_list  = Types.h LzmaEnc.h LzmaEnc.c LzmaDec.h LzmaDec.c LzHash.h
inc_list += LzFind.h LzFind.c 7zVersion.h

define INSTALL_CMD
  $(IW) $(INSTxFILES) $(PKG_BLD_SUB_DIR)/lzma_alone $(BBIN_DIR) $(LOG)
  $(IW) $(INSTxFILES) $(PKG_BLD_SUB_DIR)/lzmp       $(BBIN_DIR) $(LOG)
  $(IW) $(LN_S)       lzmp   $(BBIN_DIR)/lzma                   $(LOG)
  $(IW) $(LN_S)       lzmp   $(BBIN_DIR)/unlzma                 $(LOG)
  $(IW) $(LN_S)       lzmp   $(BBIN_DIR)/lzcat                  $(LOG)
  $(IW) $(INST_FILES) $(addprefix $(PKG_BLD_DIR)/C/,$(inc_list)) $(IINC_DIR)/lzma $(LOG)
endef


endif # has no lzma for build platform

include $(RULES)
