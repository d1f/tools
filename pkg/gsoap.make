PKG_VER_MAJOR    = 2.8
PKG_VER_MINOR    = 17
PKG_VERSION      = $(PKG_VER_MAJOR).$(PKG_VER_MINOR)
PKG_SITES        = $(SF_NET_SITES)
PKG_SITE_PATH    = gsoap2/gSOAP
PKG_INFIX        = _
PKG_LOCAL__FILE_SUFFICES = .zip
PKG_REMOTE_FILE_SUFFICES = .zip
PKG_DIR          = $(PKG)-$(PKG_VER_MAJOR)
PKG_ORIG_SRC_DIR = $(ORIG_SRC_DIR)/$(PKG_DIR)

include $(DEFS)


define POST_EXTRACT_CMD
  $(RM_R) $(PKG_ORIG_SRC_DIR)/autom4te.cache
  $(RM_R) $(PKG_ORIG_SRC_DIR)/gsoap/bin
  $(RM_R) $(addprefix $(PKG_ORIG_SRC_DIR)/gsoap/src/,soapcpp2_lex.c \
		soapcpp2_yacc.c soapcpp2_yacc.h y.output soapcpp2_yacc.output)
endef


PKG_PATCH_FILES = $(wildcard $(PKG_PKG_DIR)/$(PKG)_*.patch)


configure : $(call bstamp_f,install,flex bison)

ifneq ($(PLATFORM),build)
  build_host = --build=$(BUILD) --host=$(TARGET)
  configure : $(call tstamp_f,install,toolchain)
  build     : $(call bstamp_f,install,$(PKG))
  MK_VARS += SOAP=$(BBIN_DIR)/soapcpp2
  STD_CFLAGS += -fPIC
  STD_CXXFLAGS += -fPIC
endif

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) &&		\
     ac_cv_func_malloc_0_nonnull=yes	\
     ac_cv_func_realloc_0_nonnull=yes \
     ./configure -C $(LOG)	\
	--prefix=$(INST_DIR)	\
	$(build_host)		\
	--disable-ssl		\
	$(STD_VARS)
endef


define BUILD_CMD
  cd $(PKG_BLD_DIR)/gsoap/src && $(DOMAKE) soapcpp2_yacc.h $(LOG)
  $(BUILD_CMD_DEFAULT)
endef

    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)
  INSTALL_CMD =   $(INSTALL_CMD_DEFAULT)


include $(RULES)
