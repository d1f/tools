ifneq ($(strip $(shell which banner 2>/dev/null)),)

PKG_HAS_NO_SOURCES = defined

else # has no sysvbanner

PKG_TYPE              = DEBIAN_NATIVE
PKG_VERSION           = 1.0.15
PKG_SITE_PATH         = pool/main/s/$(PKG_BASE)
SEPARATE_SRC_BLD_DIRS = defined
PLATFORM              = build

include $(DEFS)


PKG_PATCH_FILES = $(PKG_PKG_DIR)/$(PKG)-*.patch


define BUILD_CMD
  '$(CC)' $(STD_CPPFLAGS) $(STD_CFLAGS) $(STD_LDFLAGS) \
	   $(PKG_SRC_DIR)/banner.c \
	-o $(PKG_BLD_DIR)/banner $(LOG)
endef


INSTALL_CMD = $(IW) $(INSTxFILES) $(PKG_BLD_DIR)/banner $(BBIN_DIR) $(LOG)


endif # has no sysvbanner

include $(RULES)
