ifneq ($(strip $(shell svn --version --quiet 2>/dev/null)),)

PKG_HAS_NO_SOURCES = defined

else # has no subversion

PKG_TYPE      = DEBIAN1
PKG_VERSION   = 1.9.5
PKG_VER_PATCH = -1
PKG_SITE_PATH = pool/main/s/$(PKG_BASE)
PLATFORM      = build

include $(DEFS)


ifeq ($(PKG_TYPE),DEBIAN1)
  include $(TOOLS_PKG_DIR)/quilt.mk

  define PATCH_CMD
    $(RELINK) $(PKG_PATCHED_SRC_DIR)/subversion/libsvn_delta/text_delta.c $(LOG)
    $(APPLY_QUILT_PATCHES_CMD)
  endef
endif


configure : $(call bstamp_f,install,sed apr apr-util pkg-config sqlite3 zlib)

without  = neon serf apr_memcache apxs
without += apache-libexecdir
without += trang berkeley-db sasl gnome-keyring kwallet ssl
without += jdk jikes swig ruby-sitedir ctypesgen junit

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) &&		\
     ./configure	$(LOG)	\
	--prefix=$(INST_DIR)	\
	--disable-static	\
	--disable-nls		\
	--disable-javahl	\
	$(addprefix --without-,$(without)) \
	--with-zlib=$(INST_DIR) \
	--with-gnu-ld		\
	$(STD_VARS)
endef


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)

libs = subr delta fs_util fs_fs fs ra_svn repos ra_local diff ra wc client
define INSTALL_CMD
  touch $(foreach l,$(libs),\
	$(PKG_BLD_DIR)/subversion/libsvn_$(l)/.libs/libsvn_$(l)-1.lai) $(LOG)
  $(INSTALL_CMD_DEFAULT)
endef


endif # has no subversion

include $(RULES)
