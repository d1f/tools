PKG_TYPE      = DEBIAN3
PKG_VERSION   = 2.08
PKG_VER_PATCH = -1.2
PKG_SITE_PATH = pool/main/l/$(PKG_BASE)

include $(DEFS)


configure : $(TOOLCHAIN_INSTALL_STAMP)
configure : $(call bstamp_f,install,libtool sed gawk)

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) &&		\
     ./configure -C	$(LOG)	\
	--prefix=$(INST_DIR)	\
	--build=$(BUILD)	\
	--host=$(TARGET)	\
	--enable-shared		\
	--disable-static	\
	--with-gnu-ld		\
	$(STD_VARS)
endef


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
  INSTALL_CMD =   $(INSTALL_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)

INSTALL_ROOT_CMD = $(IW) $(INSTxFILES) $(ILIB_DIR)/liblzo2.so* $(RLIB_DIR) $(LOG)


include $(RULES)
