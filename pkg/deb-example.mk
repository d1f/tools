ifneq ($(filter %debian1,$(GCC_VERSION)),)
  PKG_TYPE = DEBIAN1
  pkg_full_version = $(patsubst %debian1,%,$(GCC_VERSION))
else ifneq ($(filter %debian3,$(GCC_VERSION)),)
  PKG_TYPE = DEBIAN3
  pkg_full_version = $(patsubst %debian3,%,$(GCC_VERSION))
else
  PKG_TYPE = ORIGIN
endif


ifneq ($(filter DEBIAN%,$(PKG_TYPE)),)
  gcc_base      = $(if $(filter 2.95% 3.0,$(PKG_VERSION)),gcc-everything,gcc)
  PKG_VERSION   = $(firstword $(subst -, ,$(pkg_full_version)))
  PKG_VER_PATCH = -$(lastword $(subst -, ,$(pkg_full_version)))
  PKG_VER_MAJOR = $(word 1,$(subst ., ,$(PKG_VERSION))).$(word 2,$(subst ., ,$(PKG_VERSION)))
  PKG_VER_MINOR = $(word 3,$(subst ., ,$(PKG_VERSION)))
  PKG_SITE_PATH = pool/main/g/$(gcc_base)-$(PKG_VER_MAJOR)
  PKG_FILE_BASE =             $(gcc_base)-$(PKG_VER_MAJOR)_$(PKG_VERSION)
  PKG_BASE      = $(PKG)-$(PKG_VER_MAJOR)

  define POST_EXTRACT_CMD
    if test -f $(PKG_ORIG_SRC_DIR)/gcc-$(PKG_VERSION)*.tar*; then		\
       $(ZCAT) $(PKG_ORIG_SRC_DIR)/gcc-$(PKG_VERSION)*.tar* |			\
               $(TAR) -C $(PKG_ORIG_SRC_DIR) -xp				\
                   $(if $(EXTRACT_STRIP_NUM),--strip $(EXTRACT_STRIP_NUM))	\
               || { $(RM_R) $(PKG_ORIG_SRC_DIR); exit 1; };			\
    fi
    $(RM)        $(PKG_ORIG_SRC_DIR)/gcc-$(PKG_VERSION)*.tar* $(LOG)
  endef

  ifeq ($(PKG_VERSION),4.3.5)
    FORCE_QUILT = defined
  endif
endif

ifdef FORCE_QUILT
  include $(TOOLS_PKG_DIR)/quilt.mk
endif

include $(DEFS)


ifneq ($(filter DEBIAN%,$(PKG_TYPE)),)
  PKG_PATCH_FILES += $(wildcard $(PKG_PKG_DIR)/deb-$(PKG_VERSION)*.patch)
endif


include $(RULES)
