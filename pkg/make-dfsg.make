ifneq ($(strip $(shell make --version 2>/dev/null | fgrep "GNU Make 4."))$(PLATFORM),build)

PKG_HAS_NO_SOURCES = defined

else # has no GNU make 4.x

PKG_TYPE      = DEBIAN1
PKG_VERSION   = 4.1
PKG_VER_PATCH = -9
PKG_SITE_PATH = pool/main/m/$(PKG_BASE)

include $(DEFS)


POST_EXTRACT_CMD = $(RM_R) $(PKG_ORIG_SRC_DIR)/autom4te.cache $(LOG)


PKG_PATCH2_FILES = $(PKG_PKG_DIR)/$(PKG)_*.patch


AC_VER = 2.69
AM_VER = 1.11

include $(TOOLS_PKG_DIR)/autotools.mk

define PRE_CONFIGURE_CMD
 touch $(PKG_BLD_DIR)/INSTALL $(LOG)
 $(AUTOTOOLS_CMD)
endef


# "make install" of big progs (glibc-headers) are failed with vfork
# when running from strace.
# Strace is too complicated for me to fix this and I fix "make" instead
# by disabling vfork. -- df
no_vfork = ac_cv_func_vfork=no ac_cv_func_vfork_works=no

ifeq ($(PLATFORM),build)
  prefix = --prefix=$(INST_DIR)
  build_host =
else
  prefix = --prefix=$(INST_DIR) --exec-prefix=$(ROOT_DIR)
  build_host = --build=$(BUILD) --host=$(TARGET)
endif

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) &&	$(no_vfork)	\
     ./configure -C $(LOG)	\
	$(prefix)  $(build_host)	\
	--disable-nls			\
	--enable-largefile		\
	--enable-job-server		\
	--with-gnu-ld			\
	--without-libiconv-prefix	\
	--without-libintl-prefix	\
	--without-guile			\
	$(STD_VARS)
endef
# WARNINGS=none autoreconf --warnings=none -f -i
# ac_cv_lib_util_getloadavg=no ./configure
# --verbose


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)
  INSTALL_CMD =   $(INSTALL_CMD_DEFAULT)


endif # has no GNU make 4.x

include $(RULES)
