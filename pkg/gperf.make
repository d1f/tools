ifneq ($(strip $(shell which $(PKG) 2>/dev/null)),)

PKG_HAS_NO_SOURCES = defined

else # has no gperf

PKG_TYPE      = DEBIAN3
PKG_VERSION   = 3.0.4
PKG_VER_PATCH = -2
PKG_SITE_PATH = pool/main/g/$(PKG_BASE)
PLATFORM      = build

include $(DEFS)


pre-configure : $(call bstamp_f,install,sed)

define PRE_CONFIGURE_CMD
  $(SED) -i -e "s/ac_subdirs_all='lib src tests doc'/ac_subdirs_all='lib src'/" \
	$(PKG_BLD_DIR)/configure $(LOG)
endef

CONFIGURE_CMD = cd $(PKG_BLD_DIR) && ./configure $(LOG) --prefix=$(INST_DIR)

define BUILD_CMD
  $(DOMAKE) -C $(PKG_BLD_DIR)/lib all     $(LOG)
  $(DOMAKE) -C $(PKG_BLD_DIR)/src all     $(LOG)
endef

define INSTALL_CMD
  $(IWMAKE) -C $(PKG_BLD_DIR)/lib install $(LOG)
  $(IWMAKE) -C $(PKG_BLD_DIR)/src install $(LOG)
endef


    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)


endif # has no gperf

include $(RULES)
