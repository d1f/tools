PKG_TYPE      = DEBIAN3
PKG_VERSION   = 3.13.0
PKG_VER_PATCH = -1
PKG_SITE_PATH = pool/main/v/$(PKG_BASE)

include $(DEFS)


AC_VER = 2.69
AM_VER = 1.11
include $(TOOLS_PKG_DIR)/autotools.mk
PRE_CONFIGURE_CMD = $(AUTOTOOLS_CMD)


configure : $(call bstamp_f,install,gawk sed)

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) &&		\
     ac_cv_func_memcmp_working=yes \
     ./configure -C $(LOG)	\
	--prefix=		\
	--build=$(BUILD)	\
	--host=$(TARGET)	\
	--enable-only32bit	\
	$(STD_VARS) DESTDIR=$(INST_DIR)
endef


MK_VARS += DESTDIR=$(INST_DIR)

    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)


INSTALL_CMD = $(INSTALL_CMD_DEFAULT)
define INSTALL_ROOT_CMD
  $(IW) $(INSTxFILES) $(ILIB_DIR)/valgrind/vgpreload_memcheck-arm-linux.so $(RLIB_DIR)/valgrind $(LOG)
  $(IW) $(INSTxFILES) $(ILIB_DIR)/valgrind/vgpreload_core-arm-linux.so     $(RLIB_DIR)/valgrind $(LOG)
  $(IW) $(INSTxFILES) $(ILIB_DIR)/valgrind/none-arm-linux                  $(RLIB_DIR)/valgrind $(LOG)
  $(IW) $(INSTxFILES) $(ILIB_DIR)/valgrind/memcheck-arm-linux              $(RLIB_DIR)/valgrind $(LOG)
  $(IW) $(INSTxFILES) $(IBIN_DIR)/vgdb                                     $(RBIN_DIR)          $(LOG)
  $(IW) $(INSTxFILES) $(IBIN_DIR)/valgrind-listener                        $(RBIN_DIR)          $(LOG)
  $(IW) $(INSTxFILES) $(IBIN_DIR)/valgrind-di-server                       $(RBIN_DIR)          $(LOG)
  $(IW) $(INSTxFILES) $(IBIN_DIR)/valgrind                                 $(RBIN_DIR)          $(LOG)
endef


include $(RULES)
