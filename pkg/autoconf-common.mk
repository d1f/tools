PKG_VERSION   ?= $(AC_VER)
PKG_SITE_PATH ?= pool/main/a/$(PKG_BASE)
PKG_DIR       ?= $(PKG_BASE)$(PKG_VERSION)

PLATFORM       = build

include $(DEFS)


define PRE_CONFIGURE_CMD
  $(RM) $(addprefix $(PKG_BLD_DIR)/doc/,version.texi stamp-vti)	$(LOG)
  $(MKDIR_P)  $(PKG_BLD_DIR)/doc				$(LOG)
  touch       $(PKG_BLD_DIR)/doc/fdl.texi			$(LOG)
  touch `find $(PKG_BLD_DIR) -name Makefile.in`			$(LOG)
  touch       $(PKG_BLD_DIR)/configure				$(LOG)
endef


include $(TOOLS_PKG_DIR)/autoconf.mk

configure : $(call bstamp_f,install,help2man)

define CONFIGURE_CMD
  cd    $(PKG_BLD_DIR) && ./configure	$(LOG)	\
	--prefix=$(AC_PREFIX)			\
	--with-lispdir=				\
	EMACS=no M4=$(M4)			\
	MAKEINFO=/bin/true
endef


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
  INSTALL_CMD =   $(INSTALL_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)


include $(RULES)
