PKG_ORIG_DIR     = $(PKG_PKG_DIR)/src
PKG_FETCH_METHOD = DONE
#SEPARATE_SRC_BLD_DIRS = defined

#PLATFORM         = build

include $(DEFS)


ifeq ($(PLATFORM),build)

configure : $(call bstamp_f,install,sed flex bison gperf ncurses)

BUILD_TYPE     = MULTI_SRC_BIN
BIN_NAME       = $(PKG)
LIBS           = ncurses tinfo
include          $(TOP_DIR)/tools/makes/conf.mk

CONFIGURE_CMD  = $(MAKES_CONFIGURE_CMD)


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)
  INSTALL_CMD =   $(INSTALL_CMD_DEFAULT)
  INSTALL_ROOT_CMD = $(INSTALL_ROOT_CMD_DEFAULT)

else # PLATFORM != build

menuconfig : $(call bstamp_f,install,mconf)

define _env_vars_and_files_
	MENUCONFIG_MODE=single_menu # unrolls multimenu hierarchy
	MENUCONFIG_COLOR=<theme>
		mono       =>	selects colors suitable for monochrome displays\n"
		blackbg    =>	selects a color scheme with black background\n"
		classic    =>	theme with blue background. The classic look\n"
		bluetitle  =>	a LCD friendly version of classic. (default)\n"
	..config.tmp		temp file
	.kconfig.d		dep file default name
	KCONFIG_CONFIG=.config
	SRCTREE = ???
	KCONFIG_OVERWRITECONFIG=DIRNAME.tmpconfig.PID
	KCONFIG_NOTIMESTAMP=defined
	KCONFIG_AUTOCONFIG=include/config/auto.conf
	KCONFIG_AUTOHEADER ?= include/generated/autoconf.h
	KCONFIG_TRISTATE   ?= include/config/tristate.conf
	chdir("include/config")
	file_write_dep("include/config/auto.conf.cmd");
	.tmpconfig
	.tmpconfig_tristate
	.tmpconfig.h
endef

define MENUCONFIG_CMD
  KCONFIG_CONFIG=$(PKG_MENUCONFIG_FILE) $(BBIN_DIR)/mconf $(TOOLS_PKG_DIR)/$(PKG).config.in
endef

endif


include $(RULES)
