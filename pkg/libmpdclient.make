PKG_TYPE      = DEBIAN1
PKG_VERSION   = 2.9
PKG_VER_PATCH = -1
PKG_SITE_PATH = pool/main/libm/$(PKG_BASE)
PLATFORM      = build

include $(DEFS)


configure : $(call bstamp_f, libtool)

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) &&		\
     ./configure $(LOG)		\
	--prefix=$(INST_DIR)	\
	--disable-documentation	\
	--disable-shared	\
	--without-pic		\
	--with-gnu-ld		\
	--with-default-socket=/var/run/mpd/socket \
	$(STD_VARS)
endef


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)
  INSTALL_CMD =   $(INSTALL_CMD_DEFAULT)


include $(RULES)
