PKG_TYPE          = DEBIAN3
#PKG_VERSION       = 5.9+20150516
PKG_VERSION       = 6.0+20161126
PKG_VER_PATCH     = -1
PKG_SITE_PATH     = pool/main/n/$(PKG_BASE)

include $(DEFS)


configure : $(call bstamp_f,install,sed)

common_conf_flags  = --without-cxx
common_conf_flags += --without-cxx-binding
common_conf_flags += --without-ada
common_conf_flags += --without-manpages
common_conf_flags += --without-tests
common_conf_flags += --with-curses-h
common_conf_flags += --enable-mixed-case
common_conf_flags += --with-build-cc=$(BUILD_CC)
common_conf_flags += --with-build-cpp=$(BUILD_CPP)
ifeq ($(PLATFORM),build)
common_conf_flags += --with-build-cflags="$(BUILD_CFLAGS)"
common_conf_flags += --with-build-cppflags="$(BUILD_CPPFLAGS)"
common_conf_flags += --with-build-ldflags="$(BUILD_LDFLAGS)"
endif
#common_conf_flags += --with-build-libs=
common_conf_flags += --without-libtool
common_conf_flags += --with-shared
common_conf_flags += --without-normal
common_conf_flags += --without-debug
common_conf_flags += --without-profile
common_conf_flags += --with-termlib
common_conf_flags += --with-ticlib
#common_conf_flags += --enable-rpath
common_conf_flags += --disable-relink
common_conf_flags += --enable-overwrite
common_conf_flags += --disable-termcap
common_conf_flags += --disable-getcap
common_conf_flags += --disable-getcap-cache
common_conf_flags += --disable-home-terminfo
common_conf_flags += --enable-symlinks
common_conf_flags += --disable-tic-depends
common_conf_flags += --enable-const
common_conf_flags += --enable-sigwinch
common_conf_flags += --enable-tcap-names
common_conf_flags += --without-develop
common_conf_flags += --enable-hard-tabs
common_conf_flags += --disable-xmc-glitch
common_conf_flags += --disable-assumed-color
common_conf_flags += --disable-hashmap
common_conf_flags += --disable-colorfgbg
#common_conf_flags += --with-pthread
common_conf_flags += --enable-weak-symbols
common_conf_flags += --disable-reentrant
common_conf_flags += --disable-safe-sprintf
common_conf_flags += --disable-scroll-hints
common_conf_flags += --disable-wgetch-events
common_conf_flags += --enable-echo
common_conf_flags += --disable-warnings
common_conf_flags += --without-ada-compiler

ifeq ($(PLATFORM),build)
 define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) && ./configure $(LOG)	\
	             --prefix=$(INST_DIR)	\
	$(common_conf_flags)			\
	$(STD_VARS)
 endef
else
 configure : $(TOOLCHAIN_INSTALL_STAMP)

 # comp_hash.c:(.text+0x1b8): undefined reference to `__ctype_b_loc'
 # gcc 3.3.6; glibc 2.2.5; linux 2.4.32
 STD_IDIRS += $(tc_HEADERDIR)

 define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) && ./configure $(LOG)	\
	--build=$(BUILD) --host=$(TARGET)	\
	--with-install-prefix=$(INST_DIR)	\
	             --prefix=			\
	--with-system-type=linux-$(target_libc)	\
	--without-static			\
	--without-progs				\
	--without-gpm				\
	--without-dlsym				\
	--without-sysmouse			\
	--disable-widec				\
	--disable-big-core			\
	--with-bool='unsigned char'		\
	$(common_conf_flags)			\
	$(STD_VARS)
 endef

 build : $(call bstamp_f,install,ncurses)
endif


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)


libs_base  = tinfo tic ncurses curses
#libs_base += panel menu form
libs_so   = $(addprefix $(ILIB_DIR)/lib,$(addsuffix .so*,$(libs_base)))

iti_dir = $(ISHARE_DIR)/terminfo
rti_dir = $(RSHARE_DIR)/terminfo

define terminfo_install_small
  $(IW) $(MKDIR_P)				$(rti_dir)/a	$(LOG)
  $(IW) cp -afv $(iti_dir)/a/ansi		$(rti_dir)/a/	$(LOG)

  $(IW) $(MKDIR_P)				$(rti_dir)/d	$(LOG)
  $(IW) cp -afv $(iti_dir)/d/dumb		$(rti_dir)/d/	$(LOG)

  $(IW) $(MKDIR_P)				$(rti_dir)/l	$(LOG)
  $(IW) cp -afv $(iti_dir)/l/linux		$(rti_dir)/l/	$(LOG)

  $(IW) $(MKDIR_P)				$(rti_dir)/r	$(LOG)
  $(IW) cp -afv $(iti_dir)/r/rxvt		$(rti_dir)/r/	$(LOG)
  $(IW) cp -afv $(iti_dir)/r/rxvt-color		$(rti_dir)/r/	$(LOG)

  $(IW) $(MKDIR_P)				$(rti_dir)/s	$(LOG)
  $(IW) cp -afv $(iti_dir)/s/screen		$(rti_dir)/s/	$(LOG)

  $(IW) $(MKDIR_P)				$(rti_dir)/v	$(LOG)
  $(IW) cp -afv $(iti_dir)/v/vt100		$(rti_dir)/v/	$(LOG)
  $(IW) cp -afv $(iti_dir)/v/vt102		$(rti_dir)/v/	$(LOG)

  $(IW) $(MKDIR_P)				$(rti_dir)/x	$(LOG)
  $(IW) cp -afv $(iti_dir)/x/xterm		$(rti_dir)/x/	$(LOG)
  $(IW) cp -afv $(iti_dir)/x/xterm-color	$(rti_dir)/x/	$(LOG)
  $(IW) cp -afv $(iti_dir)/x/xterm-256color	$(rti_dir)/x/	$(LOG)
endef

define terminfo_install_big
  $(IW) $(MKDIR_P)			$(rti_dir)/x	$(LOG)
  $(IW) cp -afv $(iti_dir)/x/xterm*	$(rti_dir)/x/	$(LOG)
  $(IW) cp -afv $(iti_dir)/x/xiterm*	$(rti_dir)/x/	$(LOG)
  $(IW) cp -afv $(iti_dir)/x/xfce*	$(rti_dir)/x/	$(LOG)

  $(IW) $(MKDIR_P)			$(rti_dir)/v	$(LOG)
  $(IW) cp -afv $(iti_dir)/v/vwmterm*	$(rti_dir)/v/	$(LOG)
  $(IW) cp -afv $(iti_dir)/v/vt*	$(rti_dir)/v/	$(LOG)
  $(IW) cp -afv $(iti_dir)/v/vs*	$(rti_dir)/v/	$(LOG)

  $(IW) $(MKDIR_P)			$(rti_dir)/u	$(LOG)
  $(IW) cp -afv $(iti_dir)/u/*		$(rti_dir)/u/	$(LOG)

  $(IW) $(MKDIR_P)			$(rti_dir)/s	$(LOG)
  $(IW) cp -afv $(iti_dir)/s/sun*	$(rti_dir)/s/	$(LOG)
  $(IW) cp -afv $(iti_dir)/s/screen*	$(rti_dir)/s/	$(LOG)

  $(IW) $(MKDIR_P)			$(rti_dir)/r	$(LOG)
  $(IW) cp -afv $(iti_dir)/r/rxvt*	$(rti_dir)/r/	$(LOG)

  $(IW) $(MKDIR_P)			$(rti_dir)/q	$(LOG)
  $(IW) cp -afv $(iti_dir)/q/qnx*	$(rti_dir)/q/	$(LOG)
  $(IW) cp -afv $(iti_dir)/q/qansi*	$(rti_dir)/q/	$(LOG)

  $(IW) $(MKDIR_P)			$(rti_dir)/p	$(LOG)
  $(IW) cp -afv $(iti_dir)/p/putty*	$(rti_dir)/p/	$(LOG)
  $(IW) cp -afv $(iti_dir)/p/pckermit*	$(rti_dir)/p/	$(LOG)
  $(IW) cp -afv $(iti_dir)/p/pcvt*	$(rti_dir)/p/	$(LOG)
  $(IW) cp -afv $(iti_dir)/p/pcansi*	$(rti_dir)/p/	$(LOG)

  $(IW) $(MKDIR_P)			$(rti_dir)/o	$(LOG)
  $(IW) cp -afv $(iti_dir)/o/oldsun*	$(rti_dir)/o/	$(LOG)

  $(IW) $(MKDIR_P)			$(rti_dir)/n	$(LOG)
  $(IW) cp -afv $(iti_dir)/n/nxterm*	$(rti_dir)/n/	$(LOG)
  $(IW) cp -afv $(iti_dir)/n/nsterm*	$(rti_dir)/n/	$(LOG)
  $(IW) cp -afv $(iti_dir)/n/next*	$(rti_dir)/n/	$(LOG)
  $(IW) cp -afv $(iti_dir)/n/nec*	$(rti_dir)/n/	$(LOG)
  $(IW) cp -afv $(iti_dir)/n/ncsa*	$(rti_dir)/n/	$(LOG)
  $(IW) cp -afv $(iti_dir)/n/nansi*	$(rti_dir)/n/	$(LOG)

  $(IW) $(MKDIR_P)			$(rti_dir)/m	$(LOG)
  $(IW) cp -afv $(iti_dir)/m/mvterm*	$(rti_dir)/m/	$(LOG)
  $(IW) cp -afv $(iti_dir)/m/mterm*	$(rti_dir)/m/	$(LOG)
  $(IW) cp -afv $(iti_dir)/m/ms*	$(rti_dir)/m/	$(LOG)
  $(IW) cp -afv $(iti_dir)/m/mrxvt*	$(rti_dir)/m/	$(LOG)
  $(IW) cp -afv $(iti_dir)/m/mlterm*	$(rti_dir)/m/	$(LOG)
  $(IW) cp -afv $(iti_dir)/m/minix*	$(rti_dir)/m/	$(LOG)
  $(IW) cp -afv $(iti_dir)/m/minitel*	$(rti_dir)/m/	$(LOG)
  $(IW) cp -afv $(iti_dir)/m/microterm*	$(rti_dir)/m/	$(LOG)
  $(IW) cp -afv $(iti_dir)/m/*sun*	$(rti_dir)/m/	$(LOG)
  $(IW) cp -afv $(iti_dir)/m/*linux*	$(rti_dir)/m/	$(LOG)
  $(IW) cp -afv $(iti_dir)/m/mac*	$(rti_dir)/m/	$(LOG)

  $(IW) $(MKDIR_P)			$(rti_dir)/l	$(LOG)
  $(IW) cp -afv $(iti_dir)/l/linux*	$(rti_dir)/l/	$(LOG)

  $(IW) $(MKDIR_P)			$(rti_dir)/k	$(LOG)
  $(IW) cp -afv $(iti_dir)/k/kvt*	$(rti_dir)/k/	$(LOG)
  $(IW) cp -afv $(iti_dir)/k/ktm*	$(rti_dir)/k/	$(LOG)
  $(IW) cp -afv $(iti_dir)/k/kterm*	$(rti_dir)/k/	$(LOG)
  $(IW) cp -afv $(iti_dir)/k/konsole*	$(rti_dir)/k/	$(LOG)
  $(IW) cp -afv $(iti_dir)/k/kermit*	$(rti_dir)/k/	$(LOG)

  $(IW) $(MKDIR_P)			$(rti_dir)/j	$(LOG)
  $(IW) cp -afv $(iti_dir)/j/*xterm*	$(rti_dir)/j/	$(LOG)

  $(IW) $(MKDIR_P)			$(rti_dir)/h	$(LOG)
  $(IW) cp -afv $(iti_dir)/h/*hurd*	$(rti_dir)/h/	$(LOG)
  $(IW) cp -afv $(iti_dir)/h/*ansi*	$(rti_dir)/h/	$(LOG)

  $(IW) $(MKDIR_P)			$(rti_dir)/g	$(LOG)
  $(IW) cp -afv $(iti_dir)/g/*gnome*	$(rti_dir)/g/	$(LOG)

  $(IW) $(MKDIR_P)			$(rti_dir)/e	$(LOG)
  $(IW) cp -afv $(iti_dir)/e/*eterm*	$(rti_dir)/e/	$(LOG)

  $(IW) $(MKDIR_P)			$(rti_dir)/d	$(LOG)
  $(IW) cp -afv $(iti_dir)/d/dumb	$(rti_dir)/d/	$(LOG)
  $(IW) cp -afv $(iti_dir)/d/dec*	$(rti_dir)/d/	$(LOG)

  $(IW) $(MKDIR_P)			$(rti_dir)/c	$(LOG)
  $(IW) cp -afv $(iti_dir)/c/cygwin*	$(rti_dir)/c/	$(LOG)
  $(IW) cp -afv $(iti_dir)/c/crt*	$(rti_dir)/c/	$(LOG)

  $(IW) $(MKDIR_P)			$(rti_dir)/a	$(LOG)
  $(IW) cp -afv $(iti_dir)/a/aterm*	$(rti_dir)/a/	$(LOG)
  $(IW) cp -afv $(iti_dir)/a/ansi*	$(rti_dir)/a/	$(LOG)
  $(IW) cp -afv $(iti_dir)/a/aixterm*	$(rti_dir)/a/	$(LOG)
  $(IW) cp -afv $(iti_dir)/a/*		$(rti_dir)/a/	$(LOG)

  $(IW) $(MKDIR_P)			$(rti_dir)/E	$(LOG)
  $(IW) cp -afv $(iti_dir)/E/Eterm*	$(rti_dir)/E/	$(LOG)
endef

define INSTALL_CMD
  $(INSTALL_CMD_DEFAULT)
  $(IW) chmod +x $(libs_so) $(LOG)
endef

ifneq ($(PLATFORM),build)
  define INSTALL_ROOT_CMD
    $(IW) $(INSTxFILES) $(libs_so)               $(RLIB_DIR)	$(LOG)

    $(IW) $(MKDIR_P)                           $(RSHARE_DIR)	$(LOG)
    $(IW) cp -afv     $(ISHARE_DIR)/tabset     $(RSHARE_DIR)	$(LOG)

    $(terminfo_install_small)
  endef
 #$(IW) $(INST_FILES) $(ILIB_DIR)/terminfo     $(RLIB_DIR)	$(LOG)
 #$(terminfo_install_big)
endif


include $(RULES)
