PKG_TYPE          = DEBIAN1
PKG_VERSION       = 1.1
PKG_VER_PATCH     = -6
PKG_SITE_PATH     = pool/main/c/$(PKG_BASE)

PLATFORM          = build

include $(DEFS)


build : $(call bstamp_f,install,zlib)

STD_CPPFLAGS += -Du32=__u32
MK_VARS = $(STD_VARS)

    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)

  INSTALL_CMD = $(IW) $(INSTxFILES)	\
	$(PKG_BLD_DIR)/mkcramfs		\
	$(PKG_BLD_DIR)/cramfsck			$(BBIN_DIR) $(LOG)


include $(RULES)
