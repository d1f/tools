# http://genext2fs.sourceforge.net/
PKG_TYPE          = DEBIAN3
PKG_VERSION       = 1.4.1
PKG_VER_PATCH     = -4
PKG_SITE_PATH     = pool/main/g/$(PKG_BASE)

PLATFORM          = build

include $(DEFS)


PKG_PATCH2_FILES += $(PKG_PKG_DIR)/*.patch


AC_VER = 2.69
include $(TOOLS_PKG_DIR)/autoconf.mk

AM_VER = 1.11
include $(TOOLS_PKG_DIR)/automake.mk

define PRE_CONFIGURE_CMD
  cd $(PKG_BLD_DIR) && $(ACLOCAL)   $(ACLOCAL_FLAGS)	$(LOG)
  cd $(PKG_BLD_DIR) && $(AUTOMAKE) $(AUTOMAKE_FLAGS)	$(LOG)
  cd $(PKG_BLD_DIR) && $(AUTOCONF) $(AUTOCONF_FLAGS)	$(LOG)
endef


define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) && \
     ./configure -C	$(LOG)	\
	--prefix=$(INST_DIR)	\
	$(STD_VARS)
endef


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)
  INSTALL_CMD = $(IW) $(INSTxFILES) $(PKG_BLD_DIR)/genext2fs	\
				       $(BBIN_DIR)	$(LOG)


include $(RULES)
