PKG_TYPE          = DEBIAN_NATIVE
PKG_VERSION       = 20161112.1
PKG_SITE_PATH     = pool/main/a/$(PKG_BASE)

PLATFORM          = build

include $(DEFS)


define INSTALL_CMD
 $(IW) $(INSTxFILES)			\
	$(PKG_BLD_DIR)/config.guess	\
	$(PKG_BLD_DIR)/config.sub	\
			$(BSHARE_DIR)/misc $(LOG)
endef


include $(RULES)
