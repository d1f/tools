include $(TOP_DIR)/tools/pkg/klibc.mk

ifeq ($(KLIBC_VERSION),1.5.12)
  PKG_TYPE          = DEBIAN1
  PKG_VERSION       = $(KLIBC_VERSION)
  PKG_VER_PATCH     = -2lenny1
  PKG_BASE          = klibc
  PKG_SITE_PATH     = pool/main/k/$(PKG_BASE)
  EXTRACT_STRIP_NUM = 0
else ifeq ($(KLIBC_VERSION),1.5.20)
  PKG_TYPE          = DEBIAN3
  PKG_VERSION       = $(KLIBC_VERSION)
  PKG_VER_PATCH     = -1+squeeze1
  PKG_BASE          = klibc
  PKG_SITE_PATH     = pool/main/k/$(PKG_BASE)
else
  PKG_TYPE          = ORIGIN
  PKG_VERSION       = $(KLIBC_VERSION)
  PKG_BASE          = klibc
  PKG_SITES         = $(LK_ORG_SITES)
  PKG_SITE_PATH     = libs/klibc/1.5
endif

include $(DEFS)



ifeq ($(PKG_TYPE),DEBIAN1)
  PKG_PATCH2_FILES  = $(PKG_PATCHED_SRC_DIR)/debian/patches/*.diff
  PKG_PATCH2_FILES += $(PKG_PATCHED_SRC_DIR)/debian/patches/*.patch
endif

PKG_PATCH2_FILES += $(PKG_PKG_DIR)/*.patch

ifeq ($(PKG_TYPE),DEBIAN3)
 define PATCH_CMD
  echo -n "$(I2)Extracting debian part ... "
  $(ZCAT) $(DL_DIR)/`cat $(FOUND_FILE_PATCH)` | \
  $(TAR) -C $(PKG_PATCHED_SRC_DIR) -xvf - $(LOG)
  echo "done"

  diff -uw /dev/null /dev/null > $(PKG_PATCHED_SRC_DIR)/debian/patches/klibc-linux-libc-dev

  $(APPLY_QUILT_PATCHES_CMD)
 endef
endif


configure : $(call bstamp_f,install,bison m4)
ifndef EXTERN_TOOLCHAIN
configure : $(call tstamp_f,install,gcc-nolibc)
endif
configure : $(call tstamp_f,depend,linux)

define CONFIGURE_CMD
  $(LN_S) $(LK_DIR) $(PKG_BLD_DIR)/linux $(LOG)
endef


MK_VARS  = KLIBCARCH=$(NARCH)
MK_VARS += CROSS_COMPILE=$(CROSS_PREFIX)
MK_VARS += HOSTCC=$(BUILD_CC)
MK_VARS += KLIBCKERNELSRC=$(LK_DIR)/
#MK_VARS += KBUILD_VERBOSE=1

# our own toolchain with ARM EABI (ABI from platform definition)
# $(CC) -dumpmachine: arm*-*-linux-gnueabi
ifeq ($(ABI),eabi)
  MK_VARS += CONFIG_AEABI=y
endif

MK_VARS += INSTALLROOT=$(INST_KLIBC_DIR)/
MK_VARS += INSTALLDIR=
MK_VARS += prefix=
ENV_VARS += PERL5LIB=$(PKG_BLD_DIR)


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)

uninstall : unlink # raw uninstall breaks following install, rebuild

define INSTALL_CMD
  $(INSTALL_CMD_DEFAULT)
  $(RM_R) $(INST_DIR)/klibc/include/mtd $(LOG)
endef


include $(RULES)
