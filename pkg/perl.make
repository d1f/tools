include $(TOP_DIR)/tools/pkg/perl.mk # PERL_VERSION

PKG_TYPE      = DEBIAN3
PKG_VERSION   = $(PERL_VERSION)
PKG_VER_PATCH = $(PERL_VER_PATCH)

PLATFORM      = build

include $(DEFS)


define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) &&		\
     unset $(PERL_UNSET);	\
     ./Configure		\
	-Dprefix=$(INST_DIR)	\
	-Dcc=$(BUILD_CC)	\
	-d -e $(LOG)
endef


  BUILD_CMD =   $(BUILD_CMD_DEFAULT)
INSTALL_CMD = $(INSTALL_CMD_DEFAULT)


include $(RULES)
