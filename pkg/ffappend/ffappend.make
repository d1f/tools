PKG_ORIG_DIR     = $(PKG_PKG_DIR)/src
PKG_FETCH_METHOD = DONE
SEPARATE_SRC_BLD_DIRS = defined
PLATFORM         = build

include $(DEFS)


libs = df df-nothread
configure : $(call tstamp_f,install,$(libs))

BUILD_TYPE     = SINGLE_SRC_BINS
LIBS           = $(libs)
MAKES_MAKEFILE = $(TOOLS_DIR)/makes/all.Makefile
include          $(TOOLS_DIR)/makes/conf.mk
CONFIGURE_CMD  = $(MAKES_CONFIGURE_CMD)


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)
  INSTALL_CMD =   $(INSTALL_CMD_DEFAULT)
  INSTALL_ROOT_CMD = $(INSTALL_ROOT_CMD_DEFAULT)


include $(RULES)
