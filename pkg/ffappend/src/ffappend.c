#define _GNU_SOURCE
#include <string.h>   // basename, memset
#include <stdio.h>
#include <stdlib.h>   // EXIT_FAILURE, EXIT_SUCCESS
#include <error.h>
#include <errno.h>
#include <fcntl.h>    // open
#include <unistd.h>   // ftruncate
#include <sys/stat.h> // stat
#include <df/write-all.h>
#include <df/x.h>


static const char *fname;
static struct stat statbuf;

static int my_write(int fd, const char *buf, size_t size)
{
    int rc = df_write_all(fd, buf, size);
    if (rc < 0)
    {
	error(EXIT_SUCCESS, errno, "Can't write to %s", fname);

	rc = ftruncate(fd, statbuf.st_size);
	if (rc < 0)
	    error(EXIT_SUCCESS, errno,
		  "Can't restore (truncate) file %s to the old size", fname);
    }

    return rc;
}

int main(int ac, char *av[])
{
    if (ac != 3)
    {
	fprintf(stderr, "Usage: %s file new-size\n", basename(av[0]));
	return EXIT_FAILURE;
    }

    fname           =         av[1];
    long long fsize = strtoll(av[2], NULL, 0);

    int rc = stat(fname, &statbuf);
    if (rc < 0)
    {
	if (errno != ENOENT)
	    error(EXIT_FAILURE, errno, "Can't stat %s", fname);
    }

    if (rc >= 0)
    {
	if (statbuf.st_size >= fsize)
	    return EXIT_SUCCESS;
    }

    static const size_t msize = 1024*1024; // 1 MB

    char *mbuf = df_xmalloc(msize);
    memset(mbuf, 0xff, msize);

    int fd = df_xopen(fname, O_WRONLY|O_APPEND|O_CREAT|O_LARGEFILE, 0600);

    long long appsize = fsize - statbuf.st_size;

    size_t appbufs = appsize / msize;
    size_t i;
    for (i=0; i < appbufs; ++i)
    {
	rc = my_write(fd, mbuf, msize);
	if (rc < 0)
	    return EXIT_FAILURE;
    }

    rc = my_write(fd, mbuf, appsize % msize);
    if (rc < 0)
	return EXIT_FAILURE;

    rc = close(fd);
    if (rc < 0)
	error(EXIT_FAILURE, errno, "Can't close %s", fname);

    return EXIT_SUCCESS;
}
