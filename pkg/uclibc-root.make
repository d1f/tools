PKG_VERSION = $(LIBC_VERSION)
PKG_HAS_NO_SOURCES = defined

include $(DEFS)


include $(TOOLS_PKG_DIR)/uclibc-vars.mk

install : $(call tstamp_f,install,uclibc)

# uclibc-vars.mk: RUNTIME_PREFIX=$(tc_SYSROOT)/
define INSTALL_ROOT_CMD
  $(IW) $(INSTxFILES)	$(tc_SYSROOT)/lib/lib*.so		\
			$(tc_SYSROOT)/lib/lib*.so.*		\
			$(tc_SYSROOT)/lib/ld-uClibc*.so		\
			$(tc_SYSROOT)/lib/ld-uClibc*.so.*	\
							$(RLIB_DIR) $(LOG)
  $(IW) $(RM) $(RLIB_DIR)/libmudflap*.so* $(LOG)
endef


include $(RULES)
