PKG_TYPE      = DEBIAN3
PKG_VERSION   = 2.1.0
PKG_VER_PATCH = -6
PKG_SITE_PATH = pool/main/e/$(PKG_BASE)

include $(DEFS)


configure : $(TOOLCHAIN_INSTALL_STAMP)

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) && \
     ./configure -C $(LOG)	\
	--prefix=$(INST_DIR)	\
	--build=$(BUILD)	\
	--host=$(TARGET)	\
	--target=$(TARGET)	\
	--with-gnu-ld		\
	--disable-static	\
	--enable-shared		\
	$(STD_VARS)
endef


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)
  INSTALL_CMD = $(INSTALL_CMD_DEFAULT)
  INSTALL_ROOT_CMD = $(IW) $(INSTxFILES) $(ILIB_DIR)/libexpat.so* $(RLIB_DIR) $(LOG)


include $(RULES)
