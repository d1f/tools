ifndef	tools/pkg/perl.mk
	tools/pkg/perl.mk = included

PERL_VERSION   = 5.20.2
PERL_VER_PATCH = -3+deb8u6
PKG_SITES      = $(DEB_ORG_SITES)
PKG_SITE_PATH ?= pool/main/p/$(PKG_BASE)

PERL_UNSET= PERL_MB_OPT PERL_MM_OPT
PERL_PATH = $(BINST_DIR)/bin
PERL_BIN  = $(PERL_PATH)/perl
PERL_LIB  = $(BINST_DIR)/lib/perl5 # use in PERL5LIB env var


endif # tools/pkg/perl.mk
