PKG_TYPE      = DEBIAN3
PKG_VERSION   = 0.51.0
PKG_VER_PATCH = -3
PKG_SITE_PATH = pool/main/i/$(PKG_BASE)

PLATFORM      = build

include $(DEFS)


configure : $(call bstamp_f,install,libxml-parser-perl)

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) && ./configure -C $(LOG)	\
	--prefix=$(INST_DIR)			\
	$(STD_VARS)
endef


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)
  INSTALL_CMD =   $(INSTALL_CMD_DEFAULT)


include $(RULES)
