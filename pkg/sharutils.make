ifneq ($(strip $(shell which unshar 2>/dev/null)),)

PKG_HAS_NO_SOURCES = defined

else # has no sharutils

PKG_TYPE      = DEBIAN3
PKG_VERSION   = 4.15.2
PKG_VER_PATCH = -1
PKG_SITE_PATH = pool/main/s/$(PKG_BASE)

PLATFORM      = build

include $(DEFS)


define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) &&			\
     ./configure $(LOG)			\
	--prefix=$(INST_DIR)		\
	--disable-nls			\
	--with-gnu-ld			\
	--with-gnu-as			\
	--without-libiconv-prefix	\
	--without-libintl-prefix	\
	$(STD_VARS)
endef


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
  INSTALL_CMD =   $(INSTALL_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)


endif # has no sharutils

include $(RULES)
