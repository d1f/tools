PKG_VERSION   = $(LK_VERSION)
PKG_HAS_NO_SOURCES = defined

include $(DEFS)


configure : $(call tstamp_f,depend,linux)

ifeq ($(NARCH),powerpc)
  lkarch = $(LK_DIR)/arch/$(NARCH)/include/asm
  define _INSTALL_CMD2_
    $(IW) cp $(lkarch)/socket.h		$(tc_HEADERDIR)/asm/ $(LOG)
    $(IW) cp $(lkarch)/unistd.h		$(tc_HEADERDIR)/asm/ $(LOG)
    $(IW) cp $(lkarch)/errno.h		$(tc_HEADERDIR)/asm/ $(LOG)
    $(IW) cp $(lkarch)/param.h		$(tc_HEADERDIR)/asm/ $(LOG)
    $(IW) cp $(lkarch)/sigcontext.h	$(tc_HEADERDIR)/asm/ $(LOG)
    $(IW) cp $(lkarch)/ptrace.h		$(tc_HEADERDIR)/asm/ $(LOG)
    $(IW) cp $(lkarch)/sockios.h	$(tc_HEADERDIR)/asm/ $(LOG)
    $(IW) cp $(lkarch)/posix_types.h	$(tc_HEADERDIR)/asm/ $(LOG)
    $(IW) cp $(lkarch)/ioctls.h		$(tc_HEADERDIR)/asm/ $(LOG)
    $(IW) cp $(lkarch)/ioctl.h		$(tc_HEADERDIR)/asm/ $(LOG)
    $(IW) cp $(lkarch)/types.h		$(tc_HEADERDIR)/asm/ $(LOG)
    $(IW) cp $(lkarch)/bitsperlong.h	$(tc_HEADERDIR)/asm/ $(LOG)
    $(IW) cp $(lkarch)/byteorder.h	$(tc_HEADERDIR)/asm/ $(LOG)
    $(IW) cp $(lkarch)/swab.h		$(tc_HEADERDIR)/asm/ $(LOG)
  endef
endif

ifeq ($(shell $(VERCMP) $(LK_VERSION) '>=' 2.6.18),true)
  # Install sanitised kernel headers to tc_HEADERDIR = $(tc_SYSROOT)/include
  define INSTALL_CMD
    $(IWMAKE) -C $(LK_DIR) ARCH=$(NARCH) headers_install INSTALL_HDR_PATH=$(tc_SYSROOT) $(LOG)
    if test -e $(LK_INC_DIR)/linux/if_addr.h; then \
    $(IW) cp $(LK_INC_DIR)/linux/if_addr.h                       $(tc_HEADERDIR)/linux/ $(LOG); \
    fi
  endef
else
  # The old manual way
  define INSTALL_CMD
    $(IW) $(MKDIR_P)                            $(tc_HEADERDIR)             $(LOG)
    $(IW) chmod -Rf u+w                         $(tc_HEADERDIR)             $(LOG)
    $(IW) cp -r $(LK_INC_DIR)/asm-generic       $(tc_HEADERDIR)/asm-generic $(LOG)
    $(IW) cp -r $(LK_INC_DIR)/linux             $(tc_HEADERDIR)             $(LOG)
    $(IW) cp -r $(LK_INC_DIR)/asm-$(LK_SRCARCH) $(tc_HEADERDIR)             $(LOG)
    $(IW) cp -a $(LK_INC_DIR)/asm               $(tc_HEADERDIR)             $(LOG)
    $(_INSTALL_CMD2_)
  endef
endif


include $(RULES)
