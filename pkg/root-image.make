PKG_HAS_NO_SOURCES = defined

include $(DEFS)


ifndef NO_IMAGE

# ROOT_IMAGE_BOOT_PROMPTS:
-include $(PRJ_PKG_DIR)/root-image.mk

install : uninstall # install anyway
install : $(call tstamp_f,install-root,root)
install : $(call tstamp_f,install-root,linux)
install : $(call tstamp_f,reinstall,root-clean)
ifndef DEBUG
install : $(call tstamp_f,reinstall,root-strip)
endif
#install : $(call tstamp_f,reinstall,conf-image)
install : $(IMAGES_DIR)/.dir $(LABEL_IMAGES_DIR)/.dir

DEVICE_TABLE     ?= $(PLATFORM_DIR)/device_table


ifeq ($(strip $(PART_ROOT_FS)),jffs2)

  install : $(call bstamp_f,install-root,mtd-utils)

  MKJFFS2        = $(BBIN_DIR)/mkfs.jffs2
  SUMTOOL        = $(BBIN_DIR)/sumtool

  MKJFFS2_FLAGS  = --root=$(ROOT_DIR)
  MKJFFS2_FLAGS +=     -o $(ROOT_IMAGE).nosum
  SUMTOOL_FLAGS +=     -i $(ROOT_IMAGE).nosum
  SUMTOOL_FLAGS +=     -o $(ROOT_IMAGE)
  MKJFFS2_FLAGS +=   --pagesize=$(FLASH_PAGE_SIZE)
  MKJFFS2_FLAGS += --eraseblock=$(FLASH_ERASE_SIZE)
  SUMTOOL_FLAGS += --eraseblock=$(FLASH_ERASE_SIZE)
  MKJFFS2_FLAGS += --no-cleanmarkers
  SUMTOOL_FLAGS += --no-cleanmarkers
  MKJFFS2_FLAGS += --devtable=$(DEVICE_TABLE)
  MKJFFS2_FLAGS += --squash
  MKJFFS2_FLAGS += --pad
  SUMTOOL_FLAGS += --pad
  MKJFFS2_FLAGS += -t
  MKJFFS2_FLAGS += -v
  SUMTOOL_FLAGS += -v
  MKJFFS2_FLAGS += -x lzo -x rtime # disable compressors
  MKJFFS2_FLAGS += -X zlib # enable zlib compressor
  MKJFFS2_FLAGS += --compression-mode=size

  ifeq      ($(ENDIANESS),LITTLE)
    MKJFFS2_FLAGS += --little-endian
    SUMTOOL_FLAGS += --littleendian
  else ifeq ($(ENDIANESS),BIG)
    MKJFFS2_FLAGS += --big-endian
    SUMTOOL_FLAGS += --bigendian
  else
    $(error Unknown ENDIANESS $(ENDIANESS), only LITTLE, BIG are known)
  endif

  ifdef ROOT_IMAGE_NO_SUMTOOL
    define MKIMAGE_CMD
      $(IW) $(MKJFFS2) $(MKJFFS2_FLAGS)	$(LOG)
      $(IW) $(LN) -f $(ROOT_IMAGE).nosum $(ROOT_IMAGE) $(LOG)
    endef
  else
    define MKIMAGE_CMD
      $(IW) $(MKJFFS2) $(MKJFFS2_FLAGS)	$(LOG)
      echo -n "$(I1)sumtool ... "
      $(IW) $(SUMTOOL) $(SUMTOOL_FLAGS)	$(LOG)
      echo done
    endef
  endif

else ifeq ($(strip $(PART_ROOT_FS)),squashfs)

  install : $(call bstamp_f,install,squashfs)
  MKSQUASHFS        = $(BBIN_DIR)/mksquashfs
  MKSQUASHFS_FLAGS  = -no-recovery
# MKSQUASHFS_FLAGS += -info
  MKSQUASHFS_FLAGS += -no-exports
  MKSQUASHFS_FLAGS += -no-progress
# MKSQUASHFS_FLAGS += -no-sparse
# MKSQUASHFS_FLAGS += -b <block_size> #Default 131072 bytes
# MKSQUASHFS_FLAGS += -no-duplicates
  MKSQUASHFS_FLAGS += -noappend
  MKSQUASHFS_FLAGS += -all-root
  MKSQUASHFS_FLAGS += -check_data

  ifeq      ($(ENDIANESS),LITTLE)
    MKSQUASHFS_FLAGS += -le
  else ifeq ($(ENDIANESS),BIG)
    MKSQUASHFS_FLAGS += -be
  else
    $(error Unknown ENDIANESS $(ENDIANESS), only LITTLE, BIG are known)
  endif

  define MKIMAGE_CMD
    $(IW) $(MKSQUASHFS) $(ROOT_DIR) $(ROOT_IMAGE) $(MKSQUASHFS_FLAGS) $(LOG)
  endef

else ifeq ($(strip $(PART_ROOT_FS)),cramfs)

  install : $(call bstamp_f,install,cramfs fakeroot)

  FAKEROOT = $(BBIN_DIR)/fakeroot

  MKCRAMFS  = $(BBIN_DIR)/mkcramfs
  MKCRAMFS_FLAGS  = -v -z #-b 4096

  define MKIMAGE_CMD
    $(IW) $(FAKEROOT) /bin/sh -c \
       "$(PLATFORM_DIR)/mknodes $(ROOT_DIR)/dev; \
       chown -R 0:0 $(ROOT_DIR); \
       $(MKCRAMFS) $(MKCRAMFS_FLAGS) $(ROOT_DIR) $(ROOT_IMAGE)" $(LOG)
  endef

else ifeq ($(strip $(PART_ROOT_FS)),ext2)

  install : $(call bstamp_f,install,genext2fs e2fsprogs)

  define MKIMAGE_CMD
    IMAGE_NAME=Root			\
    IMAGE_DIR=$(ROOT_DIR)		\
    IMAGE_SIZE=$(PART_ROOT_SIZE)	\
    IMAGE_FILE=$(ROOT_IMAGE)		\
    PLATFORM_DEV_TABLE=$(DEVICE_TABLE)	\
    INDENT="$(I1)"			\
    IW="$(IW)"				\
	$(TOOLS_DIR)/script/mk-e2fs-image $(LOG)

    #echo -n "$(I1)Checking resulted image size ... "
    #$(scr)/check-size $(ROOT_IMAGE) $(PART_ROOT_SIZE) 1024	$(LOG)
    #echo done

    echo -n "$(I1)Fixing root image ... "
    { $(BBIN_DIR)/e2fsck -f -y -D $(ROOT_IMAGE);	\
	test $$? -le 3 || exit 1; } $(LOG)
    echo done

    echo -n "$(I1)Tuning root image ... "
    $(BBIN_DIR)/tune2fs -c0 -i0 -e continue -L Root -m0 -r0 \
	$(ROOT_IMAGE) $(LOG)
    echo done

    echo -n "$(I1)Fixing root image (after tunefs) ... "
    { $(BBIN_DIR)/e2fsck -f -y -D $(ROOT_IMAGE);	\
	test $$? -le 3 || exit 1; } $(LOG)
    echo done

    #$(RM) $(ROOT_IMAGE).gz					$(LOG)
    #echo -n "$(I1)Compressing ... "
    #$(IWSH) "gzip -9vc $(ROOT_IMAGE) > $(ROOT_IMAGE).gz"	$(LOG)
    #echo done

    #$(check_compressed_image_size)				$(LOG)
  endef

else
  ifneq ($(PLATFORM),build)
	$(error Unsupported PART_ROOT_FS type $(PART_ROOT_FS))
  endif
endif


ifeq ($(filter ROOT,$(INSTALL_IMAGES_TO_LIST)),ROOT)
  inst_files  = $(ROOT_IMAGE)
endif
ifeq ($(filter ROOT.MD5,$(INSTALL_IMAGES_TO_LIST)),ROOT.MD5)
  inst_files += $(ROOT_IMAGE).md5
endif
ifeq ($(filter ROOT.CKSUM,$(INSTALL_IMAGES_TO_LIST)),ROOT.CKSUM)
  inst_files += $(ROOT_IMAGE).cksum
endif

ifneq ($(strip $(INST_IMAGES_TO)),)
 ifneq ($(inst_files),)
  define INSTALL_IMAGES_TO_CMD
    echo -n "$(I1)installing to $(INST_IMAGES_TO) ... "
    $(IW) $(INST_FILES) $(inst_files) $(INST_IMAGES_TO) $(LOG)
    echo done
  endef
 endif
endif

install : $(call tstamp_f,reinstall-root,root-conf root-dirs-common)

define INSTALL_CMD
  echo "$(I1)Creating root image ... "

  $(RM) $(ROOT_DIR)/.dir
  $(RM) $(ROOT_IMAGE)*

  $(MKIMAGE_CMD)

  echo -n "$(I1)md5sum  ... "
  $(IWSH) "cd $(LABEL_IMAGES_DIR) && md5sum $(notdir $(ROOT_IMAGE)) > $(ROOT_IMAGE).md5"
  echo done

  echo -n "$(I1) cksum  ... "
  $(IWSH) "cd $(LABEL_IMAGES_DIR) &&  cksum $(notdir $(ROOT_IMAGE)) > $(ROOT_IMAGE).cksum"
  echo done

  $(INSTALL_IMAGES_TO_CMD)

  $(ROOT_IMAGE_BOOT_PROMPTS)
endef


    CLEAN_CMD = $(RM) $(ROOT_IMAGE)*			$(LOG)
DISTCLEAN_CMD = $(CLEAN_CMD)


uninstall: distclean
uninstall: $(call tstamp_f,uninstall,root-strip)
uninstall: $(call tstamp_f,uninstall,root-clean)

reinstall: uninstall install

endif # ndef NO_IMAGE


include $(RULES)
