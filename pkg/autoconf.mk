ifndef	tools/pkg/autoconf.mk
	tools/pkg/autoconf.mk = included

define __autoconf_usage__

AC_VER = 2.69
include $(TOOLS_PKG_DIR)/autoconf.mk

PRE_CONFIGURE_CMD = $(AUTOCONF_CMD)

endef

$(if $(AC_VER),,$(error AC_VER undefined))


# output vars:

AC_PREFIX  = $(BINST_DIR)/autoconf-$(AC_VER)
AC_PATH    = $(AC_PREFIX)/bin
PATH_AC    = $(AC_PATH):$(PATH)
AUTOCONF   = $(AC_PATH)/autoconf
AUTOM4TE   = $(AC_PATH)/autom4te
AUTOHEADER = $(AC_PATH)/autoheader
AUTORECONF = $(AC_PATH)/autoreconf

AC_VARS    = AUTOCONF=$(AUTOCONF) AUTOM4TE=$(AUTOM4TE)
AC_VARS   += AUTOHEADER=$(AUTOHEADER) AUTORECONF=$(AUTORECONF)

AUTOHEADER_FLAGS = --force #--verbose
AUTORECONF_FLAGS = --force #--verbose
  AUTOCONF_FLAGS = --force #--verbose

ifeq ($(filter autoconf%,$(PKG)),)
pre-configure : $(call bstamp_f,install,autoconf-$(AC_VER))
else
pre-configure : $(call bstamp_f,install,sed m4)
endif

export PATH := $(PATH_AC)


AUTOCONF_CMD = cd $(PKG_BLD_SUB_DIR) && $(AUTOCONF) $(AUTOCONF_FLAGS) $(LOG)


endif # tools/pkg/autoconf.mk
