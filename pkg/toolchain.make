# Whole toolchain responsibility package.
# Be care with destruction targets - it destructs all prerequisite packages.

PKG_HAS_NO_SOURCES = defined

include $(DEFS)


NESTED_PKGES = $(TOOLCHAIN_DEP_PKGES)


include $(RULES)
