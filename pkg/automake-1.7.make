AC_VER = 2.59
AM_VER = 1.7

PKG_TYPE      = DEBIAN1
PKG_VER_MAJOR = $(AM_VER)
PKG_VER_MINOR = 9
PKG_VER_PATCH = -9.1+squeeze1
PKG_BASE      = automake$(PKG_VER_MAJOR)

include $(TOP_DIR)/tools/pkg/automake-common.mk
