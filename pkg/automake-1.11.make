AC_VER = 2.69

AM_VER_MAJOR = 1.11
AM_VER_MINOR = 6
AM_VER       = $(AM_VER_MAJOR)

PKG_TYPE      = DEBIAN3
PKG_VER_MAJOR = $(AM_VER_MAJOR)
PKG_VERSION   = $(AM_VER_MAJOR).$(AM_VER_MINOR)
PKG_VER_PATCH = -4
PKG_BASE      = automake$(PKG_VER_MAJOR)

include $(TOP_DIR)/tools/pkg/automake-common.mk
