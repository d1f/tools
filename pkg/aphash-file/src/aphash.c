#include <stdio.h>  // printf
#include <stdlib.h> // EXIT_*
#include <stddef.h> // size_t
#include <error.h>
#include <errno.h>
#include <fcntl.h>    // open
#include <unistd.h>   // close
#include <sys/mman.h> // mmap
#include <sys/stat.h> // fstat

#ifndef  O_NOATIME
# define O_NOATIME 0
#endif


unsigned int APHash(const char* str, unsigned int len)
{
   unsigned int hash = 0xAAAAAAAA;
   unsigned int i    = 0;

   for(i = 0; i < len; str++, i++)
   {
      hash ^= ((i & 1) == 0) ? (  (hash <<  7) ^  (*str) * (hash >> 3)) :
                               (~((hash << 11) + ((*str) ^ (hash >> 5))));
   }

   return hash;
}

int main(int ac, char *av[])
{
    if (ac != 2)
    {
	fprintf(stderr, "Usage: %s file\n", av[0]);
	return EXIT_FAILURE;
    }

    const char *name = av[1];
    struct stat sb;
    if (stat(name, &sb) < 0)
    {
	error(EXIT_SUCCESS, errno, "%s(): stat on %s failed", __func__, name);
	return EXIT_FAILURE;
    }

    int fd = open(name, O_RDONLY | O_NOATIME | O_NOCTTY);
    if (fd < 0)
    {
	error(EXIT_SUCCESS, errno, "%s(): Can't open %s", __func__, name);
	return EXIT_FAILURE;
    }

    char *map = mmap(NULL, sb.st_size, PROT_READ, MAP_PRIVATE, fd, 0);
    if (map == MAP_FAILED)
    {
	error(EXIT_SUCCESS, errno, "%s(): mmap on %s failed", __func__, name);
	return EXIT_FAILURE;
    }

    unsigned int hash = APHash(map, sb.st_size);

    printf("0x%08x\n", hash);
    return EXIT_SUCCESS;
}
