ifndef	tools/pkg/toolchain.mk
	tools/pkg/toolchain.mk = included


ifndef	    xwmake/defs.mk
$(error	$(I)xwmake/defs.mk did not included)
endif


# BINUTILS_VERSION = 2.15	# goes from PRJ_DIR/setup.sh
#      GCC_VERSION = 2.95.3	# goes from PRJ_DIR/setup.sh
#     LIBC_TYPE    = g		# goes from PRJ_DIR/setup.sh
#     LIBC_VERSION = 2.2.5	# goes from PRJ_DIR/setup.sh

ifneq ($(PLATFORM),build)
  tc_PREFIX     = $(INST_DIR)
  tc_SYSROOT    = $(tc_PREFIX)/$(TARGET)
  tc_HEADERDIR  = $(tc_SYSROOT)/include
  tc_BFD_PREFIX = $(tc_PREFIX)/$(BUILD)/$(TARGET)
else
  tc_PREFIX     = $(INST_DIR)
  tc_SYSROOT    = $(tc_PREFIX)
  tc_HEADERDIR  = $(tc_SYSROOT)/include
  tc_BFD_PREFIX = $(tc_PREFIX)
endif


ifdef EXTERN_TOOLCHAIN

  # FIXME
  ifndef    CROSS_PREFIX
    $(error CROSS_PREFIX is not defined for EXTERN_TOOLCHAIN)
  endif
  #CROSS_PREFIX ?= /usr/local/bin/arm-linux-

else

  CROSS_PREFIX ?= $(tc_PREFIX)/bin/$(TARGET)-

  BTGPP = $(tc_PREFIX)/bin/$(TARGET)-cpp
  BTGCC = $(tc_PREFIX)/bin/$(TARGET)-gcc
  BTGXX = $(tc_PREFIX)/bin/$(TARGET)-g++
  BTGLD = $(tc_PREFIX)/bin/$(TARGET)-ld

  TBGPP = $(tc_SYSROOT)/bin/cpp
  TBGCC = $(tc_SYSROOT)/bin/gcc
  TBGXX = $(tc_SYSROOT)/bin/g++
  TBGLD = $(tc_SYSROOT)/bin/ld

 ifneq ($(PLATFORM),build)
  GCC_INSTALL_DEP_STAMP = $(call tstamp_f,install,gccwrapper)
  GLD_INSTALL_DEP_STAMP = $(GCC_INSTALL_DEP_STAMP)
  GPPWRAPPER_BIN = $(BLD_DIR)/gccwrapper/gppwrapper
  GCCWRAPPER_BIN = $(BLD_DIR)/gccwrapper/gccwrapper
  GXXWRAPPER_BIN = $(BLD_DIR)/gccwrapper/gxxwrapper
  GLDWRAPPER_BIN = $(BLD_DIR)/gccwrapper/gldwrapper

  define MANGLE_INSTALLED_GPP
    $(IW) mv    -v $(BTGPP)          $(BTGPP)__ $(LOG)
    $(IW) $(LN) -v $(GPPWRAPPER_BIN) $(BTGPP)   $(LOG)

    if test -e $(TBGPP); then \
       $(IW) mv    -v $(TBGPP)          $(TBGPP)__ $(LOG); \
       $(IW) $(LN) -v $(GPPWRAPPER_BIN) $(TBGPP)   $(LOG); \
    fi
  endef

  define MANGLE_INSTALLED_GCC
    $(IW) mv    -v $(BTGCC)          $(BTGCC)__ $(LOG)
    $(IW) $(LN) -v $(GCCWRAPPER_BIN) $(BTGCC)   $(LOG)

    if test -e $(TBGCC); then \
       $(IW) mv    -v $(TBGCC)          $(TBGCC)__ $(LOG); \
       $(IW) $(LN) -v $(GCCWRAPPER_BIN) $(TBGCC)   $(LOG); \
    fi
  endef

  define MANGLE_INSTALLED_GXX
    $(IW) mv    -v $(BTGXX)          $(BTGXX)__ $(LOG)
    $(IW) $(LN) -v $(GXXWRAPPER_BIN) $(BTGXX)   $(LOG)

    if test -e $(TBGPP); then \
       $(IW) mv    -v $(TBGXX)          $(TBGXX)__ $(LOG); \
       $(IW) $(LN) -v $(GXWRAPPER_BIN)  $(TBGXX)   $(LOG); \
    fi
  endef

  define MANGLE_INSTALLED_GLD
    $(IW) mv    -v $(BTGLD)          $(BTGLD)__ $(LOG)
    $(IW) $(LN) -v $(GLDWRAPPER_BIN) $(BTGLD)   $(LOG)

    $(IW) mv    -v $(TBGLD)          $(TBGLD)__ $(LOG)
    $(IW) $(LN) -v $(GLDWRAPPER_BIN) $(TBGLD)   $(LOG)
  endef
 endif #  $(PLATFORM) != build

endif # !EXTERN_TOOLCHAIN

ifneq ($(PLATFORM),build)
  ifndef EXTERN_TOOLCHAIN
    # order is significant
    TOOLCHAIN_DEP_PKGES  = gccwrapper binutils
    TOOLCHAIN_DEP_PKGES += gcc-nolibc
    TOOLCHAIN_DEP_PKGES += $(LIBC_TYPE)libc
    TOOLCHAIN_DEP_PKGES += gcc-libc
    #ksymoops
  endif
endif


endif # tools/pkg/toolchain.mk
