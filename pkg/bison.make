ifneq ($(strip $(shell which $(PKG) 2>/dev/null)),)

PKG_HAS_NO_SOURCES = defined

else # has no bison

PKG_TYPE      = DEBIAN3
PKG_VERSION   = 3.0.2.dfsg
# 3.0.4 requires automake
PKG_VER_PATCH = -2
PKG_SITE_PATH = pool/main/b/$(PKG_BASE)
PLATFORM      = build

include $(DEFS)


define PRE_CONFIGURE_CMD
  # Touch all files in the dependency tree of bison.info to
  # inhibit makeinfo invocation in the build process.
  $(MKDIR_P) $(PKG_BLD_DIR)/doc/figs $(LOG)
  touch --date="Jan 01 2000" \
	$(PKG_BLD_DIR)/doc/bison.info \
	$(PKG_BLD_DIR)/doc/bison.texi \
	$(PKG_BLD_DIR)/doc/bison.help \
	$(PKG_BLD_DIR)/doc/figs/example.txt \
	$(PKG_BLD_DIR)/doc/figs/example-reduce.txt \
	$(PKG_BLD_DIR)/doc/figs/example-shift.txt \
	$(PKG_BLD_DIR)/doc/*.texi \
	$(PKG_BLD_DIR)/build-aux/cross-options.pl \
	$(PKG_BLD_DIR)/src/getargs.c $(LOG)
endef


configure : $(call bstamp_f,install,ccache gawk sed m4)

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) &&			\
     ./configure -C $(LOG)		\
	--prefix=$(INST_DIR)		\
	--enable-threads=posix		\
	--disable-assert		\
	--disable-nls			\
	--with-gnu-ld			\
	--without-libpth-prefix		\
	--without-libiconv-prefix	\
	--without-libintl-prefix	\
	$(STD_VARS)
endef


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)
  INSTALL_CMD =   $(INSTALL_CMD_DEFAULT)


endif # has no bison

include $(RULES)
