#include <stdint.h>
#include <linux/jffs2.h>
#include <mtd-test.h>


int main(void)
{
    PRT(jint32_t, 4);
    PRT( jmode_t, 4);
    PRT(jint16_t, 2);
    PRS(jffs2_unknown_node, 2*2 + 2*4);
    PRS(jffs2_raw_dirent  , 2*2 + 6*4 + 4 + 2*4);
    PRS(jffs2_raw_inode   , 2*2 + 4*4 + 4 + 2*2 + 7*4 + 2 + 2 + 2*4);
    PRS(jffs2_raw_xattr   , 2*2 + 4*4 + 2 +   2 + 2*4);
    PRS(jffs2_raw_xref    , 2*2 + 6*4);
    PRS(jffs2_raw_summary , 2*2 + 7*4);

    return 0;
}
