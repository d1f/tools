#ifndef  MTD_TEST_H
# define MTD_TEST_H

# include <stdio.h>


# define PRS(T,S) do { printf("%-20s: sizeof: %3u, expected: %3u\n", #T, sizeof(struct T), (S)); \
    if (sizeof(struct T) != (S)) fprintf(stderr, "\t SIZE MISMATCH!!!\n"); } while (0)

# define PRT(T,S) do { printf("%-20s: sizeof: %3u, expected: %3u\n", #T, sizeof(T), (S)); \
    if (sizeof(T) != (S)) fprintf(stderr, "\t SIZE MISMATCH!!!\n"); } while (0)


#endif //MTD_TEST_H
