#include <stdint.h>
#include <mtd/mtd-abi.h>
#include <mtd-test.h>

//#define NEW

int main(void)
{
    PRS(erase_info_user, 8);
#ifdef NEW
    PRS(erase_info_user64, 16);
#endif
    PRS(mtd_oob_buf, 12);
#ifdef NEW
    PRS(mtd_oob_buf64, 8+4+4+8);
    PRS(mtd_write_req, 8*5+8);
#endif
    PRS(mtd_info_user, 4+5*4+8);
    PRS(region_info_user, 4*4);
    PRS(otp_info, 3*4);
    PRS(nand_oobinfo, 4*2+4*8*2+4*32);
    PRS(nand_oobfree, 4*2);
#ifdef NEW
    PRS(nand_ecclayout_user, 4+4*MTD_MAX_ECCPOS_ENTRIES+4+(4*2)*MTD_MAX_OOBFREE_ENTRIES);
#endif
    PRS(mtd_ecc_stats, 4*4);

    return 0;
}
