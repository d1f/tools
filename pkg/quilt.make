ifneq ($(strip $(shell which $(PKG) 2>/dev/null)),)

PKG_HAS_NO_SOURCES = defined

else # has quilt


PKG_TYPE      = DEBIAN3
PKG_VERSION   = 0.63
PKG_VER_PATCH = -8
PKG_SITE_PATH = pool/main/q/$(PKG_BASE)
PLATFORM      = build

include $(DEFS)


define PATCH_CMD
  $(ZCAT) $(DL_DIR)/$(PKG_NAME)$(PKG_VER_PATCH).debian.tar* | \
  $(TAR) -C $(PKG_PATCHED_SRC_DIR) -xf - $(LOG)

  cd $(PKG_PATCHED_SRC_DIR) && \
  list="`sed 's/\#.*$$//' < debian/patches/series | grep -v '^$$'`"; \
  if test -n "$$list" ; then \
    for patch in `echo $$list` ; do \
        patch -p1 < debian/patches/$$patch >> $(LOG_FILE); \
    done ; \
  fi
endef


configure : $(call bstamp_f,install,ccache)

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) &&			\
     ./configure	$(LOG)		\
	--prefix=$(INST_DIR)		\
	--disable-nls			\
	--without-sendmail		\
	$(STD_VARS)
endef


    BUILD_CMD_TARGET = configure scripts compat mofiles
    BUILD_CMD =     $(BUILD_CMD_DEFAULT)

define INSTALL_CMD
  touch	$(PKG_BLD_DIR)/doc/README	\
	$(PKG_BLD_DIR)/doc/quilt.1	\
	$(PKG_BLD_DIR)/bin/guards.1	$(LOG)
  $(INSTALL_CMD_DEFAULT)
endef

    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)


endif # has quilt

include $(RULES)
