PKG_VERSION   = $(CROSSTOOL_NG_VERSION)
PKG_SITES     = http://crosstool-ng.org
PKG_SITE_PATH = download/crosstool-ng
PKG_BASE      = crosstool-ng
PKG_DIR       = crosstool-ng

include $(DEFS)


include $(TOOLS_PKG_DIR)/crosstool.mk


patch : $(call bstamp_f,install,sed)

define PATCH_CMD
  echo "$(I2)Removing linaro symlink ..."
  $(RM_R) $(PKG_PATCHED_SRC_DIR)/patches/glibc/linaro-* $(LOG)
endef


include $(RULES)
