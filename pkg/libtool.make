PKG_TYPE          = DEBIAN3
PKG_VERSION       = 2.4.2
PKG_VER_PATCH     = -1.11
PKG_SITE_PATH     = pool/main/libt/$(PKG_BASE)

PLATFORM          = build

include $(DEFS)


pre-configure : $(call bstamp_f,install,sed help2man)
    configure : $(call bstamp_f,install,gawk autotools-dev)

AC_VER = 2.69
include $(TOOLS_PKG_DIR)/autoconf.mk

AM_VER = 1.14
include $(TOOLS_PKG_DIR)/automake.mk


pre_conf_vars  = SED=$(SED)
pre_conf_vars += $(AC_VARS)
pre_conf_vars += $(AM_VARS)
pre_conf_vars += MAKEINFO=/bin/true

define PRE_CONFIGURE_CMD
  $(RM) $(PKG_BLD_DIR)/libltdl/stamp-mk			$(LOG)
  $(RM) $(PKG_BLD_DIR)/tests/package.m4			$(LOG)
  cd $(PKG_BLD_DIR) && $(pre_conf_vars) ./bootstrap --force $(LOG)
endef


define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) &&		\
     ./configure $(LOG)		\
	--prefix=$(INST_DIR)	\
	--disable-ltdl-install	\
	--with-gnu-ld		\
	$(STD_VARS)
endef


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
  INSTALL_CMD =   $(INSTALL_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)


include $(RULES)
