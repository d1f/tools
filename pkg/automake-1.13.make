AC_VER = 2.69

AM_VER_MAJOR = 1.13
AM_VER_MINOR = 4
AM_VER       = $(AM_VER_MAJOR)

PKG_TYPE      = ORIGIN
PKG_VER_MAJOR = $(AM_VER_MAJOR)
PKG_VERSION   = $(AM_VER_MAJOR).$(AM_VER_MINOR)
PKG_BASE      = automake
PKG_SITES     = $(GNU_ORG_SITES)
PKG_SITE_PATH = $(PKG_BASE)

include $(TOP_DIR)/tools/pkg/automake-common.mk
