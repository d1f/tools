include $(TOP_DIR)/tools/pkg/mtd-utils.mk

PKG_VERSION     = $(MTD_UTILS_VERSION)

ifeq ($(MTD_UTILS_VERSION),1.3.1)
  PKG_TYPE      = ORIGIN
  PKG_SITES     = http://repository.timesys.com
  PKG_SITE_PATH = buildsources/m/$(PKG)/$(PKG)-$(MTD_UTILS_VERSION)
  PKG_REMOTE_FILE_SUFFICES = .tar.gz
else
  PKG_TYPE      = DEBIAN1
  PKG_VER_PATCH = $(MTD_UTILS_VER_PATCH)
  PKG_SITE_PATH = pool/main/m/$(PKG_BASE)
endif

include $(DEFS)


patch : $(call bstamp_f,install,sed)

PKG_PATCH2_FILES = $(wildcard $(PKG_PKG_DIR)/*.patch)

define PATCH_CMD
  #cp -v $(TOOLS_PKG_DIR)/nandcmp/src/nandcmp.c $(PKG_PATCHED_SRC_DIR)/ $(LOG)
  $(SED) -i -e 's/rm -rf $$(BUILDDIR)//' $(PKG_PATCHED_SRC_DIR)/Makefile $(LOG)
endef


ifeq ($(PLATFORM),build)
 build : $(call bstamp_f,install,attr acl zlib lzo2)
#build : $(call bstamp_f,depend,linux)
#build : $(call bstamp_f,install,linux-headers)
else
 build : $(call tstamp_f,install,toolchain)
endif

ifneq ($(PLATFORM),build)
 ENV_VARS = $(STD_VARS)
else
 ENV_VARS = CPPFLAGS="-I./include $(STD_CPPFLAGS)"
 ENV_VARS += LDFLAGS="$(STD_LDFLAGS)"
endif
 MK_VARS = CC=$(CC) AR=$(AR) RANLIB=$(RANLIB)
 MK_VARS += BUILDDIR=$(PKG_BLD_DIR)
 MK_VARS += DESTDIR=$(INST_DIR) PREFIX= SBINDIR=bin
#MK_VARS += WITHOUT_XATTR=1
#MK_VARS += WITHOUT_LZO=1
#MK_VARS += Q=

ifeq ($(PLATFORM),build)
    targets = mkfs.jffs2 sumtool #jffs2dump
  ifeq ($(shell $(VERCMP) $(MTD_UTILS_VERSION) '>=' 1.5),true)
    targets += jffs2reader
  endif
    instdir = $(BBIN_DIR)
else
    targets = flash_erase nandwrite nanddump #nandtest #nandcmp
    instdir = $(RSBIN_DIR)
endif

    BUILD_CMD_TARGET = $(addprefix $(PKG_BLD_DIR)/,$(targets))
    BUILD_CMD =   $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =   $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD =   $(CLEAN_CMD_DEFAULT)

  INSTALL_ROOT_CMD = $(IW) $(INSTxFILES) \
	$(addprefix $(PKG_BLD_DIR)/,$(targets)) $(instdir) $(LOG)


include $(RULES)
