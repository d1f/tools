PKG_TYPE              = DEBIAN_NATIVE
PKG_VERSION           = 0.4
PKG_SITE_PATH         = pool/main/u/$(PKG_BASE)

PLATFORM = build

include $(DEFS)


PKG_PATCH_FILES += $(PKG_PKG_DIR)/*.patch


build : $(call bstamp_f,install,zlib)

STD_CFLAGS += $(STD_CPPFLAGS) -I.
MK_VARS = $(STD_VARS)

    BUILD_CMD_TARGET = build
    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)
  INSTALL_CMD = $(IW) $(INSTxFILES) $(PKG_BLD_DIR)/mkimage $(BBIN_DIR) $(LOG)

install : $(call bstamp_f,uninstall,u-boot)


include $(RULES)
