PKG_TYPE          = DEBIAN3
PKG_VERSION       = 2.41
PKG_VER_PATCH     = -3
PKG_SITE_PATH     = pool/main/libx/$(PKG_BASE)

PLATFORM          = build

include $(DEFS)


configure : $(call bstamp_f,install, expat)

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) &&	\
  perl Makefile.PL PREFIX=$(INST_DIR) \
	EXPATLIBPATH=$(BLIB_DIR) EXPATINCPATH=$(BINC_DIR) $(LOG)
endef


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)
  INSTALL_CMD =   $(INSTALL_CMD_DEFAULT)


include $(RULES)
