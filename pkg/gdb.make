#only for fetch, extract, patch

include $(TOP_DIR)/tools/pkg/gdb-version.mk

PKG_VERSION     = $(GDB_VERSION)
PKG_BASE        = gdb
PKG_TYPE        = DEBIAN3

ifeq ($(findstring DEBIAN,$(PKG_TYPE)),DEBIAN)
  PKG_SITE_PATH = pool/main/g/$(PKG_BASE)
  PKG_VER_PATCH = $(GDB_VER_PATCH)
else
  PKG_SITES     = $(GNU_ORG_SITES)
  PKG_SITE_PATH = gdb
endif

SEPARATE_SRC_BLD_DIRS=defined


include $(DEFS)


include $(RULES)
