PKG_TYPE      = ORIGIN
PKG_VERSION   = 3.9.1
PKG_SITES     = http://releases.llvm.org
PKG_SITE_PATH = $(PKG_VERSION)
PKG_BASE      = llvm
PKG_REMOTE_FILE_SUFFICES = .src.tar.xz
SEPARATE_SRC_BLD_DIRS = defined
PLATFORM      = build

include $(DEFS)


configure : $(call bstamp_f,install,cmake)
configure : $(call tstamp_f,install,pkg-config)

conf_opts  = -DCMAKE_INSTALL_PREFIX=$(INST_DIR)
conf_opts += -DCMAKE_BUILD_TYPE=Release
#conf_opts += -DBUILD_SHARED_LIBS=OFF
conf_opts += -DLLVM_ENABLE_ZLIB=OFF
conf_opts += -DLLVM_INCLUDE_EXAMPLES=OFF
conf_opts += -DLLVM_INCLUDE_TESTS=OFF
#conf_opts += -DLLVM_TARGETS_TO_BUILD=all
conf_opts += -DPKG_CONFIG_EXECUTABLE=$(INST_DIR)/bin/$(TARGET)-pkg-config

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) && \
     $(BBIN_DIR)/cmake $(PKG_SRC_DIR) $(conf_opts) $(LOG)
endef


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)
  INSTALL_CMD =   $(INSTALL_CMD_DEFAULT)


include $(RULES)
