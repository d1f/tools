ifndef	tools/pkg/pkg-config.mk
	tools/pkg/pkg-config.mk = included


define pkg_config_usage

include $(TOOLS_PKG_DIR)/pkg-config.mk

$(ENV_VARS) ./configure ...

endef

# Users can define the PKG_CONFIG environment variable to point at the
# right one, or if they cross-compile and have a correctly named pkg-config
# (eg. arm-linux-pkg-config) in their PATH that will be used in preference.


# .. lang=ru
# ���� ������� :manpage:`pkg-config(1)`
# .. lang=en
# Path to :manpage:`pkg-config(1)` utility
ifeq ($(PLATFORM),build)
  PKG_CONFIG = $(BBIN_DIR)/pkg-config
else
  PKG_CONFIG = $(BBIN_DIR)/$(TARGET)-pkg-config
endif

# .. lang=ru
# ���� ������ ������ .pc ������� :manpage:`pkg-config(1)`
# .. lang=en
# Paths where to find .pc files of :manpage:`pkg-config(1)` utility
PKG_CONFIG_PATH	= $(ILIB_DIR)/pkgconfig:$(ISHARE_DIR)/pkgconfig



ENV_VARS += PKG_CONFIG=$(PKG_CONFIG)
ENV_VARS += PKG_CONFIG_PATH=$(PKG_CONFIG_PATH)


endif # tools/pkg/pkg-config.mk
