PKG_TYPE      = ORIGIN
PKG_SITES     = $(SF_NET_SITES)
PKG_SITE_PATH = $(PKG)
#PKG_VERSION   = 1.1.0
#PKG_VERSION   = 1.0.0
#PKG_VERSION   = 0.9.8
 PKG_VERSION   = 0.9.9
PKG_REMOTE_FILE_SUFFICES = .tar.gz

include $(DEFS)


PKG_PATCH_FILES  = $(wildcard $(PKG_PKG_DIR)/$(PKG)_*.patch)
PKG_PATCH_FILES += $(wildcard $(PKG_PKG_DIR)/$(PKG)-$(PKG_VERSION)_*.patch)


configure : $(call tstamp_f,install,toolchain popt binutils-target)
configure : $(call tstamp_f,build,linux)

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) &&		\
     ./configure -C $(LOG)	\
	--prefix=$(INST_DIR)	\
	--build=$(BUILD)	\
	--host=$(TARGET)	\
	--enable-shared		\
	--disable-static	\
	--disable-gui		\
	--disable-account-check	\
	--with-gnu-ld		\
	--with-kernel=$(LK_DIR) \
	$(STD_VARS)
endef


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)


ifeq ($(shell $(VERCMP) $(PKG_VERSION) '>' 0.9.9),true)
  bins  =           opreport opimport ophelp opgprof           oparchive opannotate
else
  bins  = oprofiled opreport opimport ophelp opgprof opcontrol oparchive opannotate op-check-perfevents
endif
  #bins += opjitconv

rbins = $(filter oprofiled opreport ophelp opcontrol op-check-perfevents,$(bins))

INSTALL_CMD = $(INSTALL_CMD_DEFAULT)

define INSTALL_ROOT_CMD
  $(IW) $(INSTxFILES) $(addprefix $(IBIN_DIR)/,$(rbins)) $(RBIN_DIR) $(LOG)
  $(IW) $(INSTxFILES)  $(BLD_DIR)/linux/vmlinux $(ROOT_DIR)/boot     $(LOG)
  $(IW) $(INSTxFILES) $(ILIB_DIR)/oprofile/libopagent.so* $(RLIB_DIR)/oprofile $(LOG)

  $(STRIP) $(STRIP_SHLIB) $(RLIB_DIR)/oprofile/*                    $(LOG)
  $(STRIP) $(STRIP_EXEC)  $(addprefix $(RBIN_DIR)/,$(filter-out opcontrol,$(rbins))) $(LOG)
endef

define _run_
  opcontrol --init
  opcontrol --start-daemon --vmlinux=/boot/vmlinux
endef


include $(RULES)
