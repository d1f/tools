PKG_TYPE      = DEBIAN1
PKG_VERSION   = 0.0.16
PKG_VER_PATCH = -6.1
PKG_SITE_PATH = pool/main/e/$(PKG_BASE)

PLATFORM      = build

include $(DEFS)


AC_VER = 2.69
include $(TOOLS_PKG_DIR)/autoconf.mk
PRE_CONFIGURE_CMD = $(AUTOCONF_CMD)


configure : $(call bstamp_f,install,e2fsprogs)

STD_CFLAGS += -I$(BLD_DIR)/e2fsprogs/lib

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) &&		\
     ./configure -C $(LOG)	\
	--prefix=$(INST_DIR)	\
	$(STD_VARS)
endef


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
  INSTALL_CMD =   $(INSTALL_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)


include $(RULES)
