AC_VER        = 2.65

PKG_TYPE      = ORIGIN
PKG_BASE      = autoconf
PKG_SITES     = $(GNU_ORG_SITES)
PKG_SITE_PATH = $(PKG_BASE)

include $(TOP_DIR)/tools/pkg/autoconf-common.mk
