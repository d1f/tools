ifneq ($(strip $(shell which $(PKG) 2>/dev/null)),)

PKG_HAS_NO_SOURCES = defined

else # has no pwgen

PKG_TYPE      = DEBIAN3
PKG_VERSION   = 2.07
PKG_VER_PATCH = -1.1
PKG_SITE_PATH = pool/main/p/$(PKG_BASE)
PLATFORM      = build

include $(DEFS)


define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) && \
     ./configure -C	$(LOG)	\
	--prefix=$(INST_DIR)	\
	$(STD_VARS)
endef


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)
  INSTALL_CMD =   $(INSTALL_CMD_DEFAULT)


endif # has no pwgen

include $(RULES)
