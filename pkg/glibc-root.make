PKG_VERSION  = $(LIBC_VERSION)
PKG_HAS_NO_SOURCES = defined

include $(DEFS)


ifeq (2.16.0,$(LIBC_VERSION))
LIBC_VERSION := 2.16
endif


libfiles += ld-$(LIBC_VERSION).so
libfiles += $(notdir $(wildcard $(tc_SYSROOT)/lib/ld-linux.so.*))
libfiles += $(notdir $(wildcard $(tc_SYSROOT)/lib/ld.so.*))

libfiles += libdl-$(LIBC_VERSION).so
libfiles += libdl.so.2
libfiles += libdl.so

libfiles += libc-$(LIBC_VERSION).so
libfiles += libc.so.6
libfiles += libc.so

libfiles += libm-$(LIBC_VERSION).so
libfiles += libm.so.6
libfiles += libm.so

libfiles += libmemusage.so

libfiles += libanl-$(LIBC_VERSION).so
libfiles += libanl.so.1
libfiles += libanl.so

libfiles += libBrokenLocale-$(LIBC_VERSION).so
libfiles += libBrokenLocale.so.1
libfiles += libBrokenLocale.so

libfiles += libcrypt-$(LIBC_VERSION).so
libfiles += libcrypt.so.1
libfiles += libcrypt.so

libfiles += libnsl-$(LIBC_VERSION).so
libfiles += libnsl.so.1
libfiles += libnsl.so

libfiles += libnss_compat-$(LIBC_VERSION).so
libfiles += libnss_compat.so.2
libfiles += libnss_compat.so

libfiles += libnss_dns-$(LIBC_VERSION).so
libfiles += libnss_dns.so.2
libfiles += libnss_dns.so

libfiles += libnss_files-$(LIBC_VERSION).so
libfiles += libnss_files.so.2
libfiles += libnss_files.so

libfiles += libnss_hesiod-$(LIBC_VERSION).so
libfiles += libnss_hesiod.so.2
libfiles += libnss_hesiod.so

libfiles += libnss_nis-$(LIBC_VERSION).so
libfiles += libnss_nis.so.2
libfiles += libnss_nis.so

libfiles += libnss_nisplus-$(LIBC_VERSION).so
libfiles += libnss_nisplus.so.2
libfiles += libnss_nisplus.so

libfiles += libpcprofile.so

libfiles += libpthread-*.*.so
libfiles += libpthread.so.0
libfiles += libpthread.so

libfiles += libthread_db-1.0.so
libfiles += libthread_db.so.1
libfiles += libthread_db.so

libfiles += libresolv-$(LIBC_VERSION).so
libfiles += libresolv.so.2
libfiles += libresolv.so

libfiles += librt-$(LIBC_VERSION).so
libfiles += librt.so.1
libfiles += librt.so

libfiles += libSegFault.so

libfiles += libutil-$(LIBC_VERSION).so
libfiles += libutil.so.1
libfiles += libutil.so


# ldd is not built for 2.3.x by unknown reason
# binfiles = ldd

#sbinfiles = ldconfig



ifdef EXTERN_TOOLCHAIN


else # !EXTERN_TOOLCHAIN


install : $(call tstamp_f,install,glibc)

define INSTALL_ROOT_CMD
  $(IW) $(INST_FILES) $(addprefix $(tc_SYSROOT)/lib/,  $(libfiles))  $(RLIB_DIR) $(LOG)
# $(IW) $(INSTxFILES) $(addprefix $(tc_SYSROOT)/bin/,  $(binfiles))  $(RBIN_DIR) $(LOG)
# $(IW) $(INSTxFILES) $(addprefix $(tc_SYSROOT)/sbin/,$(sbinfiles)) $(RSBIN_DIR) $(LOG)
endef

endif # EXTERN_TOOLCHAIN


include $(RULES)
