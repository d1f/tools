PKG_TYPE          = DEBIAN3
PKG_VERSION       = 1.8.2
PKG_VER_PATCH     = -1
PKG_SITE_PATH     = pool/main/n/$(PKG_BASE)

PLATFORM          = build

include $(DEFS)


build : $(call bstamp_f,install,ccache)
#build : $(call bstamp_f,install,python2.6)

  BUILD_CMD =  cd $(PKG_BLD_DIR) && ./configure.py --bootstrap --verbose $(LOG)
INSTALL_CMD =  $(IW) $(INSTxFILES) $(PKG_BLD_DIR)/ninja $(IBIN_DIR) $(LOG)


include $(RULES)
