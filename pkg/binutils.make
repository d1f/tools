ifneq ($(filter %debian1,$(BINUTILS_VERSION)),)
  PKG_TYPE = DEBIAN1
  pkg_full_version = $(patsubst %debian1,%,$(BINUTILS_VERSION))
  FORCE_QUILT = defined # 2.22
else ifneq ($(filter %+deb7u2,$(BINUTILS_VERSION)),)
  PKG_TYPE = DEBIAN1
  pkg_full_version = $(BINUTILS_VERSION)
  FORCE_QUILT = defined # 2.22
else ifneq ($(filter %debian3,$(BINUTILS_VERSION)),)
  PKG_TYPE = DEBIAN3
  pkg_full_version = $(patsubst %debian3,%,$(BINUTILS_VERSION))
else
  PKG_TYPE = ORIGIN
endif

ifneq ($(filter DEBIAN%,$(PKG_TYPE)),)
  PKG_VERSION   = $(firstword $(subst -, ,$(pkg_full_version)))
  PKG_VER_PATCH = -$(lastword $(subst -, ,$(pkg_full_version)))
  PKG_SITE_PATH = pool/main/b/$(PKG_BASE)
else
  PKG_VERSION   = $(BINUTILS_VERSION)
  PKG_SITES     = $(GNU_ORG_SITES)
  PKG_SITE_PATH = binutils
endif

include $(DEFS)


ifdef EXTERN_TOOLCHAIN

PKG_HAS_NO_SOURCES = defined

else


ifdef FORCE_QUILT
  include $(TOOLS_PKG_DIR)/quilt.mk
endif

ifdef BINUTILS_PLATFORM_PATCHES
  PKG_PATCH_FILES += $(BINUTILS_PLATFORM_PATCHES)
else
 ifeq ($(filter DEBIAN%,$(PKG_TYPE)),)
  include $(TOOLS_PKG_DIR)/crosstool.mk
  PKG_PATCH_FILES  = $(CROSSTOOL_PATCH_DIR)/binutils/$(PKG_VERSION)/*.patch
 endif
endif # ndef BINUTILS_PLATFORM_PATCHES

patch : $(call bstamp_f,install,sed)

ifneq ($(filter 2.17%,$(PKG_VERSION)),)
  define PATCH_CMD
    $(SED) -i -e 's/SUBDIRS *= *doc *po/SUBDIRS=/g' $(PKG_PATCHED_SRC_DIR)/bfd/Makefile.am      $(LOG)
    $(SED) -i -e 's/SUBDIRS *= *doc *po/SUBDIRS=/g' $(PKG_PATCHED_SRC_DIR)/bfd/Makefile.in      $(LOG)

    $(SED) -i -e 's/SUBDIRS *= *doc *po/SUBDIRS=/g' $(PKG_PATCHED_SRC_DIR)/binutils/Makefile.am $(LOG)
    $(SED) -i -e 's/SUBDIRS *= *doc *po/SUBDIRS=/g' $(PKG_PATCHED_SRC_DIR)/binutils/Makefile.in $(LOG)

    $(SED) -i -e 's/gprof.info//g'                  $(PKG_PATCHED_SRC_DIR)/gprof/Makefile.am	$(LOG)

    $(SED) -i -e 's/SUBDIRS *= *doc *po/SUBDIRS=/g' $(PKG_PATCHED_SRC_DIR)/gas/Makefile.am      $(LOG)
    $(SED) -i -e 's/SUBDIRS *= *doc *po/SUBDIRS=/g' $(PKG_PATCHED_SRC_DIR)/gas/Makefile.in      $(LOG)

    $(SED) -i -e 's/SUBDIRS *= *po/SUBDIRS=/g'      $(PKG_PATCHED_SRC_DIR)/ld/Makefile.am       $(LOG)
    $(SED) -i -e 's/SUBDIRS *= *po/SUBDIRS=/g'      $(PKG_PATCHED_SRC_DIR)/ld/Makefile.in       $(LOG)
  endef
else
  define PATCH2_CMD
    $(SED) -i -e 's/install-info-recursive//g'      $(PKG_PATCHED_SRC_DIR)/binutils/Makefile.in $(LOG)
    $(SED) -i -e 's/install-info-recursive//g'      $(PKG_PATCHED_SRC_DIR)/bfd/Makefile.in      $(LOG)
    $(SED) -i -e 's/install-info-recursive//g'      $(PKG_PATCHED_SRC_DIR)/gas/Makefile.in      $(LOG)
    $(SED) -i -e 's/install-info-recursive//g'      $(PKG_PATCHED_SRC_DIR)/opcodes/Makefile.in  $(LOG)
    $(SED) -i -e 's/install-info-recursive//g'      $(PKG_PATCHED_SRC_DIR)/ld/Makefile.in       $(LOG)

    $(SED) -i -e 's/info-recursive//g'              $(PKG_PATCHED_SRC_DIR)/binutils/Makefile.in $(LOG)
    $(SED) -i -e 's/info-recursive//g'              $(PKG_PATCHED_SRC_DIR)/bfd/Makefile.in      $(LOG)
    $(SED) -i -e 's/info-recursive//g'              $(PKG_PATCHED_SRC_DIR)/gas/Makefile.in      $(LOG)
    $(SED) -i -e 's/info-recursive//g'              $(PKG_PATCHED_SRC_DIR)/opcodes/Makefile.in  $(LOG)
    $(SED) -i -e 's/info-recursive//g'              $(PKG_PATCHED_SRC_DIR)/ld/Makefile.in       $(LOG)
  endef
endif


configure : $(call bstamp_f,install,bison flex)


define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) &&			\
	CC=$(BUILD_CC)			\
	CFLAGS="$(STD_CFLAGS) $(TC_CFLAGS)"	\
	$(PKG_SRC_DIR)/configure $(LOG)	\
	--prefix=$(tc_PREFIX)		\
	--target=$(TARGET)		\
	--disable-nls			\
	--disable-multilib		\
	--with-gnu-ld			\
	--with-mmap			\
	--enable-install-libbfd		\
	--enable-install-libiberty	\
	--without-pic			\
	--disable-libada		\
	--disable-libssp		\
	--enable-stage1-languages=c	\
	--disable-objc-gc		\
	--without-docdir		\
	--without-pdfdir		\
	--disable-shared		\
	--enable-static			\
	--without-htmldir		\
	--without-libiconv-prefix	\
	--without-libintl-prefix	\
	--without-included-gettext	\
	$(BINUTILS_EXTRA_CONFIG)
endef


define BUILD_CMD
  $(RM) $(PKG_BLD_DIR)/binutils/arlex.c $(LOG)
  $(BUILD_CMD_DEFAULT)
endef

    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)


install : $(GLD_INSTALL_DEP_STAMP)

ifeq ($(PLATFORM),build)
  LIBIBERTY_CMD =
else
  define LIBIBERTY_CMD
    $(IW) $(MKDIR_P)	$(tc_BFD_PREFIX)/lib $(LOG)
    $(IW) mv -v	$(tc_PREFIX)/lib*/libiberty.a	\
			$(tc_BFD_PREFIX)/lib $(LOG)
  endef
endif


define INSTALL_CMD
  $(INSTALL_CMD_DEFAULT)
  $(LIBIBERTY_CMD)
  $(MANGLE_INSTALLED_GLD)
endef


endif # ! EXTERN_TOOLCHAIN


include $(RULES)
