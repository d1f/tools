AC_VER = 2.13

PKG_TYPE      = DEBIAN3
PKG_VER_MAJOR = $(AC_VER)
PKG_VERSION   = $(AC_VER)
PKG_VER_PATCH = -67
PKG_BASE      = autoconf$(PKG_VER_MAJOR)
PKG_SITE_PATH = pool/main/a/$(PKG_BASE)

PLATFORM      = build

include $(DEFS)


include $(TOOLS_PKG_DIR)/autoconf.mk

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) && ./configure $(LOG) \
	--prefix=$(AC_PREFIX)
endef

MK_VARS = MAKEINFO=/bin/true

    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)
  INSTALL_CMD =   $(INSTALL_CMD_DEFAULT)


include $(RULES)
