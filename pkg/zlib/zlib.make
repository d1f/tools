PKG_TYPE      = DEBIAN3
PKG_VERSION   = 1.2.8.dfsg
PKG_VER_PATCH = -4
PKG_SITE_PATH = pool/main/z/$(PKG_BASE)

include $(DEFS)


POST_EXTRACT_CMD = $(RM)			\
	$(PKG_ORIG_SRC_DIR)/Makefile		\
	$(PKG_ORIG_SRC_DIR)/zlib.pc		\
	$(PKG_ORIG_SRC_DIR)/zlibdefs.h		\
	$(PKG_ORIG_SRC_DIR)/zconf.temp.h	\
	$(PKG_ORIG_SRC_DIR)/ztest$$.c		\
	$(LOG)


PKG_PATCH_FILES = $(PKG_PKG_DIR)/*.patch


configure : $(TOOLCHAIN_INSTALL_STAMP)
# configure --help tells --exec_prefix= but really parses --eprefix
conf_opts = --prefix=$(INST_DIR)
conf_opts += --shared
MK_VARS = pkgconfigdir=$(ILIB_DIR)/pkgconfig

ifneq ($(PLATFORM),build)
  conf_opts += "--uname=Linux"
endif


# -Wl,-EL breaks shared lib test with extern Montavista toolchain
STD_CFLAGS := $(filter-out -Wl%$(GLD_ENDIAN_FLAG),$(STD_CFLAGS))
define CONFIGURE_CMD
  $(RELINK) $(PKG_BLD_DIR)/zconf.h $(LOG)
  cd $(PKG_BLD_DIR) &&		\
  $(STD_VARS) uname=Linux	\
  CFLAGS="$(STD_CFLAGS) -fPIC"	\
 LDFLAGS="$(STD_LDFLAGS)"	\
  ./configure $(conf_opts) $(LOG)
endef


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)


define INSTALL_CMD
  $(IW) $(MKDIR_P) $(ILIB_DIR)/pkgconfig	$(LOG)
  $(INSTALL_CMD_DEFAULT)
  $(IW) $(RM) -v $(ILIB_DIR)/libz.a		$(LOG)
endef

INSTALL_ROOT_CMD = $(IW) $(INSTxFILES) $(ILIB_DIR)/libz.so* $(RLIB_DIR) $(LOG)


include $(RULES)
