# mbedtls is former polarssl
PKG_TYPE         = ORIGIN
PKG_VERSION      = 2.4.0
PKG_ORIG_DIR     = $(TOOLS_SRC_DIR)/$(PKG)
PKG_FETCH_METHOD = DONE
PLATFORM         = build

include $(DEFS)


PKG_PATCH_FILES = $(PKG_PKG_DIR)/$(PKG)_*.patch


build : $(call tstamp_f,install,toolchain)

MK_VARS += DESTDIR=$(INST_DIR) PREFIX=mbedtls_ $(STD_VARS)

# -DLIB_INSTALL_DIR=lib/$(DEB_HOST_MULTIARCH)
# -DUSE_STATIC_MBEDTLS_LIBRARY=OFF
# -DUSE_SHARED_MBEDTLS_LIBRARY=ON

define CONFIGURE_CMD
  $(RELINK) $(PKG_BLD_DIR)/include/mbedtls/config.h $(LOG)
endef


    BUILD_CMD_TARGET = no_test
    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)
  INSTALL_CMD =   $(INSTALL_CMD_DEFAULT)


include $(RULES)
