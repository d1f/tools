PKG_VERSION   =        $(LIBC_VERSION)
PKG_FILE_BASE = uClibc-$(LIBC_VERSION)

NG := $(filter ng-%,$(LIBC_VERSION))
ifneq ($(NG),)
  PKG_SITES     = http://downloads.uclibc-ng.org
else
  PKG_SITES     = http://www.uclibc.org
endif

include $(DEFS)

ifneq ($(NG),)
  PKG_SITE_PATH = releases/$(patsubst ng-%,%,$(LIBC_VERSION))
else
 ifeq ($(shell $(VERCMP) $(LIBC_VERSION) '<=' 0.9.29),true)
  PKG_SITE_PATH = downloads/old-releases
 else
  PKG_SITE_PATH = downloads
 endif
endif


include $(TOOLS_PKG_DIR)/crosstool.mk
ifneq ($(NG),)
  PKG_PATCH_FILES  = $(CROSSTOOL_PATCH_DIR)/uClibc-ng/$(patsubst ng-%,%,$(LIBC_VERSION))/*.patch
else
  PKG_PATCH_FILES  = $(CROSSTOOL_PATCH_DIR)/uClibc/$(LIBC_VERSION)/*.patch
  PKG_PATCH_FILES +=       $(TOOLS_PKG_DIR)/uclibc/patch/$(LIBC_VERSION)/*.patch
endif


ifeq ($(shell $(VERCMP) $(LIBC_VERSION) '>=' 0.9.29),true)
  utils_Makefile = Makefile.in
else
  utils_Makefile = Makefile
endif

       pre-configure : $(call bstamp_f,install,sed)
define PRE_CONFIGURE_CMD
  $(RELINK)                $(PKG_BLD_DIR)/utils/$(utils_Makefile) $(LOG)
  $(SED) -i -e "s,usr/,,g" $(PKG_BLD_DIR)/utils/$(utils_Makefile) $(LOG)
endef


configure : $(call tstamp_f,install,gcc-nolibc linux-headers)
uninstall : $(call tstamp_f,uninstall,uclibc-root)


include $(TOOLS_PKG_DIR)/uclibc-vars.mk


ifeq ($(shell $(VERCMP) $(LIBC_VERSION) '<=' 0.9.28),true)
  MCONF_MK_VARS +=      HOSTNCURSES='-I$(BINC_DIR) -DCURSES_LOC="<ncurses.h>" -DLOCALE'
  MCONF_MK_VARS +=   NATIVE_LDFLAGS='-L$(BLIB_DIR) -Wl,-rpath=$(BLIB_DIR) -lncurses -ltinfo'
  MCONF_MK_VARS +=             LIBS="-L$(BLIB_DIR) -Wl,-rpath=$(BLIB_DIR) -lncurses -ltinfo"
else
  # linux && new uclibc
 ifneq ($(NG),)
  MCONF_MK_VARS += HOST_EXTRACFLAGS='-I$(BINC_DIR) -DCURSES_LOC="<ncurses.h>" -DLOCALE -L$(BLIB_DIR) -Wl,-rpath=$(BLIB_DIR) -lncurses -ltinfo'
 else
  MCONF_MK_VARS += HOST_EXTRACFLAGS='-I$(BINC_DIR) -DCURSES_LOC="<ncurses.h>" -DLOCALE'
 endif

  MCONF_MK_VARS +=   HOST_LOADLIBES='-L$(BLIB_DIR) -Wl,-rpath=$(BLIB_DIR) -lncurses -ltinfo'
  MCONF_MK_VARS += CC=$(BUILD_CC)
endif

MENUCONFIG_CMD = $(MENUCONFIG_CMD_DEFAULT)


ifeq ($(shell $(VERCMP) $(LIBC_VERSION) '>=' 0.9.30),true)
  define configure_locale
    $(RM)	$(PKG_BLD_DIR)/extra/locale/locales.txt		\
		$(PKG_BLD_DIR)/extra/locale/codesets.txt		$(LOG)

    cp		$(TOOLS_PKG_DIR)/uclibc/locales.txt		\
		$(TOOLS_PKG_DIR)/uclibc/codesets.txt		\
					$(PKG_BLD_DIR)/extra/locale/	$(LOG)

    touch	$(PKG_BLD_DIR)/extra/locale/locales.txt		\
		$(PKG_BLD_DIR)/extra/locale/codesets.txt		$(LOG)
  endef
else
  define configure_locale
    cp		$(TOOLS_PKG_DIR)/uclibc/uClibc-locale-030818.tgz \
		  $(PKG_BLD_DIR)/extra/locale/				$(LOG)
  endef
endif

define CONFIGURE_CMD
  $(configure_locale)
  $(RELINK) $(PKG_BLD_DIR)/extra/scripts/format.lds			$(LOG)
  cp -vf $(PKG_MENUCONFIG_FILE) $(PKG_BLD_DIR)/.config			$(LOG)
  yes '' 2>/dev/null | $(DOMAKE) -C $(PKG_BLD_DIR) $(MK_VARS) oldconfig	$(LOG)
endef


MK_JOBS=1 # multijob build produce wrong binaries

# pregen: include/bits/sysnum.h headers
DEPEND_CMD_TARGET = pregen
DEPEND_CMD = $(DEPEND_CMD_DEFAULT)


ifeq ($(shell $(VERCMP) $(LIBC_VERSION) '>=' 0.9.30),true)
  define gen_locale
    echo "$(I1)Generate locale ..."
    $(DOMAKE) -C $(PKG_BLD_SUB_DIR)/extra/locale $(MK_VARS) all	$(LOG)
  endef
endif

ifeq ($(shell $(VERCMP) $(LIBC_VERSION) '<=' 0.9.28),true)
	BUILD_MK_VARS    = RUNTIME_PREFIX=/
	BUILD_CMD_TARGET = shared utils
else
        BUILD_CMD_TARGET = libs utils
endif

define  BUILD_CMD
  $(gen_locale)

  echo "$(I1)Building $(BUILD_CMD_TARGET) ..."
  $(BUILD_CMD_DEFAULT)
endef

    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)


ifeq ($(shell $(VERCMP) $(LIBC_VERSION) '>=' 0.9.30),true)
  # shamelessly stolen from OpenWrt backfire
  inst_ln_libc_so = $(IW) $(LN_S) libc.so.0 $(tc_SYSROOT)/lib/libc.so $(LOG)
endif

       INSTALL_CMD_TARGET = install_dev install_runtime
define INSTALL_CMD
  $(IW) $(MKDIR_P) $(tc_SYSROOT)/lib                              $(LOG)
  $(IW) touch      $(tc_SYSROOT)/lib/ld-uClibc-$(LIBC_VERSION).so $(LOG)

  $(INSTALL_CMD_DEFAULT)

  $(inst_ln_libc_so)
endef


include $(RULES)
