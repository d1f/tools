PKG_TYPE         = ORIGIN
PKG_VERSION      = 1.29
PKG_ORIG_DIR     = $(TOOLS_SRC_DIR)/$(PKG)
PLATFORM         = build
PKG_FETCH_METHOD = DONE

include $(DEFS)


define CONFIGURE_CMD
  cd  $(PKG_BLD_DIR) && \
      ./configure	-C $(LOG)	\
	--prefix=$(INST_DIR)		\
	--disable-acl			\
	--disable-nls			\
	--enable-backup-scripts		\
	--without-posix-acls		\
	--without-selinux		\
	--with-rmt=$(shell which true)	\
	--without-xattrs		\
	--without-libiconv-prefix	\
	--without-libintl-prefix	\
	$(STD_VARS)

	$(RM) $(PKG_BLD_DIR)/doc/header.texi
endef


    MK_VARS  = ACLOCAL=true AUTOMAKE=true AUTOCONF=true AUTOHEADER=true
    MK_VARS += MAKEINFO=true YLWRAP=true
    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)
  INSTALL_CMD =   $(INSTALL_CMD_DEFAULT)


include $(RULES)
