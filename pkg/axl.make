PKG_TYPE      = ORIGIN
PKG_VERSION   = 6.0.0
PKG_SITES     = http://tibbo.com
PKG_SITE_PATH = downloads/archive/$(PKG)
PKG_BASE      = $(PKG)-src
SEPARATE_SRC_BLD_DIRS = defined
PLATFORM      = build

include $(DEFS)


configure : $(call bstamp_f,install,cmake ragel)

conf_opts  = -DCMAKE_INSTALL_PREFIX=$(INST_DIR)
conf_opts += -DRAGEL_EXE=$(INST_DIR)/bin/ragel
conf_opts += -DCMAKE_BUILD_TYPE=Release
#conf_opts += -DBUILD_AXL_EXE=ON
conf_opts += -DBUILD_AXL_GUI=ON
conf_opts += -DBUILD_AXL_INI=ON
conf_opts += -DBUILD_AXL_IO=ON


CMAKE := $(or $(wildcard $(BBIN_DIR)/cmake),cmake)

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) && \
     $(CMAKE) $(PKG_SRC_DIR) $(conf_opts) $(LOG)
endef


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)


       INSTALL_CMD_TARGET = preinstall
define INSTALL_CMD
  $(INSTALL_CMD_DEFAULT)
  $(IW) $(INST_FILES) $(PKG_BLD_DIR)/lib/Release/*.a $(ILIB_DIR) $(LOG)
  $(IW) $(INST_FILES) $(PKG_SRC_DIR)/include/*.h     $(IINC_DIR) $(LOG)
endef


include $(RULES)
