#include <stdlib.h>
#include <stdio.h>
#include <limits.h>
#include <error.h>
#include <errno.h>


int main(int ac, char *av[])
{
    if (ac != 3)
    {
	printf("Usage: %s maxlen numlines\n", av[0]);
	return EXIT_FAILURE;
    }

    int maxlen = atoi(av[1]);
    if (maxlen < 1)
	maxlen = 1;

    int numlines = atoi(av[2]);
    if (numlines < 1)
	numlines = 1;

    for (size_t line = 0; line < (size_t)numlines; ++line)
    {
	size_t len = random() % maxlen + 1;
	char  *str = malloc(len+1);
	if (str == NULL)
	    error(EXIT_FAILURE, errno, "Can't allocate %zu bytes", len);

	for (size_t i = 0; i < len; ++i)
	{
	    char rchar = 0;
	    while (rchar < 32 /*rchar == '\n' || rchar == 0*/)
		rchar = random() % CHAR_MAX;

	    str[i] = rchar;
	}
	str[len] = 0;

	printf("%s\n", str);
	free(str);
    }

    return EXIT_SUCCESS;
}
