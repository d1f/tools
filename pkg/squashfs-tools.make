ifneq ($(strip $(shell which mksquashfs 2>/dev/null)),)

PKG_HAS_NO_SOURCES = defined

else # has no squashfs-tools

PKG_TYPE      = DEBIAN3
PKG_VERSION   = 4.3
PKG_VER_PATCH = -3
PKG_SITE_PATH = pool/main/s/$(PKG_BASE)
PKG_SUB_DIR   = squashfs-tools

PLATFORM      = build

include $(DEFS)


#PKG_PATCH2_FILES += $(PKG_PKG_DIR)/$(PKG)_*.patch


build : $(call bstamp_f,install,zlib)
# deps: libattr1-dev, liblzma-dev, liblzo2-dev, zlib1g-dev

STD_CFLAGS += -I. -D_FILE_OFFSET_BITS=64 -D_LARGEFILE_SOURCE -D_GNU_SOURCE
MK_VARS = $(STD_VARS)
# LZO_SUPPORT=1 XZ_SUPPORT=1
targets = mksquashfs unsquashfs

    BUILD_CMD =   $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =   $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD =   $(CLEAN_CMD_DEFAULT)
  INSTALL_CMD =   $(IW) $(INSTxFILES) \
  			$(addprefix $(PKG_BLD_SUB_DIR)/,$(targets)) $(BBIN_DIR) $(LOG)


endif # has no squashfs-tools

include $(RULES)
