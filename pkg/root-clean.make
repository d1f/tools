PKG_HAS_NO_SOURCES = defined

# special case - [un]install itself
IWTYPE = no
include $(DEFS)


UNINSTALL_CMD=

ifdef NO_ROOT_CLEAN
  define INSTALL_CMD
    echo "$(I1)Skipped, NO_ROOT_CLEAN defined"
  endef
else
  define INSTALL_CMD
    echo "$(I1)Deleting include,info,man,doc ..."
    $(RM_R) $(ROOT_DIR)/usr/include	$(LOG)
    $(RM_R) $(ROOT_DIR)/usr/info	$(LOG)
    $(RM_R) $(ROOT_DIR)/usr/man		$(LOG)
    $(RM_R) $(ROOT_DIR)/usr/doc		$(LOG)

    $(RM_R) $(ROOT_DIR)/usr/share/info	$(LOG)
    $(RM_R) $(ROOT_DIR)/usr/share/man	$(LOG)
    $(RM_R) $(ROOT_DIR)/usr/share/doc	$(LOG)

    $(RM_R) $(ROOT_DIR)/share/info	$(LOG)
    $(RM_R) $(ROOT_DIR)/share/man	$(LOG)
    $(RM_R) $(ROOT_DIR)/share/doc	$(LOG)

    $(RM_R) $(ROOT_DIR)/include		$(LOG)
    $(RM_R) $(ROOT_DIR)/info		$(LOG)
    $(RM_R) $(ROOT_DIR)/man		$(LOG)
    $(RM_R) $(ROOT_DIR)/doc		$(LOG)

    echo "$(I1)Deleting static libs ..."
    find $(ROOT_DIR) -name lib\*.a  -exec $(RM) '{}' ';'		$(LOG)
    find $(ROOT_DIR) -name    \*.la -exec $(RM) '{}' ';'		$(LOG)

    echo "$(I1)Deleting pkgconfig's ..."
    find $(ROOT_DIR) -name    \*.pc -exec $(RM) '{}' ';'		$(LOG)
    $(RM_R) $(RLIB_DIR)/pkgconfig					$(LOG)

    echo "$(I1) CVS* clean up ..."
    find $(ROOT_DIR) -depth -type d -name CVS\*  -exec $(RM_R) '{}' ';'	$(LOG)

    echo "$(I1).svn  clean up ..."
    find $(ROOT_DIR) -depth -type d -name .svn   -exec $(RM_R) '{}' ';'	$(LOG)

    echo "$(I1).git* clean up ..."
    find $(ROOT_DIR) -depth         -name .git\* -exec $(RM_R) '{}' ';'	$(LOG)

    echo "$(I1).dir  clean up ..."
    find $(ROOT_DIR) -depth         -name .dir   -exec $(RM_R) '{}' ';'	$(LOG)
  endef
endif #ndef NO_ROOT_CLEAN


include $(RULES)
