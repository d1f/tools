PKG_VERSION   = $(CROSSTOOL_NG_153_VERSION)
PKG_SITES     = http://crosstool-ng.org
PKG_SITE_PATH = download/crosstool-ng
PKG_BASE      = crosstool-ng
PKG_DIR       = crosstool-ng-1.5.3

include $(DEFS)


include $(TOOLS_PKG_DIR)/crosstool.mk


patch : $(call bstamp_f,install,sed)

# glibc/ports-2.9/*.patch has non-common dir name inside, force the right one
patch_dir   = $(PKG_PATCHED_SRC_DIR)/patches/glibc/ports-2.9
patch_files = $(patch_dir)/*.patch

define PATCH_CMD
  echo "$(I2)Fixing glibc/ports-2.9/*.patch dir names ..."
  chmod u+w $(patch_dir)
  $(RELINK) $(patch_files)
  $(SED) -i -e 's,glibc-2.9/ports/,glibc-2.9/glibc-ports-2.9/,g' \
			$(patch_files)
endef


include $(RULES)
