ifneq ($(strip $(shell which $(PKG) 2>/dev/null)),)

PKG_HAS_NO_SOURCES = defined

else # has no fakeroot

PKG_TYPE          = DEBIAN3
PKG_VERSION       = 1.21
PKG_VER_PATCH     = -3
PKG_SITE_PATH     = pool/main/f/$(PKG_BASE)
PLATFORM          = build

include $(DEFS)


AC_VER = 2.69
AM_VER = 1.14
include $(TOOLS_PKG_DIR)/autotools.mk
PRE_CONFIGURE_CMD = $(AUTOTOOLS_CMD)


define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) &&		\
     ./configure	$(LOG)	\
	--prefix=$(INST_DIR)	\
	--disable-static	\
	--with-gnu-ld		\
	$(STD_VARS)
endef


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
  INSTALL_CMD =   $(INSTALL_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)


endif # has no fakeroot

include $(RULES)
