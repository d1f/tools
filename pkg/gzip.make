ifneq ($(strip $(shell which gunzip 2>/dev/null)),)

PKG_HAS_NO_SOURCES = defined

else # has no gunzip

PKG_TYPE         = ORIGIN
PKG_VERSION      = 1.8
PKG_ORIG_DIR     = $(TOOLS_SRC_DIR)/$(PKG)
PLATFORM         = build
PKG_FETCH_METHOD = DONE

include $(DEFS)


PKG_PATCH_FILES = $(PKG_PKG_DIR)/$(PKG)_*.patch


define CONFIGURE_CMD
  cd  $(PKG_BLD_DIR) && \
      ./configure -C $(LOG)	\
	--prefix=$(INST_DIR)	\
	--without-libpth-prefix	\
	$(STD_VARS)
  touch $(PKG_BLD_DIR)/doc/gzip.info-t
endef

# --disable-threads
# --with-gnu-ld
# --enable-threads=posix


MK_VARS  = ACLOCAL=true AUTOMAKE=true AUTOCONF=true AUTOHEADER=true
MK_VARS += MAKEINFO=true
    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)
  INSTALL_CMD =   $(INSTALL_CMD_DEFAULT)


endif # has no gunzip

include $(RULES)
