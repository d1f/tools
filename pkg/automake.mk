ifndef	tools/pkg/automake.mk
	tools/pkg/automake.mk = included


define __automake_usage__

AM_VER = 1.11
include $(TOOLS_PKG_DIR)/automake.mk

endef

$(if $(AM_VER),,$(error AM_VER undefined))


# output vars:

AM_PREFIX = $(BINST_DIR)/automake-$(AM_VER)
AM_PATH   = $(AM_PREFIX)/bin
PATH_AM   = $(AM_PATH):$(PATH)
AUTOMAKE  = $(AM_PATH)/automake-$(AM_VER)
ACLOCAL   = $(AM_PATH)/aclocal-$(AM_VER)

AM_VARS   = AUTOMAKE=$(AUTOMAKE) ACLOCAL=$(ACLOCAL)

AUTOMAKE_FLAGS = --foreign --add-missing --force-missing --copy #--verbose
 ACLOCAL_FLAGS = --force #--verbose
 ACLOCAL_FLAGS += -I $(BINST_DIR)/share/aclocal

ifeq ($(filter automake%,$(PKG)),)
pre-configure : $(call bstamp_f,install,automake-$(AM_VER))
else
pre-configure : $(call bstamp_f,install,sed m4 gawk)
endif

export PATH := $(PATH_AM)


endif # tools/pkg/automake.mk
