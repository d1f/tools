PKG_VERSION   = $(LIBC_VERSION)
PKG_DIR       = uclibc
PKG_HAS_NO_SOURCES = defined

include $(DEFS)


configure : $(call tstamp_f,install,linux-headers)
configure : $(call tstamp_f,link,uclibc)

include $(TOOLS_PKG_DIR)/uclibc-vars.mk

define CONFIGURE_CMD
  cp $(PKG_MENUCONFIG_FILE) $(PKG_BLD_DIR)/.config                      $(LOG)
  yes '' 2>/dev/null | $(DOMAKE) -C $(PKG_BLD_DIR) $(MK_VARS) oldconfig $(LOG)
endef


ifeq ($(shell $(VERCMP) $(LIBC_VERSION) '<=' 0.9.28),true)
  INSTALL_HEADERS_TARGET = install_dev
else
  INSTALL_HEADERS_TARGET = install_headers
endif

#MK_VARS += HAVE_DOT_CONFIG=y # uClibc-ng
    BUILD_CMD = $(DOMAKE) -C $(PKG_BLD_DIR) $(MK_VARS)           headers         $(LOG)
  INSTALL_CMD = $(IWMAKE) -C $(PKG_BLD_DIR) $(MK_VARS) $(INSTALL_HEADERS_TARGET) $(LOG)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)


include $(RULES)
