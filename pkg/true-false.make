PKG_HAS_NO_SOURCES = defined
PLATFORM = build


define INSTALL_CMD
  $(IWSH) "$(call lnsdir_f,$(BBIN_DIR),/bin/true,true)"		$(LOG)
  $(IWSH) "$(call lnsdir_f,$(BBIN_DIR),/bin/false,false)"	$(LOG)
endef


include $(RULES)
