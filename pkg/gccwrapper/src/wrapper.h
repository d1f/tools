#include <stdlib.h> /* EXIT_FAILURE, getenv() */
#include <unistd.h> /* exec* */
#include <string.h>   // strlen, memcpy
#include <errno.h>
#include <error.h>
#include <limits.h>   // PATH_MAX, NAME_MAX
#include <sys/stat.h> // stat

#ifdef __GPP_WRAPPER__
# define PARAMS_H "gpp_params.h"
# define LOGFILE "log.gppwrapper"
#endif

#ifdef __GCC_WRAPPER__
# define PARAMS_H "gcc_params.h"
# define LOGFILE "log.gccwrapper"
#endif

#ifdef __GXX_WRAPPER__
# define PARAMS_H "gxx_params.h"
# define LOGFILE "log.gxxwrapper"
#endif

#ifdef __GLD_WRAPPER__
# define PARAMS_H "gld_params.h"
# define LOGFILE "log.gldwrapper"
#endif

#ifndef __GENDEPS__
# include PARAMS_H
#endif

static const size_t params_len = sizeof(params) / sizeof(params[0]);


#ifdef DEBUG
#include <stdio.h>

static void logv(char **argv)
{
    FILE* f = fopen(LOGFILE, "a");
    if (f==NULL)
	error(0, errno, "can't open " LOGFILE);

    for ( ; *argv != NULL; ++argv)
	fprintf(f, "%s ", *argv);

    fprintf(f, "\n");

    fclose(f);
}
#else
static void logv(char **argv) { (void)argv; }
#endif


int main(int argc, char* argv[])
{
    static char callname[PATH_MAX + NAME_MAX + 2];
    size_t argv0_len = strlen(argv[0]);
    memcpy(callname, argv[0], argv0_len);
    callname[argv0_len++] = '_';
    callname[argv0_len++] = '_';
    callname[argv0_len++] =   0;

    size_t parv_offs = 0;
    char *parv0 = NULL;

    const char *ccache = getenv("CCACHE");
    if (ccache != NULL)
    {
	static char ccache_path[PATH_MAX + NAME_MAX + 2];
	static char ccache_bin[] = "ccache";
	static const size_t ccache_bin_len = sizeof(ccache_bin) - 1;

	char *path = getenv("PATH");
	if (path == NULL)
	{
	    static char default_path[] = "/usr/local/bin:/usr/bin:/bin";
	    path      = default_path;
	}

	char *curr = path;
	char *token = curr;

	while ((token = strsep(&curr, ":")) != NULL)
	{
	    size_t token_len = strlen(token);

	    // strcpy(ccache_path, token):
	    memcpy(ccache_path, token, token_len);

	    // strcat(ccache_path, "/"):
	    ccache_path[token_len++] = '/';

	    // strcat(ccache_path, ccache_bin):
	    memcpy(ccache_path + token_len, ccache_bin, ccache_bin_len);
	    ccache_path[token_len + ccache_bin_len] = 0;

	    struct stat stbuf;
	    int rc = stat(ccache_path, &stbuf);
	    if (rc == 0)
	    {
		parv0 = ccache_path;
		break;
	    }
	}

	if (parv0 != NULL)
	    ++parv_offs;
    }

    char*   parv[argc   + params_len + 2];
    memcpy( parv        + parv_offs, argv  , argc       * sizeof(char*) );
    memcpy( parv + argc + parv_offs, params, params_len * sizeof(char*) );

    parv[        0] = parv0;
    parv[parv_offs] = callname;
    parv[argc + parv_offs + params_len] = NULL;

    logv(parv);

    execvp(parv[0], parv);
    error(EXIT_FAILURE, errno, "execve %s", parv[0]);
    return EXIT_FAILURE; /* to make compiler happy */
}
