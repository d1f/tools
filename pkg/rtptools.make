PKG_VERSION = 1.22
PKG_URL     = http://www.cs.columbia.edu/irt/software/rtptools/download/rtptools-$(PKG_VERSION).tar.gz
PLATFORM    = build

include $(DEFS)


define _PRE_CONFIGURE_CMD_
  aclocal
  automake --add-missing --force-missing
  autoconf
endef

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) && \
     ./configure -C \
	--prefix=$(INST_DIR) \
	$(STD_VARS) $(LOG)
endef


  BUILD_CMD =   $(BUILD_CMD_DEFAULT)
INSTALL_CMD = $(INSTALL_CMD_DEFAULT)


include $(RULES)
