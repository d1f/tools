ifneq ($(strip $(shell which ctags 2>/dev/null)),)

PKG_HAS_NO_SOURCES = defined

else # has no $(PKG)

PKG_TYPE      = DEBIAN3
PKG_VERSION   = 5.9~svn20110310
PKG_VER_PATCH = -11
PKG_SITE_PATH = pool/main/e/$(PKG_BASE)
PLATFORM      = build

include $(DEFS)


configure : $(call bstamp_f,install,ccache gawk)

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) &&			\
     ./configure $(LOG)			\
	--prefix=$(INST_DIR)		\
	--disable-etags			\
	--disable-external-sort		\
	--enable-shell-globbing=no	\
	--enable-tmpdir=/tmp		\
	--disable-largefile		\
	--without-readlib		\
	$(STD_VARS)
endef


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)
  INSTALL_CMD =   $(INSTALL_CMD_DEFAULT)


endif # has no $(PKG)

include $(RULES)
