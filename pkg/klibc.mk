ifndef	tools/pkg/klibc.mk
	tools/pkg/klibc.mk = included


INST_KLIBC_DIR = $(INST_DIR)/klibc

prj_klibc_mk := $(wildcard $(PRJ_DIR)/pkg/klibc.mk)

ifneq ($(strip $(prj_klibc_mk)),)
       include $(prj_klibc_mk)
endif

ifndef XDEP
ifeq ($(filter fetch% check %extract %patch link none all %clean un% %list update save%,$(MAKECMDGOALS)),)
KLIBC_VERSION ?= $(error no KLIBC_VERSION provided)
endif
endif


endif # tools/pkg/klibc.mk
