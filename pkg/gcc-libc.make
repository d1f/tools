PKG_VERSION  = $(GCC_VERSION)
PKG_HAS_NO_SOURCES    = defined
SEPARATE_SRC_BLD_DIRS = defined

include $(DEFS)


ifdef EXTERN_TOOLCHAIN

PKG_HAS_NO_SOURCES = defined

else

configure : $(call tstamp_f,install,binutils gcc-nolibc $(LIBC_TYPE)libc)

gcc-ge-4.3 := $(shell $(VERCMP) $(GCC_VERSION) '>=' 4.3)
ifeq ($(gcc-ge-4.3),true)
  configure : $(call bstamp_f,install,gmp mpfr)
  GCC_EXTRA_CONFIG +=  --with-gmp=$(BINST_DIR)
  GCC_EXTRA_CONFIG += --with-mpfr=$(BINST_DIR)
endif

PKG_PATCHED_SRC_DIR = $(PATCHED_SRC_DIR)/gcc_$(GCC_VERSION)

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR)			&&	\
  CC_FOR_BUILD=$(BUILD_CC)			\
  CFLAGS="$(STD_CFLAGS) $(TC_CFLAGS) -Wa,-W"	\
  LANGUAGES="c c++"				\
  $(PKG_SRC_DIR)/configure		$(LOG)	\
	--prefix=$(tc_PREFIX)			\
	--with-local-prefix=$(tc_SYSROOT)	\
	--target=$(TARGET)			\
	--with-headers=$(tc_HEADERDIR)		\
	--disable-nls				\
	--disable-multilib			\
	--enable-languages=c,c++		\
	--enable-c99				\
	--enable-long-long			\
	--enable-threads=posix			\
	--enable-symvers=gnu			\
	--disable-__cxa_atexit			\
	--disable-libssp			\
	--disable-libgomp			\
	--enable-shared				\
	--with-gnu-as				\
	--with-gnu-ld				\
	--with-as=$(AS)				\
	--with-ld=$(LD)				\
	$(GCC_EXTRA_CONFIG)
endef


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)


install : $(GCC_INSTALL_DEP_STAMP)

define INSTALL_CMD
  $(IW) $(RM) $(PKG_BLD_DIR)/gcc/include-fixed/asm $(LOG)
  $(INSTALL_CMD_DEFAULT)
  $(IW) $(RM)  $(tc_PREFIX)/bin/$(TARGET)-c++ $(LOG)
  $(IW) $(RM) $(tc_SYSROOT)/bin/c++ $(LOG)
  $(MANGLE_INSTALLED_GCC)
  $(MANGLE_INSTALLED_GPP)
  $(MANGLE_INSTALLED_GXX)
endef

UNINSTALL2_CMD = $(RM) $(call tstamp_f,install,gcc-nolibc)


endif # ! EXTERN_TOOLCHAIN


include $(RULES)
