ifneq ($(strip $(shell which $(PKG) 2>/dev/null))$(PLATFORM),build)

PKG_HAS_NO_SOURCES = defined

else # has no flex for build platform

PKG_TYPE      = DEBIAN1
PKG_VERSION   = 2.6.1
PKG_VER_PATCH = -1.1
PKG_SITE_PATH = pool/main/f/$(PKG_BASE)

include $(DEFS)


AC_VER = 2.69
AM_VER = 1.14
include $(TOOLS_PKG_DIR)/autotools.mk
define PRE_CONFIGURE_CMD
  $(RELINK) $(PKG_BLD_DIR)/src/stage1scan.c $(LOG)
  $(AUTOTOOLS_CMD)
endef


configure : $(call bstamp_f,install,m4 bison help2man)
configure : $(TOOLCHAIN_INSTALL_STAMP)

ifneq ($(PLATFORM),build)
     conf_env  = ac_cv_func_malloc_0_nonnull=yes
     conf_env += ac_cv_func_realloc_0_nonnull=yes
endif

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) &&			\
     $(conf_env)			\
     ./configure -C $(LOG)		\
	--prefix=$(INST_DIR)		\
	--build=$(BUILD)		\
	--host=$(TARGET)		\
	--enable-shared			\
	--disable-static		\
	--disable-nls			\
	--with-gnu-ld			\
	--with-gnu-as			\
	--without-libiconv-prefix	\
	--without-libintl-prefix	\
	$(STD_VARS)
endef


ifeq ($(PLATFORM),build)

MK_VARS += MAKEINFO=true

  BUILD_CMD =     $(BUILD_CMD_DEFAULT)
INSTALL_CMD =   $(INSTALL_CMD_DEFAULT)

else

define INSTALL_CMD
  $(IW) $(INST_FILES) $(PKG_SRC_DIR)/src/FlexLexer.h $(IINC_DIR) $(LOG)
# $(IW) $(INST_FILES) $(PKG_BLD_DIR)/libfl.a     $(ILIB_DIR) $(LOG)
endef

endif


    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)


endif # has no flex for build platform

include $(RULES)
