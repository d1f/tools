PKG_VERSION   = $(LIBC_VERSION)
PKG_SITES     = $(GNU_ORG_SITES)
PKG_SITE_PATH = glibc
SEPARATE_SRC_BLD_DIRS = defined

include $(DEFS)


include $(TOOLS_PKG_DIR)/glibc/addons.mk

ifneq ($(glibc_have_ext_addon_linuxthreads),)

EXTRACT_STRIP_NUM = 0

include $(TOOLS_PKG_DIR)/crosstool.mk
PKG_PATCH_FILES  = $(CROSSTOOL_PATCH_DIR)/glibc/linuxthreads-$(LIBC_VERSION)/*.patch

else  # !glibc_have_ext_addon_linuxthreads
  PKG_HAS_NO_SOURCES = defined
endif #  glibc_have_ext_addon_linuxthreads


include $(RULES)
