PKG_TYPE              = DEBIAN3
PKG_VERSION           = 1.43.3
PKG_VER_PATCH         = -1
PKG_SITE_PATH         = pool/main/e/$(PKG_BASE)
#SEPARATE_SRC_BLD_DIRS = defined

include $(DEFS)


AC_VER = 2.69
include $(TOOLS_PKG_DIR)/autoconf.mk
PRE_CONFIGURE_CMD = $(AUTOCONF_CMD)


common_conf_opts = --prefix=$(INST_DIR)

build_conf_opts += --sbindir=$(INST_DIR)/bin
cross_conf_opts += --build=$(BUILD) --host=$(TARGET)

common_enable += symlink-install
common_enable += relative-symlinks
common_enable += symlink-build
common_enable += verbose-makecmds
common_enable += compression
common_enable += htree
common_enable += elf-shlibs
#common_disable += libuuid
#common_disable += libblkid
common_disable += backtrace
 build_enable  += debugfs
 cross_disable += debugfs
common_disable += imager
common_disable += resizer
common_disable += defrag
common_disable += fsck # build fsck wrapper program
common_disable += e2initrd-helper
#common_disable += tls   # disable use of thread local support
common_disable += uuidd # disable building the uuid daemon
common_disable += nls
#common_enable += threads=posix
#common_disable += threads

common_with    += gnu-ld
common_without += libpth-prefix
common_without += libiconv-prefix
#common_with += included-gettext # use the GNU gettext library included here
common_without += libintl-prefix

common_conf_opts += $(addprefix --enable-,$(common_enable))
common_conf_opts += $(addprefix --disable-,$(common_disable))
common_conf_opts += $(addprefix --with-,$(common_with))
common_conf_opts += $(addprefix --without-,$(common_without))

build_conf_opts += $(addprefix --enable-,$(build_enable))
build_conf_opts += $(addprefix --disable-,$(build_disable))
build_conf_opts += $(addprefix --with-,$(build_with))
build_conf_opts += $(addprefix --without-,$(build_without))

cross_conf_opts += $(addprefix --enable-,$(cross_enable))
cross_conf_opts += $(addprefix --disable-,$(cross_disable))
cross_conf_opts += $(addprefix --with-,$(cross_with))
cross_conf_opts += $(addprefix --without-,$(cross_without))

cross_conf_opts += CFLAGS="$(STD_CFLAGS)"
cross_conf_opts += LDFLAGS="$(STD_LDFLAGS)"

      conf_opts  = $(common_conf_opts)

ifeq ($(PLATFORM),build)
      conf_opts += $(build_conf_opts)
else
      conf_opts += $(cross_conf_opts)
endif
      conf_opts += $(STD_VARS)


configure : $(call tstamp_f,install,pkg-config)

CONFIGURE_CMD = cd $(PKG_BLD_DIR) && ./configure $(LOG) $(conf_opts)
    BUILD_CMD = $(BUILD_CMD_DEFAULT)
  INSTALL_CMD = $(INSTALL_CMD_DEFAULT)

define INSTALL_ROOT_CMD
  $(IW) $(INSTxFILES) $(PKG_BLD_DIR)/e2fsck/e2fsck $(RSBIN_DIR)  $(LOG)
  $(IW) $(INSTxFILES)    $(ILIB_DIR)/libuuid.so*    $(RLIB_DIR)  $(LOG)
endef


include $(RULES)
