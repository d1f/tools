PKG_TYPE      = DEBIAN3
PKG_VERSION   = 4.5.20
PKG_VER_PATCH = -2.3
PKG_SITE_PATH = pool/main/s/$(PKG_BASE)

ifeq ($(PLATFORM),build)
  # special case - [un]install itself
  IWTYPE = no
endif

include $(DEFS)


#PKG_PATCH2_FILES = $(PKG_PKG_DIR)/*.patch


ifeq ($(PLATFORM),build)
  STD_CFLAGS += $(if $(filter %64,$(shell uname -m)),-m64)

  define CONFIGURE_CMD
    cd $(PKG_BLD_DIR) && ./configure $(LOG)	\
	     --prefix=$(INST_DIR)		\
	$(STD_VARS)
  endef

  bin_dir = $(BBIN_DIR)
else
  configure : $(TOOLCHAIN_INSTALL_STAMP)

  define CONFIGURE_CMD
    cd $(PKG_BLD_DIR) && ./configure $(LOG)	\
	     --prefix=$(INST_DIR)		\
	--build=$(BUILD) --host=$(TARGET)	\
	$(STD_VARS)
  endef
  # --enable-arm-oabi       enable OABI support on ARM EABI
  # --with-libunwind        use libunwind to implement stack tracing support

  bin_dir = $(RBIN_DIR)
endif


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)


ifeq ($(PLATFORM),build)
    INSTALL_CMD = $(INSTxFILES) $(PKG_BLD_DIR)/strace $(bin_dir)        $(LOG)
  UNINSTALL_CMD = $(RM)                               $(bin_dir)/strace $(LOG)
else
    INSTALL_CMD =   $(INSTALL_CMD_DEFAULT)
    INSTALL_ROOT_CMD = $(IW) $(INSTxFILES) $(IBIN_DIR)/$(PKG) $(RBIN_DIR) $(LOG)
endif


include $(RULES)
