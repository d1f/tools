PKG_TYPE      = DEBIAN3
PKG_VERSION   = 0.26
PKG_VER_PATCH = -1
PKG_SITE_PATH = pool/main/m/$(PKG_BASE)
PLATFORM      = build

include $(DEFS)


AC_VER = 2.69
AM_VER = 1.11
include $(TOOLS_PKG_DIR)/autotools.mk
PRE_CONFIGURE_CMD = $(AUTOTOOLS_CMD)


configure : $(call tstamp_f,install,pkg-config)
configure : $(call bstamp_f,install,libmpdclient)

# Could use pkg-config but what the hell.
LIBMPDCLIENT  = LIBMPDCLIENT_LIBS="-L$(INST_DIR)/lib -lmpdclient "
LIBMPDCLIENT += LIBMPDCLIENT_CFLAGS="-I$(INST_DIR)/include"

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) &&		\
     $(LIBMPDCLIENT)		\
     ./configure $(LOG)		\
	--prefix=$(INST_DIR)	\
	--disable-iconv		\
	$(STD_VARS)
endef


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)


define  INSTALL_CMD
    $(INSTALL_CMD_DEFAULT)
    $(IW) $(INST_FILES) $(PKG_BLD_DIR)/src/mpc.h $(BINC_DIR) $(LOG)
endef


include $(RULES)
