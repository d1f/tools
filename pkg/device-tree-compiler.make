PKG_TYPE          = DEBIAN1
PKG_VERSION       = 1.4.2
PKG_VER_PATCH     = -1
PKG_SITE_PATH     = pool/main/d/$(PKG_BASE)
PLATFORM          = build

include $(DEFS)


include $(TOOLS_PKG_DIR)/quilt.mk


build : $(call bstamp_f,install,flex bison)

MK_VARS = PREFIX=$(INST_DIR)
    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
  INSTALL_CMD =   $(INSTALL_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)


include $(RULES)
