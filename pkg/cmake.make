ifneq ($(strip $(shell which $(PKG) 2>/dev/null)),)

PKG_HAS_NO_SOURCES = defined

else # has no cmake

PKG_TYPE          = DEBIAN3
PKG_VERSION       = 3.7.2
PKG_VER_PATCH     = -1
PKG_SITE_PATH     = pool/main/c/$(PKG_BASE)
PLATFORM          = build

include $(DEFS)


configure : $(call bstamp_f,install,ncurses)

STD_LDFLAGS += -ltinfo

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) &&		\
     $(STD_VARS)		\
     ./bootstrap		\
	--parallel=$(MK_JOBS)	\
	--enable-ccache		\
	--no-qt-gui		\
	--prefix=$(INST_DIR)	$(LOG)
endef


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)
  INSTALL_CMD =   $(INSTALL_CMD_DEFAULT)


endif # has no cmake

include $(RULES)
