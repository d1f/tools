--- glibc/sysdeps/powerpc/fpu/fenv_libc.h.orig	2008-04-12 07:50:26.000000000 +0700
+++ glibc/sysdeps/powerpc/fpu/fenv_libc.h	2013-03-04 19:34:42.040520999 +0700
@@ -27,14 +27,42 @@ libm_hidden_proto (__fe_nomask_env)
 /* The sticky bits in the FPSCR indicating exceptions have occurred.  */
 #define FPSCR_STICKY_BITS ((FE_ALL_EXCEPT | FE_ALL_INVALID) & ~FE_INVALID)
 
+/*
+Nick Clifton:
+
+These definitions only work if the "f" register type is valid, which
+means that both TARGET_HARD_FLOAT and TARGET_FPRS have to be true.
+Thus attempting to build glibc using a powerpc-linux-gnuspe
+toolchain results in the error message:
+
+  error: impossible constraint in 'asm'
+
+The patch below is part of a fix for this problem.  It provides for
+the definition of __NO_FPRS__ when the floating point registers are
+not available, and with such a definition.
+*/
+#ifdef _SOFT_FLOAT
+# define __NO_FPRS__
+#endif
+
 /* Equivalent to fegetenv, but returns a fenv_t instead of taking a
    pointer.  */
+#ifdef __NO_FPRS__
+#define fegetenv_register() \
+        ({ fenv_t env; asm volatile ("mffs %0" : "=r" (env)); env; })
+#else
 #define fegetenv_register() \
         ({ fenv_t env; asm volatile ("mffs %0" : "=f" (env)); env; })
+#endif
 
 /* Equivalent to fesetenv, but takes a fenv_t instead of a pointer.  */
+#ifdef __NO_FPRS__
+#define fesetenv_register(env) \
+        ({ double d = (env); asm volatile ("mtfsf 0xff,%0" : : "r" (d)); })
+#else
 #define fesetenv_register(env) \
         ({ double d = (env); asm volatile ("mtfsf 0xff,%0" : : "f" (d)); })
+#endif
 
 /* This very handy macro:
    - Sets the rounding mode to 'round to nearest';
@@ -133,11 +159,26 @@ enum {
    out by gcc.  */
+#ifdef __NO_FPRS__
+#define f_wash(x) \
+   ({ double d; asm volatile ("fmul %0,%1,%2" \
+			      : "=r"(d) \
+			      : "r" (x), "r"((float)1.0)); d; })
+#else
 #define f_wash(x) \
    ({ double d; asm volatile ("fmul %0,%1,%2" \
 			      : "=f"(d) \
 			      : "f" (x), "f"((float)1.0)); d; })
+#endif
+
+#ifdef __NO_FPRS__
+#define f_washf(x) \
+   ({ float f; asm volatile ("fmuls %0,%1,%2" \
+			     : "=r"(f) \
+			     : "r" (x), "r"((float)1.0)); f; })
+#else
 #define f_washf(x) \
    ({ float f; asm volatile ("fmuls %0,%1,%2" \
 			     : "=f"(f) \
 			     : "f" (x), "f"((float)1.0)); f; })
+#endif
 
 #endif /* fenv_libc.h */
--- glibc/sysdeps/powerpc/fpu/e_sqrt.c.orig	2008-04-12 10:39:18.000000000 +0700
+++ glibc/sysdeps/powerpc/fpu/e_sqrt.c	2013-03-04 19:14:54.180555237 +0700
@@ -177,7 +177,11 @@ __ieee754_sqrt (x)
       /* Volatile is required to prevent the compiler from moving the
          fsqrt instruction above the branch.  */
       __asm __volatile ("	fsqrt	%0,%1\n"
+#ifdef __NO_FPRS__
+				:"=r" (z):"r" (x));
+#else
 				:"=f" (z):"f" (x));
+#endif
     }
   else
     z = __slow_ieee754_sqrt (x);
--- glibc/sysdeps/powerpc/fpu/e_sqrtf.c.orig	2008-04-12 10:39:18.000000000 +0700
+++ glibc/sysdeps/powerpc/fpu/e_sqrtf.c	2013-03-04 19:33:15.460523041 +0700
@@ -154,7 +154,11 @@ __ieee754_sqrtf (x)
       /* Volatile is required to prevent the compiler from moving the
          fsqrt instruction above the branch.  */
       __asm __volatile ("	fsqrts	%0,%1\n"
+#ifdef __NO_FPRS__
+				:"=r" (z):"r" (x));
+#else
 				:"=f" (z):"f" (x));
+#endif
     }
   else
     z = __slow_ieee754_sqrtf (x);
