#!/bin/sh

# shamelessly stolen from crosstool-0.28-rc37 http://kegel.com/crosstool

# Fix problems in linker scripts.
#
# 1. Remove absolute paths
# Any file in a list of known suspects that isn't a symlink is assumed
# to be a linker script.
# FIXME: test -h is not portable
# FIXME: probably need to check more files than just these three...
# Need to use sed instead of just assuming we know what's in libc.so
# because otherwise alpha breaks.
# But won't need to do this at all once we use --with-sysroot
# (available in gcc-3.3.3 and up)
#
# 2. Remove lines containing BUG per
# http://sources.redhat.com/ml/bug-glibc/2003-05/msg00055.html,
# needed to fix gcc-3.2.3/glibc-2.3.2 targeting arm
#
# To make "strip *.so.*" not fail (ptxdist does this),
# rename to .so_orig rather than .so.orig


if test "$#" -ne 1; then
	echo 1>&2
	echo 1>&2 "`basename $0` sysroot-dir"
	echo 1>&2
	exit 1
fi

SYSROOT=$1

set -e

for file in libc.so libpthread.so libgcc_s.so; do
  for lib in lib lib64 usr/lib usr/lib64; do
    if test   -f ${SYSROOT}/$lib/$file &&
       test ! -h ${SYSROOT}/$lib/$file; then
      if test -z "$USE_SYSROOT"; then
        sed -i -e 's,/usr/lib/,,g;s,/usr/lib64/,,g;s,/lib/,,g;s,/lib64/,,g;/BUG in libc.scripts.output-format.sed/d' \
	  ${SYSROOT}/$lib/$file || exit 1
      else
        sed -i -e '/BUG in libc.scripts.output-format.sed/d' \
	  ${SYSROOT}/$lib/$file || exit 1
      fi
    fi
  done
done

exit 0
