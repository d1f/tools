ifndef	tools/pkg/glibc/versions.mk
	tools/pkg/glibc/versions.mk = included


v_glibc                         = 2.0.1  2.0.6
v_glibc_ext_addon_linuxthreads  = 2.0.1  2.0.6
v_glibc                        += 2.1.1  2.1.2  2.1.3
v_glibc_ext_addon_linuxthreads += 2.1.1  2.1.2  2.1.3
v_glibc                        += 2.2    2.2.1  2.2.2  2.2.3  2.2.4  2.2.5
v_glibc_ext_addon_linuxthreads += 2.2    2.2.1  2.2.2  2.2.3  2.2.4  2.2.5
v_glibc                        += 2.3    2.3.1  2.3.2  2.3.3  2.3.4  2.3.5 2.3.6
v_glibc_ext_addon_linuxthreads += 2.3    2.3.1  2.3.2  2.3.3  2.3.4  2.3.5 2.3.6
v_glibc_ext_addon_ports         =                                    2.3.5 2.3.6
v_glibc_ext_addon_libidn        =                             2.3.4  2.3.5 2.3.6
v_glibc                        += 2.4
v_glibc_ext_addon_ports        += 2.4
v_glibc_ext_addon_libidn       += 2.4
v_glibc                        += 2.5    2.5.1
v_glibc_ext_addon_linuxthreads += 2.5
v_glibc_ext_addon_ports        += 2.5
v_glibc_ext_addon_libidn       += 2.5    2.5.1
v_glibc                        += 2.6    2.6.1
v_glibc_ext_addon_ports        += 2.6    2.6.1
v_glibc_ext_addon_libidn       += 2.6    2.6.1
v_glibc                        += 2.7    2.8    2.9
v_glibc_ext_addon_ports        += 2.7    2.8    2.9
v_glibc_ext_addon_libidn       += 2.7    2.8    2.9
v_glibc                        += 2.10.1 2.11   2.11.1 2.11.2 2.11.3
v_glibc_ext_addon_ports        += 2.10.1 2.11
v_glibc_ext_addon_libidn       += 2.10.1
v_glibc                        += 2.12.1 2.12.2 2.13   2.14   2.14.1 2.15  2.16.0 2.17
v_glibc_ext_addon_ports        += 2.12.1        2.13   2.14   2.14.1 2.15  2.16.0


endif # tools/pkg/glibc/versions.mk
