ifndef	tools/pkg/glibc/common.mk
	tools/pkg/glibc/common.mk = included

ifndef	    xwmake/defs.mk
$(error	$(I)xwmake/defs.mk did not included)
endif


$(if $(filter glibc%,$(PKG)),,$(error Only for glibc% packages))


# glibc/versions.mk, glibc_have_*, addons dependencies, addon_dirs
include $(TOOLS_PKG_DIR)/glibc/addons.mk


# libc_cv_ppc_machine=yes - crosstools:
# Override libc_cv_ppc_machine so glibc-cvs doesn't complain
# 'a version of binutils that supports .machine "altivec" is needed'.
GLIBC_CONF_ENVS  = libc_cv_ppc_machine=yes

GLIBC_CONF_ENVS += PATH=$(tc_PREFIX)/bin:$$PATH
GLIBC_CONF_ENVS += libc_cv_forced_unwind=yes
GLIBC_CONF_ENVS += libc_cv_c_cleanup=yes
#GLIBC_CONF_ENVS += libc_cv_z_initfirst=yes
GLIBC_CONF_ENVS += libc_cv_z_relro=yes
GLIBC_CONF_ENVS += sysincludedir=$(tc_HEADERDIR)
GLIBC_CONF_ENVS += BUILD_CC=$(BUILD_CC)
GLIBC_CONF_ENVS +=       AR=$(TARGET)-ar
GLIBC_CONF_ENVS +=   RANLIB=$(TARGET)-ranlib


GLIBC_CONF_OPTS  = --prefix=
GLIBC_CONF_OPTS += --build=$(BUILD)
GLIBC_CONF_OPTS += --host=$(TARGET)
ifneq ($(PLATFORM),build)
GLIBC_CONF_OPTS += --with-headers=$(tc_HEADERDIR)
GLIBC_CONF_OPTS += --enable-kernel=$(LK_VERSION)
endif
GLIBC_CONF_OPTS += --without-cvs
GLIBC_CONF_OPTS += --without-gd
GLIBC_CONF_OPTS += --disable-profile
GLIBC_CONF_OPTS += --disable-debug
GLIBC_CONF_OPTS += --enable-shared
GLIBC_CONF_OPTS += --disable-static
GLIBC_CONF_OPTS += --with-elf
GLIBC_CONF_OPTS += --without-xcoff
GLIBC_CONF_OPTS += --disable-nls


# __thread -> nptl

ifeq ($(shell $(VERCMP) $(LIBC_VERSION) '<' 2.4),true)
GLIBC_CONF_OPTS += --enable-add-ons=$(addon_list_comma)
GLIBC_CONF_OPTS += --without-tls
GLIBC_CONF_OPTS += --without-__thread
endif

ifeq ($(shell $(VERCMP) $(LIBC_VERSION) '>=' 2.4),true)
#
# configure: error: compiler support for __thread is required
#
# Checking gcc for __thread support ... no
# Could not find a gcc that supports the __thread directive!
# please update to gcc-3.2.2-r1 or later, and try again.
#
# "Since version 2.3.5, the glibc is built with TLS support on all
# platforms which support it, unless it is explicitely disabled.
#
# oldier binutils does not contain support for the
# required TLS relocations.  So the configure script is probably deciding
# that TLS can't be done, even though your compiler is up to scratch.
# 2.16.1 is too old for ARM EABI work.
#
# you need binutils-2.16.90+
#

# glibc-2.4 has no linuxthreads, replace to nptl

ifeq ($(shell $(VERCMP) $(LIBC_VERSION) '<'  2.20),true)
addon_list2 = $(strip nptl $(filter-out linuxthreads,$(addon_list)))
endif

addon_list_comma = $(strip $(subst $(SPACE),$(COMMA),$(addon_list2)))

GLIBC_CONF_OPTS += --enable-add-ons=$(addon_list_comma)
GLIBC_CONF_OPTS += --with-tls
GLIBC_CONF_OPTS += --with-__thread
endif


#GLIBC_CONF_OPTS += --disable-sanity-checks

ifeq ($(shell $(VERCMP) $(LIBC_VERSION) '>=' 2.17),true)
GLIBC_CONF_OPTS += --disable-nscd
endif

GLIBC_CONF_OPTS += $(GLIBC_EXTRA_CONFIG)

# if using floating-point hardware [default=yes]
#	--without-fp
# build undebuggable optimized library [default=no]
#	--enable-omitfp
# specify location of binutils (as and ld)
#	--with-binutils=$(tc_SYSROOT)/bin


_MKJ_ := $(MKJ)
 MKJ  :=
MK_VARS += PARALLELMFLAGS=$(_MKJ_)

MK_VARS += cross-compiling=yes
MK_VARS += install_root=$(tc_SYSROOT)

#MK_VARS += LD=$(TARGET)-ld RANLIB=$(TARGET)-ranlib
#MK_VARS += prefix=
#MK_VARS += sysincludedir=$(tc_HEADERDIR)


endif # tools/pkg/glibc/common.mk
