ifndef	tools/pkg/glibc/addons.mk
	tools/pkg/glibc/addons.mk = included

ifndef	    xwmake/defs.mk
$(error	$(I)xwmake/defs.mk did not included)
endif


$(if $(filter glibc%,$(PKG)),,$(error Only for glibc% packages))


include $(TOOLS_PKG_DIR)/glibc/versions.mk

glibc_have_ext_addon_linuxthreads = $(filter $(v_glibc_ext_addon_linuxthreads),$(LIBC_VERSION))
glibc_have_ext_addon_ports        = $(filter $(v_glibc_ext_addon_ports),$(LIBC_VERSION))
# Libidn's purpose is to encode and decode internationalized domain names.
#glibc_have_ext_addon_libidn       = $(filter $(v_glibc_ext_addon_libidn),$(LIBC_VERSION))


ifneq ($(glibc_have_ext_addon_linuxthreads),)
 define GLIBC_ADDON_LINUXTHREADS_PATCH_DEP
  patch : $(call tstamp_f,patch,glibc-linuxthreads)
 endef
  addon_dirs += $(PATCHED_SRC_DIR)/glibc-linuxthreads_$(LIBC_VERSION)/linuxthreads
  addon_dirs += $(PATCHED_SRC_DIR)/glibc-linuxthreads_$(LIBC_VERSION)/linuxthreads_db
  addon_list += linuxthreads
endif

ifneq ($(glibc_have_ext_addon_ports),)
 define GLIBC_ADDON_PORTS_PATCH_DEP
  patch : $(call tstamp_f,patch,glibc-ports)
 endef
  addon_dirs += $(PATCHED_SRC_DIR)/glibc-ports_$(LIBC_VERSION)
  addon_list += ports
endif

# 2.17 has ports included, no separate package
ifeq ($(shell $(VERCMP) $(LIBC_VERSION) '>=' 2.17),true)
ifeq ($(shell $(VERCMP) $(LIBC_VERSION) '<'  2.20),true)
  addon_list += ports
endif
endif

ifneq ($(glibc_have_ext_addon_libidn),)
 define GLIBC_ADDON_LIBIDN_PATCH_DEP
  patch : $(call tstamp_f,patch,glibc-libidn)
 endef
  addon_dirs += $(PATCHED_SRC_DIR)/glibc-libidn_$(LIBC_VERSION)/libidn
  addon_list += libidn
endif

addon_list_comma = $(subst $(SPACE),$(COMMA),$(addon_list))


endif # tools/pkg/glibc/addons.mk
