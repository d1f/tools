PKG_VERSION   = $(LIBC_VERSION)
PKG_SITES     = $(GNU_ORG_SITES)
PKG_SITE_PATH = glibc


include $(DEFS)


ifdef EXTERN_TOOLCHAIN

PKG_HAS_NO_SOURCES = defined

else


POST_EXTRACT_CMD = $(RM) $(PKG_ORIG_SRC_DIR)/manual/libc.info* $(LOG)


include $(TOOLS_PKG_DIR)/crosstool.mk
PKG_PATCH_FILES  = $(CROSSTOOL_PATCH_DIR)/glibc/$(LIBC_VERSION)/*.patch
PKG_PATCH_FILES +=       $(TOOLS_PKG_DIR)/glibc/$(LIBC_VERSION)/*.patch
PKG_PATCH_FILES +=         $(PRJ_PKG_DIR)/glibc/$(LIBC_VERSION)/*.patch


include $(TOOLS_PKG_DIR)/glibc/common.mk

$(GLIBC_ADDON_LINUXTHREADS_PATCH_DEP)
$(GLIBC_ADDON_PORTS_PATCH_DEP)
#$(GLIBC_ADDON_LIBIDN_PATCH_DEP)

ifeq ($(filter 2.3.5 2.3.6 2.4 2.6% 2.7,$(LIBC_VERSION)),)

define NO_GCC_EH_CMD
  # -lgcc_eh is absent, linking failed
  echo "$(I2)Cutting off -lgcc_eh ..."
  $(RELINK)                    $(PKG_PATCHED_SRC_DIR)/Makeconfig $(LOG)
  $(SED) -i -e "s,-lgcc_eh,,g" $(PKG_PATCHED_SRC_DIR)/Makeconfig $(LOG)
endef

endif

ifeq ($(filter 2.16,%,$(LIBC_VERSION)),)

define NO_GCC_S_CMD
  echo "$(I2)Cutting off -lgcc_s  ..."
  $(RELINK)                    $(PKG_PATCHED_SRC_DIR)/Makeconfig $(LOG)
  $(SED) -i -e "s,-lgcc_s,,g"  $(PKG_PATCHED_SRC_DIR)/Makeconfig $(LOG)
endef

endif

       patch : $(call bstamp_f,install,sed)
define PATCH_CMD
	@echo "$(I2)Integrate add-ons ..."
	@if test -n "$(addon_dirs)"; then cd $(PKG_PATCHED_SRC_DIR) && \
	for a in $(addon_dirs); do \
		bn=`basename $$a | sed -e 's|glibc\-\(.\+\)_$(LIBC_VERSION)|\1|'`; \
		$(LNTREE) $$a $$bn; \
	done; \
	fi

	$(NO_GCC_EH_CMD)
	$(NO_GCC_S_CMD)
endef

ifeq ($(shell $(VERCMP) $(LIBC_VERSION) '>=' 2.10),true)
ifeq ($(shell $(VERCMP) $(LIBC_VERSION) '<=' 2.16),true)

define PATCH2_CMD
	@echo "$(I2)Symlinking glibc-ports-$(LIBC_VERSION) to ports ..."
	@$(LN_S) ports $(PKG_PATCHED_SRC_DIR)/glibc-ports-$(LIBC_VERSION) $(LOG)
endef

PKG_PATCH3_FILES  = $(CROSSTOOL_PATCH_DIR)/glibc/ports-$(LIBC_VERSION)/*.patch
endif
endif


configure : $(call bstamp_f,install,gawk make-3.81)

ifneq ($(PLATFORM),build)
configure : $(call tstamp_f,install,gcc-nolibc linux-headers)
configure : $(call tstamp_f,configure,linux)
else
  AC_VER = 2.69
  include $(TOOLS_PKG_DIR)/autoconf.mk
  GLIBC_CONF_ENVS += $(AC_VARS)
endif

PKG_BLD_SUB_DIR = $(PKG_BLD_DIR)/build

define CONFIGURE_CMD
  unset LD_LIBRARY_PATH;		\
  cd $(PKG_BLD_SUB_DIR)		&&	\
	$(GLIBC_CONF_ENVS)		\
        CC=$(CC)			\
	CFLAGS="$(STD_CFLAGS)"		\
  $(PKG_BLD_DIR)/configure	$(LOG)	\
	$(GLIBC_CONF_OPTS)
endef


MK_VARS += LINGUAS=

BUILD_CMD        = $(BUILD_CMD_DEFAULT)


uninstall : $(call tstamp_f,uninstall,glibc-root)

define INSTALL_CMD
  $(INSTALL_CMD_DEFAULT)
  $(TOOLS_PKG_DIR)/glibc/fix-ld-scripts.sh $(tc_SYSROOT) $(LOG)
endef


    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)


endif # ! EXTERN_TOOLCHAIN


include $(RULES)
