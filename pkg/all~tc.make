# all packages w/o toolchain

PKG_HAS_NO_SOURCES = defined

include $(DEFS)


filter_out   = all% %all toolchain glibc% uclibc% linux-headers% linux
filter_out  += gcc% binutils% checkinstall installwatch ccache
filter_out  += $(TOOLCHAIN_DEP_PKGES) root
filter_out  += sed gawk flex bison m4 quilt dpatch bzip2 xz make-dfsg make-3.81
filter_out  += uboot-mkimage unifdef attr acl perl
filter_out  += libtool help2man autoconf-% automake-% autotools-dev crosstool%
filter_out  += gmp mpfr coreutils-wrappers
filter_out  += texinfo pkg-config gettext ragel fdupes
NESTED_PKGES = $(filter-out $(filter_out),$(ALL_PKGES))


include $(RULES)
