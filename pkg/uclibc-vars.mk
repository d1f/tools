# include only from uclibc*.make

# install_headers: $(PREFIX)$(DEVEL_PREFIX)include/bits/
# install_dev    : $(PREFIX)$(DEVEL_PREFIX)lib/libc.so
# install_runtime: $(PREFIX)$(RUNTIME_PREFIX)lib/*.so

ifndef	tools/pkg/uclibc-vars.mk
	tools/pkg/uclibc-vars.mk = included


MK_VARS +=         PREFIX=
MK_VARS +=   DEVEL_PREFIX=$(tc_SYSROOT)/
MK_VARS += RUNTIME_PREFIX=$(tc_SYSROOT)/
MK_VARS += CROSS=$(CROSS_PREFIX)
MK_VARS += CROSS_COMPILER_PREFIX=$(CROSS_PREFIX)
MK_VARS += CC=$(CC)
MK_VARS += HOSTCC=$(BUILD_CC)
MK_VARS += TARGET_ARCH=$(NARCH) ARCH=$(NARCH)
MK_VARS += KERNEL_SOURCE=$(LK_DIR)
MK_VARS += KERNEL_HEADERS=$(LK_INC_DIR)
MK_VARS += SHARED_LIB_LOADER_PREFIX=/lib
MK_VARS += V=1

ifeq ($(filter $(NOT_BUILD_TARGETS),$(MAKECMDGOALS)),)
 ifeq ($(ENDIANESS),LITTLE)
  MK_VARS += ARCH_LITTLE_ENDIAN=y ARCH_BIG_ENDIAN=n
 else ifeq ($(ENDIANESS),BIG)
  MK_VARS += ARCH_LITTLE_ENDIAN=n ARCH_BIG_ENDIAN=y
 else
  ifndef XDEP
  $(error Unknown ENDIANESS "$(ENDIANESS)", see $(PLATFORM_MK))
  endif
 endif
endif


PKG_MENUCONFIG_FILE = $(PLATFORM_DIR)/uclibc.config


endif # tools/pkg/uclibc-vars.mk
