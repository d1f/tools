/*
 *  nandcmp.c
 *
 *  Copyright (C) 2012 Sigrand LLC <http://sigrand.ru>
 *  Written by Dmitry Fedorov <dm.fedorov@gmail.com>
 *
 *  Derived from:
 *  nanddump.c
 *  Copyright (C) 2000 David Woodhouse (dwmw2@infradead.org)
 *                     Steven J. Hill (sjhill@realitydiluted.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 *  Overview:
 *   This utility compares the contents of raw NAND chips with file.
 */

#define PROGRAM_NAME "nandcmp"

#define _GNU_SOURCE
#include <ctype.h>
#include <errno.h>
#include <fcntl.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <getopt.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>

#include <asm/types.h>
#include <mtd/mtd-user.h>
#include "common.h"
#include <libmtd.h>

#undef  VERSION
#define VERSION "0.0"

static void display_help(void)
{
    printf(
"Usage: %s [OPTIONS] MTD-device file\n"
"Compares the contents of a nand mtd partition with file.\n"
"\n"
"           --help               Display this help and exit\n"
"           --version            Output version information and exit\n"
"-q         --quiet              Don't display progress and status messages\n"
	   ,PROGRAM_NAME);
    exit(EXIT_SUCCESS);
}

static void display_version(void)
{
    printf("%1$s " VERSION "\n"
	   "\n"
	   "%1$s comes with NO WARRANTY\n"
	   "to the extent permitted by law.\n"
	   "\n"
	   "You may redistribute copies of %1$s\n"
	   "under the terms of the GNU General Public Licence.\n"
	   "See the file `COPYING' for more information.\n",
	   PROGRAM_NAME);
    exit(EXIT_SUCCESS);
}

static const char		*mtddev;		// mtd device name
static const char		*file;			// file name
static bool			quiet = false;		// suppress diagnostic output
static long long		start_addr;		// start address
static long long		length;			// dump length

static void process_options(int argc, char * const argv[])
{
    int error = 0;

    for (;;)
    {
	int option_index = 0;
	static const char *short_options = "q";
	static const struct option long_options[] =
	{
	    {"help",    no_argument, 0, 0},
	    {"version", no_argument, 0, 0},
	    {"quiet",   no_argument, 0, 'q'},
	    {0, 0, 0, 0},
	};

	int c = getopt_long(argc, argv, short_options,
			    long_options, &option_index);
	if (c == EOF)
	{
	    break;
	}

	switch (c)
	{
	    case 0:
		switch (option_index)
		{
		    case 0:
			display_help();
			break;
		    case 1:
			display_version();
			break;
		}
		break;
	    case 'q':
		quiet = true;
		break;
	    case '?':
		error++;
		break;
	}
    }

    if ((argc - optind) != 2 || error)
	display_help();

    mtddev = argv[optind];
    file   = argv[optind+1];
}

int main(int argc, char * const argv[])
{
    long long ofs, end_addr = 0;
    long long blockstart = 1;
    int fd, ifd = 0, bs, badblock = 0;
    struct mtd_dev_info mtd;
    int firstblock = 1;
    struct mtd_ecc_stats stat1, stat2;
    bool eccstats = false;
    unsigned char *readbuf = NULL, *filebuf = NULL;
    libmtd_t mtd_desc;

    process_options(argc, argv);

    /* Initialize libmtd */
    mtd_desc = libmtd_open();
    if (!mtd_desc)
	return errmsg("can't initialize libmtd");

    /* Open MTD device */
    if ((fd = open(mtddev, O_RDONLY)) == -1) {
	perror(mtddev);
	exit(EXIT_FAILURE);
    }

    /* Fill in MTD device capability structure */
    if (mtd_get_dev_info(mtd_desc, mtddev, &mtd) < 0)
	return errmsg("mtd_get_dev_info failed");

    /* Allocate buffers */
    readbuf = xmalloc(mtd.min_io_size);
    filebuf = xmalloc(mtd.min_io_size);

    /* check if we can read ecc stats */
    if (!ioctl(fd, ECCGETSTATS, &stat1))
    {
	eccstats = true;
	if (!quiet)
	{
	    fprintf(stderr, "ECC failed: %d\n", stat1.failed);
	    fprintf(stderr, "ECC corrected: %d\n", stat1.corrected);
	    fprintf(stderr, "Number of bad blocks: %d\n", stat1.badblocks);
	    fprintf(stderr, "Number of bbt blocks: %d\n", stat1.bbtblocks);
	}
    }
    else
	perror("No ECC status information available");


    if ((ifd = open(file, O_RDONLY))== -1)
    {
	perror(file);
	goto closeall;
    }

    struct stat statbuf;
    memset(&statbuf, 0, sizeof(statbuf));
    if (fstat(ifd, &statbuf) < 0)
	return errmsg("fstat %s failed", file);
    length = statbuf.st_size;

    end_addr = length;

    bs = mtd.min_io_size;

    /* Print informative message */
    if (!quiet)
    {
	fprintf(stderr, "Block size %d, page size %d, OOB size %d\n",
		mtd.eb_size, mtd.min_io_size, mtd.oob_size);
	fprintf(stderr,
		"Comparing data starting at 0x%08llx and ending at 0x%08llx...\n",
		start_addr, end_addr);
    }

    /* Dump the flash contents */
    for (ofs = start_addr; ofs < end_addr; ofs += bs)
    {
	/* Check for bad block */
	if (blockstart != (ofs & (~mtd.eb_size + 1)) || firstblock)
	{
	    blockstart = ofs & (~mtd.eb_size + 1);
	    firstblock = 0;
	    if ((badblock = mtd_is_bad(&mtd, fd, ofs / mtd.eb_size)) < 0)
	    {
		errmsg("libmtd: mtd_is_bad");
		goto closeall;
	    }
	}

	if (badblock)
	{
	    continue;
	}
	else
	{
	    /* Read page data and exit on failure */
	    if (mtd_read(&mtd, fd, ofs / mtd.eb_size, ofs % mtd.eb_size, readbuf, bs))
	    {
		errmsg("mtd_read");
		goto closeall;
	    }
	}

	/* ECC stats available ? */
	if (eccstats)
	{
	    if (ioctl(fd, ECCGETSTATS, &stat2))
	    {
		perror("ioctl(ECCGETSTATS)");
		goto closeall;
	    }
	    if (stat1.failed != stat2.failed)
		fprintf(stderr, "ECC: %d uncorrectable bitflip(s)"
			" at offset 0x%08llx\n",
			stat2.failed - stat1.failed, ofs);
	    if (stat1.corrected != stat2.corrected)
		fprintf(stderr, "ECC: %d corrected bitflip(s) at"
			" offset 0x%08llx\n",
			stat2.corrected - stat1.corrected, ofs);
	    stat1 = stat2;
	}

	int ret = read(ifd, filebuf, bs);
	if (ret < 0)
	{
	    sys_errmsg("read file %s", file);
	    goto closeall;
	}
	else if (ret == 0)
	{
	    errmsg("Unexpected EOF on file %s", file);
	    goto closeall;
	}

	if (memcmp(readbuf, filebuf, ret) != 0)
	{
	    errmsg("file %s and device %s are differ", file, mtddev);
	    goto closeall;
	}
    }

    /* Close the input file and MTD device, free memory */
    close(fd);
    close(ifd);
    free(filebuf);
    free(readbuf);

    /* Exit happy */
    return EXIT_SUCCESS;

closeall:
    close(fd);
    close(ifd);
    free(filebuf);
    free(readbuf);
    exit(EXIT_FAILURE);
}
