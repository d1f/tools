# Usage:
# include $(TOOLS_PKG_DIR)/dpatch.mk
# after include $(DEFS)

ifndef	tools/pkg/dpatch.mk
	tools/pkg/dpatch.mk = included

ifndef	    xwmake/defs.mk
$(error	$(I)xwmake/defs.mk did not included)
endif

ifeq ($(findstring DEBIAN,$(PKG_TYPE)),DEBIAN)

patch : $(call bstamp_f,install,dpatch)
PATCH_CMD = $(APPLY_DPATCHES_CMD)

ifeq ($(PKG_TYPE),DEBIAN3)
$(error DEBIAN3 uses quilt patch system!)
endif

endif # PKG_TYPE == DEBIAN%


endif # tools/pkg/dpatch.mk
