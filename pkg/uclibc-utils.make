PKG_VERSION = $(LIBC_VERSION)
PKG_HAS_NO_SOURCES = defined

include $(DEFS)


include $(TOOLS_PKG_DIR)/uclibc-vars.mk

install : $(call tstamp_f,install,uclibc-root gcc-libc)

PKG_BLD_DIR = $(BLD_DIR)/uclibc

INSTALL_CMD_TARGET = install_utils
INSTALL_CMD = $(INSTALL_CMD_DEFAULT)
INSTALL_ROOT_CMD = $(IW) $(INSTxFILES) $(tc_SYSROOT)/bin/ldd $(RBIN_DIR) $(LOG)


include $(RULES)
