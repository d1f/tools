ifndef	tools/pkg/autotools.mk
	tools/pkg/autotools.mk = included


define __autotools_usage__

AC_VER = 2.69
AM_VER = 1.11
include $(TOOLS_PKG_DIR)/autotools.mk
PRE_CONFIGURE_CMD = $(AUTOTOOLS_CMD)

endef


include $(TOOLS_PKG_DIR)/autoconf.mk
include $(TOOLS_PKG_DIR)/automake.mk

#+ libtool.mk
ifeq ($(filter libtool%,$(PKG)),)
pre-configure : $(call bstamp_f,install,libtool)
endif

LIBTOOLIZE_FLAGS = --force --copy #--verbose

LIBTOOLIZE_CMD_DEFAULT = cd $(PKG_BLD_SUB_DIR) && libtoolize $(LIBTOOLIZE_FLAGS) $(LOG)
#- libtool.mk


define AUTOTOOLS_CMD
  $(RELINK) $(PKG_BLD_SUB_DIR)/aclocal.m4 $(PKG_BLD_SUB_DIR)/configure $(LOG)
  cd $(PKG_BLD_SUB_DIR) && libtoolize    $(LIBTOOLIZE_FLAGS)	$(LOG)
  cd $(PKG_BLD_SUB_DIR) && $(ACLOCAL)       $(ACLOCAL_FLAGS)	$(LOG)
  cd $(PKG_BLD_SUB_DIR) && $(AUTOHEADER) $(AUTOHEADER_FLAGS)	$(LOG)
  cd $(PKG_BLD_SUB_DIR) && $(AUTOMAKE)     $(AUTOMAKE_FLAGS)	$(LOG)
  cd $(PKG_BLD_SUB_DIR) && $(AUTORECONF) $(AUTORECONF_FLAGS)	$(LOG)
endef


# AUTOCONF AUTOHEADER AUTOMAKE ACLOCAL AUTOPOINT LIBTOOLIZE M4 MAKE


endif # tools/pkg/autotools.mk
