ifneq ($(strip $(shell which $(PKG) 2>/dev/null)),)

PKG_HAS_NO_SOURCES = defined

else # has no $(PKG)

PKG_TYPE      = DEBIAN3
pkg_version   = 4.1.3
PKG_VERSION   = $(pkg_version)+dfsg
PKG_VER_PATCH = -0.1
PKG_SITE_PATH = pool/main/g/$(PKG_BASE)
PLATFORM      = build

include $(DEFS)


# shamelessly stolen from debian/rules
define PRE_CONFIGURE_CMD
  touch --date="Jan 01 2000"			\
	$(PKG_BLD_DIR)/doc/gawktexi.in		\
	$(PKG_BLD_DIR)/doc/gawk.texi		\
	$(PKG_BLD_DIR)/doc/gawkinet.texi	\
	$(PKG_BLD_DIR)/doc/gawk.info		\
	$(PKG_BLD_DIR)/doc/gawkinet.info	\
	$(PKG_BLD_DIR)/doc/sidebar.awk
endef

configure : $(call bstamp_f,install,ccache)

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR)	&&		\
    ./configure -C	$(LOG)		\
	--prefix=$(INST_DIR)		\
	--disable-lint			\
	--disable-nls			\
	--with-gnu-ld			\
	--without-libiconv-prefix	\
	--without-libintl-prefix	\
	--without-libsigsegv-prefix	\
	$(STD_VARS)
endef


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)
  INSTALL_CMD =   $(INSTALL_CMD_DEFAULT)


endif # has no $(PKG)

include $(RULES)
