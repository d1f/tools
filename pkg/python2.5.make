PKG_TYPE      = DEBIAN1
pkg_ver_major = 2.5
PKG_VERSION   = $(pkg_ver_major).5
PKG_VER_PATCH = -11
PKG_SITE_PATH = pool/main/p/$(PKG_BASE)
PLATFORM      = build

include $(DEFS)


include $(TOOLS_PKG_DIR)/dpatch.mk


define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) && \
  ac_cv_prog_SVNVERSION="not-found"	\
     ./configure -C $(LOG)		\
	--prefix=$(INST_DIR)		\
	--enable-shared			\
	--disable-ipv6			\
	--enable-unicode=ucs4		\
	--with-system-ffi		\
	--with-signal-module		\
	--without-threads		\
	--without-pth			\
	--without-doc-strings		\
	--without-tsc			\
	--without-pymalloc		\
	--with-wctype-functions		\
	--with-fpectl			\
	$(STD_VARS)
endef


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)
  INSTALL_CMD =   $(INSTALL_CMD_DEFAULT)


include $(RULES)
