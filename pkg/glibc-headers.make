# crosstool:
# Install glibc headers needed to build bootstrap compiler
# -- but only if gcc-3.x"
#
# Only need to install bootstrap glibc headers for gcc-3.0 and above?
# Or maybe just gcc-3.3 and above?
# See also http://gcc.gnu.org/PR8180,
# which complains about the need for this step.
# Don't install them if they're already there (it's really slow)

PKG_VERSION = $(LIBC_VERSION)
PKG_HAS_NO_SOURCES    = defined
SEPARATE_SRC_BLD_DIRS = defined

include $(DEFS)


ifdef EXTERN_TOOLCHAIN

PKG_HAS_NO_SOURCES = defined

else


AC_VER = 2.69
include $(TOOLS_PKG_DIR)/autoconf.mk

configure : $(call tstamp_f,patch,glibc)
configure : $(call bstamp_f,install,sed gawk)

GLIBC_SRC_DIR = $(PATCHED_SRC_DIR)/glibc_$(LIBC_VERSION)


# gcc-[34].x only
gcc34 = $(or $(filter 3.%,$(GCC_VERSION)),$(filter 4.%,$(GCC_VERSION)))
ifneq ($(gcc34),)

configure : $(call tstamp_f,install,linux-headers)

include $(TOOLS_PKG_DIR)/glibc/common.mk

ifeq ($(NARCH),powerpc)
  GLIBC_CONF_ENVS += libc_cv_mabi_ibmlongdouble=yes
  GLIBC_CONF_ENVS += libc_cv_mlong_double_128=yes
endif

define CONFIGURE_CMD
  unset LD_LIBRARY_PATH;		\
  cd $(PKG_BLD_DIR)		&&	\
	$(GLIBC_CONF_ENVS)		\
        CC=$(BUILD_CC)			\
	CFLAGS="-O1 -pipe"		\
  $(GLIBC_SRC_DIR)/configure $(LOG)	\
	$(GLIBC_CONF_OPTS)
endef


MK_VARS += CFLAGS="-DBOOTSTRAP_GCC -O1 -pipe"

# glibc 2.3.x only
ifneq ($(filter 2.3.%,$(LIBC_VERSION)),)

define BUILD_CMD
  $(MKDIR_P) $(PKG_BLD_DIR)/stdio-common			$(LOG)
  touch      $(PKG_BLD_DIR)/stdio-common/errlist-compat.c	$(LOG)
endef

endif # glibc 2.3.x only


# "make install" of big progs (glibc-headers) are failed with vfork
# when running from strace.
# Strace is too complicated for me to fix this and I fix "make" instead
# by disabling vfork. -- df
#install : $(call bstamp_f,install,make-3.81)

MK_VARS += _CC_NO_ABI_=1
define INSTALL_CMD
  $(GLIBC_CONF_ENVS) $(IWMAKE) -C $(PKG_BLD_DIR) $(MK_VARS) install-headers           $(LOG)

  $(IW) $(MKDIR_P)                                        $(tc_HEADERDIR)/gnu         $(LOG)
  $(IW) touch                                             $(tc_HEADERDIR)/gnu/stubs.h $(LOG)
  $(IW) $(INST_FILES) $(GLIBC_SRC_DIR)/include/features.h $(tc_HEADERDIR)             $(LOG)
  $(IW) touch           $(PKG_BLD_DIR)/bits/stdio_lim.h                               $(LOG)
  $(IW) $(INST_FILES)   $(PKG_BLD_DIR)/bits/stdio_lim.h   $(tc_HEADERDIR)/bits        $(LOG)
endef
  # crosstool:
  # Following error building gcc-4.0.0's gcj:
  #  error: bits/syscall.h: No such file or directory
  # solved by following copy; see
  # http://sourceware.org/ml/crossgcc/2005-05/msg00168.html
  # but it breaks arm, see http://sourceware.org/ml/crossgcc/2006-01/msg00091.html
  # so uncomment this if you need it
  #$(IW) $(INST_FILE) $(PKG_BLD_DIR)/misc/syscall-list.h $(tc_HEADERDIR)/bits/syscall.h	$(LOG)

endif # gcc-3.x only

endif # ! EXTERN_TOOLCHAIN


include $(RULES)
