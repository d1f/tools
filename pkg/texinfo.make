ifneq ($(strip $(shell which makeinfo 2>/dev/null)),)

PKG_HAS_NO_SOURCES = defined

else # has no texinfo

PKG_TYPE      = DEBIAN3
PKG_VERSION   = 6.3.0.dfsg.1
PKG_VER_PATCH = -1
PKG_SITE_PATH = pool/main/t/$(PKG_BASE)
PLATFORM      = build

include $(DEFS)


configure : $(call bstamp_f,install,gawk sed help2man)

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) &&					\
     ./configure -C	$(LOG)				\
	--prefix=$(INST_DIR)				\
	--enable-threads=posix				\
	--disable-multiplatform				\
	--disable-nls					\
	--disable-perl-xs				\
	--disable-tp-tests				\
	--disable-perl-api-texi-build			\
	--disable-pod-simple-texinfo-tests		\
	--with-gnu-ld					\
	--without-libiconv-prefix			\
	--without-included-regex			\
	--without-libpth-prefix				\
	--without-external-Unicode-EastAsianWidth	\
	--without-external-Text-Unidecode		\
	--without-libintl-prefix			\
	--without-external-libintl-perl			\
	$(STD_VARS)
endef


define BUILD_CMD
  $(RM) $(PKG_BLD_DIR)/texindex/texindex $(LOG)
  $(RM) $(PKG_BLD_DIR)/man/texi2dvi.1    $(LOG)
  $(BUILD_CMD_DEFAULT)
endef

    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)
  INSTALL_CMD =   $(INSTALL_CMD_DEFAULT)


endif # has no texinfo

include $(RULES)
