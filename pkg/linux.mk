ifndef	tools/pkg/linux.mk
	tools/pkg/linux.mk = included


ifndef	    xwmake/defs.mk
$(error	$(I)xwmake/defs.mk did not included)
endif


# place this before include $(DEFS)
#LK_LOCALVERSION = -something

# goes from $(PRJ_DIR)/setup.sh
ifndef    LK_VERSION
  $(error LK_VERSION is not defined!)
endif

LK_RELEASE	= $(LK_VERSION)$(LK_LOCALVERSION)

lk_ver_space	= $(subst .,$(SPACE),$(LK_VERSION))
LK_V1		= $(word 1,$(lk_ver_space))
LK_V2		= $(word 2,$(lk_ver_space))
LK_V3		= $(word 3,$(lk_ver_space))
LK_V4		= $(word 4,$(lk_ver_space))
LK_V12		= $(LK_V1).$(LK_V2)

LK_DIR		= $(BLD_DIR)/linux
LK_INC_DIR	= $(LK_DIR)/include


LK_SRCARCH	?= $(NARCH)

ifeq ($(LK_V12),2.6)
  ifeq ($(NARCH),i386)
    LK_SRCARCH = x86
  endif
  ifeq ($(NARCH),x86_64)
    LK_SRCARCH = x86
  endif
endif


endif # tools/pkg/linux.mk
