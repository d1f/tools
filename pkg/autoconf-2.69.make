AC_VER        = 2.69

PKG_TYPE      = DEBIAN3
PKG_VER_PATCH = -10~bpo8+1
PKG_BASE      = autoconf

include $(TOP_DIR)/tools/pkg/autoconf-common.mk
