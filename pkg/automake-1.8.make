AC_VER = 2.59
AM_VER = 1.8

PKG_TYPE      = DEBIAN1
PKG_VER_MAJOR = $(AM_VER)
PKG_VER_MINOR = 5+nogfdl
PKG_VER_PATCH = -2
PKG_BASE      = automake$(PKG_VER_MAJOR)

include $(TOP_DIR)/tools/pkg/automake-common.mk
