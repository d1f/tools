ifneq ($(strip $(shell make --version 2>/dev/null | fgrep "GNU Make 3.")),)

PKG_HAS_NO_SOURCES = defined

else # has no GNU make 3.x

PKG_TYPE      = DEBIAN1
PKG_VERSION   = 3.81
PKG_VER_PATCH = -8.2
PKG_BASE      = make-dfsg
PKG_SITE_PATH = pool/main/m/$(PKG_BASE)

PLATFORM      = build

SEPARATE_SRC_BLD_DIRS=defined

include $(DEFS)


PRE_CONFIGURE_CMD = $(RM_R) $(PKG_ORIG_SRC_DIR)/autom4te.cache $(LOG)

# "make install" of big progs (glibc-headers) are failed with vfork
# when running from strace.
# Strace is too complicated for me to fix this and I fix "make" instead
# by disabling vfork. -- df
no_vfork = ac_cv_func_vfork=no ac_cv_func_vfork_works=no

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) &&	$(no_vfork)	\
     $(PKG_SRC_DIR)/configure	$(LOG)	\
	-C				\
	--prefix=$(INST_DIR)		\
	--with-gnu-ld			\
	--disable-nls			\
	--enable-largefile		\
	--enable-job-server		\
	--without-libiconv-prefix	\
	--without-libintl-prefix	\
	$(STD_VARS)
endef
# WARNINGS=none autoreconf --warnings=none -f -i
# ac_cv_lib_util_getloadavg=no ./configure
# --verbose

    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)


  INSTALL_CMD =   $(INSTALL_CMD_DEFAULT)


endif # has no GNU make 3.x

include $(RULES)
