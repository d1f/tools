PKG_VERSION   = $(CROSSTOOL_KEGEL_VERSION)
PKG_SITES     = http://kegel.com
PKG_SITE_PATH = crosstool
PKG_REMOTE_FILE_SUFFICES = .tar.gz
PKG_BASE      = crosstool
PKG_DIR       = crosstool-kegel

include $(DEFS)


include $(TOOLS_PKG_DIR)/crosstool.mk


PKG_PATCH_FILES  = $(PKG_PKG_DIR)/$(PKG)*.patch


include $(RULES)
