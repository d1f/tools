ifneq ($(strip $(shell which unxz 2>/dev/null)),)

PKG_HAS_NO_SOURCES = defined

else # has no xz

PKG_TYPE      = ORIGIN
PKG_VERSION   = 5.2.3
PKG_SITES     = http://tukaani.org
PKG_SITE_PATH = xz
PLATFORM      = build
PKG_REMOTE_FILE_SUFFICES = .tar.gz # bootstrap

include $(DEFS)


configure : $(call bstamp_f,install,ccache)

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) &&			\
     ./configure -C $(LOG)		\
	--prefix=$(INST_DIR)		\
	--enable-threads		\
	--disable-scripts		\
	--disable-doc			\
	--disable-static		\
	--disable-nls			\
	--with-gnu-ld			\
	--without-libiconv-prefix	\
	--without-libintl-prefix	\
	$(STD_VARS)
endef


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)
  INSTALL_CMD =   $(INSTALL_CMD_DEFAULT)


endif # has no xz

include $(RULES)
