# This builds gdb with host=target
GDB_HOST   = $(TARGET)
GDB_PREFIX = $(INST_DIR)

configure : $(call tstamp_f,install,ncurses)

define INSTALL_CMD
  $(INSTALL_CMD_DEFAULT)
  $(IW) $(INSTxFILES) $(IBIN_DIR)/gdb $(RBIN_DIR) $(LOG)
endef

include $(TOP_DIR)/tools/pkg/gdb.mk
