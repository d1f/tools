PKG_SITE_PATH ?= pool/main/a/$(PKG_BASE)
 AM_VER_MAJOR ?= $(AM_VER)

PLATFORM      = build

include $(DEFS)



POST_EXTRACT_CMD = $(RM) $(addprefix $(PKG_ORIG_SRC_DIR)/doc/,\
	version.texi stamp-vti) $(LOG)


include $(TOOLS_PKG_DIR)/autoconf.mk
include $(TOOLS_PKG_DIR)/automake.mk

define CONFIGURE_CMD
  $(RELINK)  $(PKG_BLD_DIR)/configure
  $(MKDIR_P) $(PKG_BLD_DIR)/doc	$(LOG)
  touch      $(PKG_BLD_DIR)/doc/automake.texi	$(LOG)
  touch      $(PKG_BLD_DIR)/doc/automake.info	$(LOG)
  touch      $(PKG_BLD_DIR)/aclocal.m4		$(LOG) # Needed by 02-init-m4-newline.diff
  touch      $(PKG_BLD_DIR)/Makefile.in		$(LOG) # Needed by 02-init-m4-newline.diff
  cd         $(PKG_BLD_DIR) && ./configure	$(LOG)	\
			--prefix=$(AM_PREFIX) $(AC_VARS)
endef


MK_VARS = MAKEINFO=true # avoid texinfo dependency

define INSTALL_CMD
  touch $(PKG_BLD_DIR)/doc/automake.texi	$(LOG)
  touch $(PKG_BLD_DIR)/doc/automake.info	$(LOG)
  $(INSTALL_CMD_DEFAULT)
  $(IWSH) "cd $(AM_PREFIX)/share && ln -s aclocal-$(AM_VER_MAJOR) aclocal" $(LOG)
  $(IWSH) "echo $(BSHARE_DIR)/aclocal > $(AM_PREFIX)/share/aclocal-$(AM_VER_MAJOR)/dirlist" $(LOG)
endef


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)


include $(RULES)
