AC_VER = 2.69
AM_VER = 1.10

PKG_TYPE      = DEBIAN3
PKG_VER_MAJOR = $(AM_VER)
PKG_VER_MINOR = 3
PKG_VER_PATCH = -3
PKG_BASE      = automake$(PKG_VER_MAJOR)

include $(TOP_DIR)/tools/pkg/automake-common.mk
