ifndef	tools/pkg/gdb.mk
	tools/pkg/gdb.mk = included


include $(TOP_DIR)/tools/pkg/gdb-version.mk

PKG_VERSION           = $(GDB_VERSION)

PKG_HAS_NO_SOURCES    = defined
SEPARATE_SRC_BLD_DIRS = defined
PKG_PATCHED_SRC_DIR   = $(PATCHED_SRC_DIR)/gdb_$(PKG_VERSION)

include $(DEFS)


fetch   : $(call tstamp_f,fetch  ,gdb)
extract : $(call tstamp_f,extract,gdb)
patch   : $(call tstamp_f,patch  ,gdb)

configure : $(call bstamp_f,install,bison flex m4 sed gawk)
configure : $(call tstamp_f,install,toolchain)

ifeq ($(GDB_HOST),$(BUILD))
  build : $(call bstamp_f,install,ncurses)

  CFG_VARS +=                  CC=$(BUILD_CC)
  CFG_VARS +=                 CXX=$(BUILD_CXX)
  CFG_VARS +=              CFLAGS="$(BUILD_CFLAGS)"
  CFG_VARS +=            CPPFLAGS="$(BUILD_CPPFLAGS)"
  CFG_VARS +=            CXXFLAGS="$(BUILD_CXXFLAGS)"
  CFG_VARS +=             LDFLAGS="$(BUILD_LDFLAGS)"

  CFG_VARS +=                  AR=$(BUILD_AR)
  CFG_VARS +=                  AS=$(BUILD_AS)
  CFG_VARS +=                  LD=$(BUILD_LD)
  CFG_VARS +=                  NM=$(BUILD_NM)
  CFG_VARS +=              RANLIB=$(BUILD_RANLIB)
  CFG_VARS +=               STRIP=$(BUILD_STRIP)
  CFG_VARS +=             OBJCOPY=$(BUILD_OBJCOPY)
  CFG_VARS +=             OBJDUMP=$(BUILD_OBJDUMP)
  CFG_VARS +=             READELF=$(BUILD_READELF)

else ifeq ($(GDB_HOST),$(TARGET))
  build : $(call tstamp_f,install,ncurses)

  CFG_VARS +=                  CC=$(CC)
  CFG_VARS +=                 CXX=$(CXX)
  CFG_VARS +=              CFLAGS="$(STD_CFLAGS)"
  CFG_VARS +=            CPPFLAGS="$(STD_CPPFLAGS)"
  CFG_VARS +=            CXXFLAGS="$(STD_CXXFLAGS)"
  CFG_VARS +=             LDFLAGS="$(STD_LDFLAGS)"

  CFG_VARS +=                  AR=$(AR)
  CFG_VARS +=                  AS=$(AS)
  CFG_VARS +=                  LD=$(LD)
  CFG_VARS +=                  NM=$(NM)
  CFG_VARS +=              RANLIB=$(RANLIB)
  CFG_VARS +=               STRIP=$(STRIP)
  CFG_VARS +=             OBJCOPY=$(OBJCOPY)
  CFG_VARS +=             OBJDUMP=$(OBJDUMP)
  CFG_VARS +=             READELF=$(READELF)

else
  $(error Unknown value of GDB_HOST: $(GDB_HOST))
endif

CFG_VARS +=       CC_FOR_TARGET=$(CC)
CFG_VARS +=      CXX_FOR_TARGET=$(CXX)
CFG_VARS +=      GCC_FOR_TARGET=$(CC)
CFG_VARS +=       AR_FOR_TARGET=$(AR)
CFG_VARS +=       AS_FOR_TARGET=$(AS)
CFG_VARS +=       LD_FOR_TARGET=$(LD)
CFG_VARS +=       NM_FOR_TARGET=$(NM)
CFG_VARS +=  OBJDUMP_FOR_TARGET=$(OBJDUMP)
CFG_VARS +=   RANLIB_FOR_TARGET=$(RANLIB)
CFG_VARS +=  READELF_FOR_TARGET=$(READELF)
CFG_VARS +=    STRIP_FOR_TARGET=$(STRIP)

  enable  = gdbcli
 disable  = gdbtk gtk libada libssp multibyte multilib nls sim tui gdbserver
 disable += libquadmath libquadmath-support
with      = curses gnu-ld mmap
without   = expat libexpat-prefix included-gettext included-regex
without  += libiconv-prefix libintl-prefix python tcl tk ust x
without  += system-readline

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) && \
     $(PKG_SRC_DIR)/configure -C $(LOG)		\
	--prefix=$(GDB_PREFIX)			\
	--build=$(BUILD)			\
	--host=$(GDB_HOST)			\
	--target=$(TARGET)			\
	$(addprefix --enable-,$(enable))	\
	$(addprefix --disable-,$(disable))	\
	$(addprefix --with-,$(with))		\
	$(addprefix --without-,$(without))	\
	$(CFG_VARS)
endef


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)


include $(RULES)


endif # tools/pkg/gdb.mk
