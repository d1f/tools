PKG_VERSION  = $(GCC_VERSION)
PKG_HAS_NO_SOURCES    = defined
SEPARATE_SRC_BLD_DIRS = defined

include $(DEFS)


ifdef EXTERN_TOOLCHAIN

PKG_HAS_NO_SOURCES = defined

else


patch     : $(call tstamp_f,patch,gcc)
ifneq ($(PLATFORM),build)
configure : $(call tstamp_f,install,binutils)
configure : $(call tstamp_f,install,linux-headers)
endif

# gcc-[34].x only
gcc34 = $(or $(filter 3.%,$(GCC_VERSION)),$(filter 4.%,$(GCC_VERSION)))
ifneq ($(gcc34),)
ifneq ($(PLATFORM),build)
ifeq ($(LIBC_TYPE),g)
# FIXME: LIBC_VERSION may be < 2.16
ifeq ($(shell $(VERCMP) $(LIBC_VERSION) '<' 2.16),true)
configure : $(call tstamp_f,install,$(LIBC_TYPE)libc-headers)
endif
endif
endif
endif

gcc-ge-4.3 := $(shell $(VERCMP) $(GCC_VERSION) '>=' 4.3)
ifeq ($(gcc-ge-4.3),true)
  configure : $(call bstamp_f,install,gmp mpfr)
  GCC_EXTRA_CONFIG +=  --with-gmp=$(BINST_DIR)
  GCC_EXTRA_CONFIG += --with-mpfr=$(BINST_DIR)
endif

# xsited: not really sure when this crap got started
gcc-ge-4.5 := $(shell $(VERCMP) $(GCC_VERSION) '>=' 4.5)
ifeq ($(gcc-ge-4.5),true)
  configure : $(call bstamp_f,install,mpclib)
  GCC_EXTRA_CONFIG += --with-mpc=$(BINST_DIR)
endif

PKG_PATCHED_SRC_DIR = $(PATCHED_SRC_DIR)/gcc_$(GCC_VERSION)

ifneq ($(PLATFORM),build)
  GCC_EXTRA_CONFIG += --with-as=$(AS) --with-ld=$(LD)
else
  STD_CFLAGS += -I/usr/include/i386-linux-gnu
  MK_JOBS = 1
endif

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR)			&&	\
  CC_FOR_BUILD=$(BUILD_CC)			\
  CFLAGS="$(STD_CFLAGS) $(TC_CFLAGS) -Wa,-W"	\
  $(PKG_SRC_DIR)/configure		$(LOG)	\
	--prefix=$(tc_PREFIX)			\
	--with-local-prefix=$(tc_SYSROOT)	\
	--build=$(BUILD)			\
	--host=$(BUILD)				\
	--target=$(TARGET)			\
	--with-headers=				\
	--with-newlib				\
	--enable-languages=c			\
	--disable-multilib			\
	--disable-shared			\
	--disable-threads			\
	--disable-nls				\
	--with-gnu-as				\
	--with-gnu-ld				\
	--disable-__cxa_atexit			\
	--disable-libssp			\
	--enable-symvers=gnu			\
	$(GCC_EXTRA_CONFIG)
endef
# --with-newlib is a hack for libgcc2
#	--nfp					\


#  make all-gcc all-target-libgcc || return 1
#  make DESTDIR=${startdir}/pkg install-gcc install-target-libgcc || return 1

  BUILD_CMD_TARGET  =     all-gcc
INSTALL_CMD_TARGET  = install-gcc
ifeq ($(gcc-ge-4.3),true)
  BUILD_CMD_TARGET +=     all-target-libgcc
INSTALL_CMD_TARGET += install-target-libgcc
endif

    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)


install : $(GCC_INSTALL_DEP_STAMP)

define INSTALL_CMD
  $(INSTALL_CMD_DEFAULT)
  $(MANGLE_INSTALLED_GPP)
  $(MANGLE_INSTALLED_GCC)
  $(IWSH) "if test -d $(tc_PREFIX)/lib/gcc/$(TARGET)/$(GCC_VERSION)/include; then \
		cd    $(tc_PREFIX)/lib/gcc/$(TARGET)/$(GCC_VERSION)/include/ && \
		  { \
		    if test -e limits.h   ; then : ; else $(LN_S) ../include-fixed/limits.h   ; fi; \
		    if test -e syslimits.h; then : ; else $(LN_S) ../include-fixed/syslimits.h; fi; \
		  } ; \
           fi" $(LOG)
endef

UNINSTALL2_CMD = $(RM) $(call tstamp_f,install,gcc-$(LIBC_TYPE)libc)


endif # ! EXTERN_TOOLCHAIN


include $(RULES)
