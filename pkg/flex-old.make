PKG_TYPE        = DEBIAN1

pkg_version     = 2.5.4

ifeq ($(PKG_TYPE),DEBIAN1)
  PKG_VERSION   = $(pkg_version)a
  PKG_VER_PATCH = -10
  PKG_SITE_PATH = pool/main/f/$(PKG_BASE)
else
  PKG_VERSION   =      $(pkg_version)a

  # http://flex.sourceforge.net/
  PKG_SITES     = $(SF_NET_SITES)
  PKG_SITE_PATH = $(PKG_BASE)
endif

include $(DEFS)


AC_VER = 2.69
include $(TOOLS_PKG_DIR)/autoconf.mk

pre-configure : $(call bstamp_f,install,libtool autotools-dev)

define PRE_CONFIGURE_CMD
  cp -v	$(BSHARE_DIR)/misc/config.guess	\
	$(BSHARE_DIR)/misc/config.sub	\
			$(PKG_BLD_SUB_DIR)           $(LOG)

  cd $(PKG_BLD_DIR) && libtoolize -c -f              $(LOG)
  $(AUTOCONF_CMD)
endef


configure : $(call bstamp_f,install,bison)
configure : $(TOOLCHAIN_INSTALL_STAMP)

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) &&			\
     ./configure -C	$(LOG)		\
	--prefix=$(INST_DIR)/old	\
	--build=$(BUILD)		\
	--host=$(TARGET)		\
	--with-gnu-ld			\
	--disable-nls			\
	--without-libiconv-prefix	\
	--without-libintl-prefix	\
	$(STD_VARS)
endef


  INSTALL_CMD =   $(INSTALL_CMD_DEFAULT)
    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)


include $(RULES)
