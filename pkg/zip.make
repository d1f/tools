ifneq ($(strip $(shell which $(PKG) 2>/dev/null)),)

PKG_HAS_NO_SOURCES = defined

else # has no zip

PKG_TYPE      = DEBIAN3
PKG_VERSION   = 3.0
PKG_VER_PATCH = -11
PKG_SITE_PATH = pool/main/z/$(PKG_BASE)
PLATFORM      = build

include $(DEFS)


PKG_PATCH2_FILES = $(PKG_PKG_DIR)/zip_*.patch


#STD_CPPFLAGS += -DNO_BZIP2_SUPPORT
#MK_VARS += prefix=$(INST_DIR)

MAKEFILE         = unix/Makefile
BUILD_CMD_TARGET = generic_gcc
BUILD_CMD = $(BUILD_CMD_DEFAULT)
BUILD_CMD = $(BUILD_CMD_DEFAULT)
CLEAN_CMD = $(CLEAN_CMD_DEFAULT)


INSTALL_CMD = $(IW) $(INSTxFILES) $(PKG_BLD_DIR)/zip $(BBIN_DIR) $(LOG)


endif # has no zip

include $(RULES)
