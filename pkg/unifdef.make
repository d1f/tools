ifneq ($(strip $(shell which $(PKG) 2>/dev/null)),)

PKG_HAS_NO_SOURCES = defined

else # has no unifdef

PKG_TYPE          = DEBIAN3
PKG_VERSION       = 2.10
PKG_VER_PATCH     = -1
PKG_SITE_PATH     = pool/main/u/$(PKG_BASE)
PLATFORM          = build

include $(DEFS)


MK_VARS += prefix=$(INST_DIR)
  BUILD_CMD =   $(BUILD_CMD_DEFAULT)
  CLEAN_CMD =   $(CLEAN_CMD_DEFAULT)
INSTALL_CMD = $(INSTALL_CMD_DEFAULT)


endif # has no unifdef

include $(RULES)
