# Libidn's purpose is to encode and decode internationalized domain names.

PKG_VERSION   = $(LIBC_VERSION)
PKG_SITES     = $(GNU_ORG_SITES)
PKG_SITE_PATH = glibc
SEPARATE_SRC_BLD_DIRS = defined

include $(DEFS)


include $(TOOLS_PKG_DIR)/glibc/addons.mk

ifneq ($(glibc_have_ext_addon_libidn),)

EXTRACT_STRIP_NUM = 0

include $(TOOLS_PKG_DIR)/crosstool.mk
PKG_PATCH_FILES  = $(CROSSTOOL_PATCH_DIR)/glibc/libidn-$(LIBC_VERSION)/*.patch

define PATCH_CMD
  if test -d $(PKG_PATCHED_SRC_DIR)/libidn; then : ;			\
  else mv    $(PKG_PATCHED_SRC_DIR)/glibc-libidn-$(LIBC_VERSION)	\
             $(PKG_PATCHED_SRC_DIR)/libidn;				\
  fi								$(LOG)
endef

else  # !glibc_have_ext_addon_libidn
  PKG_HAS_NO_SOURCES = defined
endif #  glibc_have_ext_addon_libidn


include $(RULES)
