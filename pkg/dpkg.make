PKG_TYPE              = DEBIAN_NATIVE
PKG_VERSION           = 1.18.18
PKG_SITE_PATH         = pool/main/d/$(PKG_BASE)
PLATFORM              = build

include $(DEFS)


configure : $(call bstamp_f,install,zlib bzip2 pkg-config)

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) && ./configure -C $(LOG) \
	--prefix=$(INST_DIR)		\
	--disable-nls			\
	--disable-dselect		\
	--disable-start-stop-daemon	\
	--disable-update-alternatives	\
	--disable-devel-docs		\
	--disable-coverage		\
	--disable-unicode		\
	--enable-mmap			\
	--disable-compiler-warnings	\
	--with-gnu-ld			\
	--without-libiconv-prefix	\
	--without-libintl-prefix	\
	--with-zlib			\
	--with-libbz2			\
	$(STD_VARS)
endef
# FIXME: pkg-config


  BUILD_CMD = $(BUILD_CMD_DEFAULT)


INSTALL_CMD = $(IW) $(INSTxFILES) $(PKG_BLD_DIR)/dpkg-deb/dpkg-deb $(BBIN_DIR) $(LOG)


include $(RULES)
