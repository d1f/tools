PKG_HAS_NO_SOURCES = defined
PLATFORM = build


define INSTALL_CMD
  $(IWSH) "$(call lnsdir_f,$(BBIN_DIR),/bin/true,texi2pdf)" $(LOG)
  $(IWSH) "$(call lnsdir_f,$(BBIN_DIR),/bin/true,texi2dvi)" $(LOG)
endef


include $(RULES)
