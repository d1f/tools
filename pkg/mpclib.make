PKG_TYPE      = DEBIAN1
PKG_VERSION   = 0.9
PKG_VER_PATCH = -4
PKG_SITE_PATH = pool/main/m/$(PKG_BASE)
PLATFORM      = build

include $(DEFS)


configure : $(call bstamp_f,install,libtool libmpdclient mpfr gmp)

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) &&		\
     ./configure $(LOG)		\
	--prefix=$(INST_DIR)	\
	--disable-shared	\
	--without-pic		\
	--with-gnu-ld		\
	$(STD_VARS)
endef


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)


define  INSTALL_CMD
    $(INSTALL_CMD_DEFAULT)
    $(IW) $(INST_FILES) $(PKG_BLD_DIR)/src/mpc.h $(BINC_DIR) $(LOG)
endef


include $(RULES)
