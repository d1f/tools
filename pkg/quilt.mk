# Usage:
# include $(TOOLS_PKG_DIR)/quilt.mk
# after include $(DEFS)

ifndef	tools/pkg/quilt.mk
	tools/pkg/quilt.mk = included


ifndef	    xwmake/defs.mk
$(error	$(I)xwmake/defs.mk did not included)
endif


QUILT_PATCH_DIRS ?= debian/patches

define APPLY_QUILT_PATCHES_CMD
  echo -n "$(I2)Applying quilt patches ... "
  for qdir in $(QUILT_PATCH_DIRS); do \
      if test -d $(PKG_PATCHED_SRC_DIR)/$$qdir; then \
         { cd    $(PKG_PATCHED_SRC_DIR); \
            QUILT_PATCHES=$$qdir quilt --quiltrc /dev/null push -a || test $$? = 2; \
         } $(LOG); \
      fi; \
  done
  echo "done"
endef


ifneq ($(PKG),quilt)
  patch : $(call bstamp_f,install,quilt)
  PATCH_CMD = $(APPLY_QUILT_PATCHES_CMD)
endif


endif # tools/pkg/quilt.mk
