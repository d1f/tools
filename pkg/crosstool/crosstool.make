PKG_ORIG_DIR     = $(PKG_PKG_DIR)/src
PKG_FETCH_METHOD = DONE

include $(DEFS)


include $(TOOLS_PKG_DIR)/crosstool.mk

patch : $(call tstamp_f,patch, crosstool-kegel crosstool-ng-1.5.3 crosstool-ng-1.22.0 crosstool-ng)

CROSSTOOL_KEGEL_PATCH_DIR = $(PATCHED_SRC_DIR)/crosstool-kegel_$(CROSSTOOL_KEGEL_VERSION)/patches
CROSSTOOL_NG153_PATCH_DIR = $(PATCHED_SRC_DIR)/crosstool-ng-1.5.3_$(CROSSTOOL_NG_153_VERSION)/patches
CROSSTOOL_NG122_PATCH_DIR = $(PATCHED_SRC_DIR)/crosstool-ng-1.22.0_$(CROSSTOOL_NG_122_VERSION)/patches
CROSSTOOL_NG_PATCH_DIR    = $(PATCHED_SRC_DIR)/crosstool-ng_$(CROSSTOOL_NG_VERSION)/patches


# list of crosstool patch dirs:
 patch_base_names  = binutils
#patch_base_names += cygwin
#patch_base_names += dejagnu
#patch_base_names += distcc
#patch_base_names += expect
#patch_base_names += eglibc
 patch_base_names += gcc
#patch_base_names += gdb
 patch_base_names += glibc
#patch_base_names += glibc-compat
 patch_base_names += glibc-linuxthreads
#patch_base_names += inetutils
#patch_base_names += linux
#patch_base_names += ptxdist
 patch_base_names += uClibc
 patch_base_names += uClibc-ng


define KEGEL_MERGE_CMD
  echo "$(I2)Merging old Kegel crosstool ..."
  set -e;								\
  for d in $(addprefix $(CROSSTOOL_KEGEL_PATCH_DIR)/,$(patch_base_names)); do	\
    bd=`basename $$d`;							\
    namevers=`echo $$d-[0-9]*`;						\
    if test -z "$$namevers"; then continue; fi;				\
    for nv in $$namevers; do						\
	if test -d "$$nv"; then : ; else continue; fi;			\
	bnv=`basename $$nv`;						\
	ver=`echo $$bnv | sed -e "s/$$bd-//"`;				\
	src=$(CROSSTOOL_KEGEL_PATCH_DIR)/$$bd-$$ver;			\
	case $$src in							\
	   *glibc-linuxthreads*)					\
		dst=$(CROSSTOOL_PATCH_DIR)/glibc/linuxthreads-$$ver;;	\
	   *)								\
		dst=$(CROSSTOOL_PATCH_DIR)/$$bd/$$ver;;			\
	esac;								\
	if test -d $$dst; then : ;					\
	else								\
	   echo "$(I3)Merging `printf "%-18s" $$bd` `printf "%-18s" $$ver` from crosstool-kegel ...";	\
	   $(LNTREE) $$src $$dst;					\
	fi;								\
    done;								\
  done
endef

define NG153_MERGE_CMD
  echo "$(I2)Merging crosstool-ng 1.5.3 ..."
  set -e;								\
  for d in $(addprefix $(CROSSTOOL_NG153_PATCH_DIR)/,$(patch_base_names)); do	\
    if test -d $$d; then : ; else continue; fi;				\
    bd=`basename $$d`;							\
    namevers=`echo $$d/*`;						\
    for nv in $$namevers; do						\
	bnv=`basename $$nv`;						\
	ver=$$bnv;							\
	src=$(CROSSTOOL_NG153_PATCH_DIR)/$$bd/$$ver;			\
	dst=$(CROSSTOOL_PATCH_DIR)/$$bd/$$ver;				\
	if test -d $$dst; then : ;					\
	else								\
	   echo "$(I3)Merging `printf "%-18s" $$bd` `printf "%-18s" $$ver` from crosstool-ng-1.5.3 ...";	\
	   $(LNTREE) $$src $$dst;					\
	fi;								\
    done;								\
  done
endef

define NG122_MERGE_CMD
  echo "$(I2)Merging crosstool-ng 1.22.0 ..."
  set -e;								\
  for d in $(addprefix $(CROSSTOOL_NG122_PATCH_DIR)/,$(patch_base_names)); do	\
    if test -d $$d; then : ; else continue; fi;				\
    bd=`basename $$d`;							\
    namevers=`echo $$d/*`;						\
    for nv in $$namevers; do						\
	bnv=`basename $$nv`;						\
	ver=$$bnv;							\
	src=$(CROSSTOOL_NG122_PATCH_DIR)/$$bd/$$ver;			\
	dst=$(CROSSTOOL_PATCH_DIR)/$$bd/$$ver;				\
	if test -d $$dst; then : ;					\
	else								\
	   echo "$(I3)Merging `printf "%-18s" $$bd` `printf "%-18s" $$ver` from crosstool-ng-1.22.0 ...";	\
	   $(LNTREE) $$src $$dst;					\
	fi;								\
    done;								\
  done
endef

define NG_MERGE_CMD
  echo "$(I2)Merging crosstool-ng $(CROSSTOOL_NG_VERSION) ..."
  set -e;								\
  for d in $(addprefix $(CROSSTOOL_NG_PATCH_DIR)/,$(patch_base_names)); do	\
    if test -d $$d; then : ; else continue; fi;				\
    bd=`basename $$d`;							\
    namevers=`echo $$d/*`;						\
    for nv in $$namevers; do						\
	bnv=`basename $$nv`;						\
	ver=$$bnv;							\
	src=$(CROSSTOOL_NG_PATCH_DIR)/$$bd/$$ver;			\
	dst=$(CROSSTOOL_PATCH_DIR)/$$bd/$$ver;				\
	if test -d $$dst; then : ;					\
	else								\
	   echo "$(I3)Merging `printf "%-18s" $$bd` `printf "%-18s" $$ver` from crosstool-ng ...";	\
	   $(LNTREE) $$src $$dst;					\
	fi;								\
    done;								\
  done
endef

define PATCH_CMD
     $(NG_MERGE_CMD)
  $(NG122_MERGE_CMD)
  $(NG153_MERGE_CMD)
  $(KEGEL_MERGE_CMD)
endef


include $(RULES)
