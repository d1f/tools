PKG_TYPE      = DEBIAN3
PKG_VER_MAJOR = 4
PKG_VERSION   = 3.1.5
PKG_VER_PATCH = -1
PKG_SITE_PATH = pool/main/m/$(PKG_BASE)
PLATFORM      = build

include $(DEFS)


configure : $(call bstamp_f,install,gmp)

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) &&		\
     ./configure $(LOG)		\
	--prefix=$(INST_DIR)	\
	--disable-shared	\
	--without-pic		\
	--with-gnu-ld		\
	--with-gmp=$(INST_DIR)	\
	$(STD_VARS)
endef


define BUILD_CMD
  $(DOMAKE) $(MKJ) -C $(PKG_BLD_DIR)/src ./get_patches.c $(LOG)
  $(BUILD_CMD_DEFAULT)
endef

    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)
  INSTALL_CMD =   $(INSTALL_CMD_DEFAULT)


include $(RULES)
