PKG_VERSION  = $(GCC_VERSION)-$(LIBC_VERSION)
PKG_HAS_NO_SOURCES    = defined
SEPARATE_SRC_BLD_DIRS = defined

include $(DEFS)


install : $(call tstamp_f,install,gcc-libc)
install : $(call bstamp_f,install,sed)

lgcc_s = $(tc_SYSROOT)/lib/libgcc_s.so
ldd    = $(tc_SYSROOT)/bin/ldd

define INSTALL_ROOT_CMD
  $(IW) $(INSTxFILES) $(tc_SYSROOT)/lib/libstdc++*.so* $(RLIB_DIR) $(LOG)
  $(IWSH) "if test -e $(lgcc_s); then $(INSTxFILES) $(lgcc_s)*  $(RLIB_DIR); fi" $(LOG)
  $(IWSH) "if test -e $(ldd)   ; then $(INSTxFILES) $(ldd)      $(RBIN_DIR); \
	$(SED) -i -e 's,/bin/bash,/bin/sh,' $(RBIN_DIR)/ldd; fi" $(LOG)
endef


include $(RULES)
