PKG_TYPE      = DEBIAN3
PKG_VERSION   = 0.7.3
PKG_VER_PATCH = -6
PKG_SITE_PATH = pool/main/l/$(PKG_BASE)

include $(DEFS)


configure : $(TOOLCHAIN_INSTALL_STAMP)
configure : $(call tstamp_f,install-root,libelf)

STD_LDFLAGS += -lstdc++ # __cxa_demangle()

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) &&		\
     ./configure -C $(LOG)	\
	--prefix=		\
	--build=$(BUILD)	\
	--host=$(TARGET)	\
	--enable-shared		\
	--disable-static	\
	--with-gnu-ld		\
	--with-libelf=$(INST_DIR)	\
	$(STD_VARS)
endef

MK_VARS += DESTDIR=$(INST_DIR)

    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)
  INSTALL_CMD =   $(INSTALL_CMD_DEFAULT)

define INSTALL_ROOT_CMD
  $(IW) $(INSTxFILES) $(IBIN_DIR)/ltrace          $(RBIN_DIR) $(LOG)
  $(IW) $(INSTxFILES) $(INST_DIR)/etc/ltrace.conf $(RETC_DIR) $(LOG)
endef


include $(RULES)
